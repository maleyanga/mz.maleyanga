package mz.maleyanga


import mz.maleyanga.pedidoDeCredito.PedidoDeCredito

/**
 * PedidoDeCreditoService
 * A service class encapsulates the core business logic of a Grails application
 */

class PedidoDeCreditoService {

    def validarPedido(PedidoDeCredito pedidoDeCreditoInstance){
        if(pedidoDeCreditoInstance.valorDeCredito < pedidoDeCreditoInstance.valorDaPenhora*2){

                respond pedidoDeCreditoInstance.errors, view:'edit'
                return

        }
    }
}
