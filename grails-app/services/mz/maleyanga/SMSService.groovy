package mz.maleyanga


import groovyx.net.http.HTTPBuilder
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.auth.BasicScheme
import org.apache.http.impl.client.DefaultHttpClient




/**
 * SMSService
 * A service class encapsulates the core business logic of a Grails application
 */

class SMSService {
    URLConnection urlConn = null

def sendMessage(String url){

   try {

       HttpClient httpClient = new DefaultHttpClient();
       HttpGet httpGet = new HttpGet("http://foo.com/bar");
       httpGet.addHeader(BasicScheme.authenticate(
               new UsernamePasswordCredentials("user", "password"),
               "UTF-8", false));

       HttpResponse httpResponse = httpClient.execute(httpGet);
       HttpEntity responseEntity = httpResponse.getEntity();

// read the stream returned by responseEntity.getContent()

   }catch (Exception e){

       throw new RuntimeException("Exception while calling URL:"+ url, e)
   }
}


}
