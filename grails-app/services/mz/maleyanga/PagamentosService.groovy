package mz.maleyanga


import mz.maleyanga.credito.Credito
import mz.maleyanga.pagamento.Pagamento
import org.springframework.transaction.annotation.Transactional

/**
 * PagamentosService
 * A service class encapsulates the core business logic of a Grails application
 */
@Transactional
class PagamentosService {

    def criarPagamentos(Credito creditoInstance){

        Calendar cal = Calendar.getInstance()
        cal.setTime(creditoInstance.dateConcecao)
        cal.add(Calendar.MONTH, 1)

        BigDecimal amortizacao = creditoInstance.valorCreditado+creditoInstance.percentualDejuros/creditoInstance.numeroDePrestacoes
        BigDecimal juros = creditoInstance.valorCreditado*(creditoInstance.percentualDejuros/100)

        1.upto(creditoInstance.numeroDePrestacoes) {
            def  pagamento = new Pagamento()
            pagamento.setDescricao("Prestacao No.${it}"+" Credito No."+creditoInstance.id)
            pagamento.setCredito(creditoInstance)
            pagamento.setDateCreated(creditoInstance.dateConcecao)
            pagamento.setValorDaPrestacao(amortizacao)

            pagamento.setDataDePagamento(cal.getTime())
            cal.add(Calendar.MONTH, 1)

            pagamento.save flush:true
        }

    }

    def calcularJurosDeDemora(Pagamento pagamentoInstance){
        def data = new Date()

            def juros = pagamentoInstance.credito.percentualJurosDeDemora
            Calendar cal = Calendar.getInstance()
            cal.setTime(pagamentoInstance.dataDePagamento)
            def dataActual = new Date()

            def dias =dataActual- pagamentoInstance.dataDePagamento
            if(dias>0 &&!pagamentoInstance.pago){
                pagamentoInstance.valorDeJurosDeDemora=(juros*pagamentoInstance.valorDaPrestacao/100)*dias
                pagamentoInstance.save flash:true
            }



    }





}
