package mz.maleyanga

import grails.transaction.Transactional
import mz.maleyanga.Taxa.Taxa
import mz.maleyanga.credito.Credito
import mz.maleyanga.pagamento.Pagamento
import mz.maleyanga.pedidoDeCredito.PedidoDeCredito

/**
 * CreditoService
 * A service class encapsulates the core business logic of a Grails application
 */
@Transactional
class CreditoService {
    Credito credito
    PedidoDeCredito pedidoDeCredito
    List<Pagamento> pagamentos
    def calcularTaxaDeSeguros(Credito creditoInstance){

      Taxa taxa = new Taxa()
        taxa.nome="Taxa se seguros"
        taxa.valor = creditoInstance.valorCreditado*0.04
        creditoInstance.addToTaxas(taxa)
    }
}
