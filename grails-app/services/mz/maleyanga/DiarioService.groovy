package mz.maleyanga

import grails.transaction.Transactional
import mz.maleyanga.diario.Diario

/**
 * DiarioService
 * A service class encapsulates the core business logic of a Grails application
 */
@Transactional
class DiarioService {

    Diario findByEstado(String estado) {
        return Diario.findByEstado(estado)
    }

    def save(Diario diario) {
        diario.save(flush: true)
    }
}
