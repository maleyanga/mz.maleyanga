package mz.maleyanga

import mz.maleyanga.cliente.Assinante
import mz.maleyanga.cliente.Cliente
import mz.maleyanga.conta.Conta
import mz.maleyanga.entidade.Entidade
import mz.maleyanga.security.Utilizador
import org.springframework.transaction.annotation.Transactional


/**
 * ClienteService
 * A service class encapsulates the core business logic of a Grails application
 */
@Transactional
class ClienteService {
    EncryptionService encryptionService

    boolean saveCliente(Cliente cliente, Conta ci) {
        Entidade entidade = Entidade.all.first()
        cliente.entidade = entidade
        System.println("entidade" + entidade)
        try {
            cliente.save flush: true
            Cliente clienteDb = Cliente.findById(cliente.id)
            if (clienteDb) {
                Conta conta = new Conta()
                conta.conta = Conta.findById(ci.id)
                conta.numeroDaConta = cliente.id
                Integer cod = cliente.id.toInteger()
                String str = String.format("%04d", cod)
                conta.codigo = ci.codigo + "." + str
                conta.designacaoDaConta = "conta_cliente" + '_' + conta.codigo
                conta.finalidade = 'conta_cliente'
                conta.cliente = cliente
                conta.save(flush: true)
                return true

            } else {
                System.println("erro nao gravacao do user")
                return false
            }


        } catch (Exception e) {
            System.println(e.toString())
            return false

        }


    }

    boolean saveAssinate(Assinante assinante) {

        try {
            if (assinante == null) {

                return false
            }
            if (assinante.hasErrors()) {

                return false
            }

            assinante.save flush: true
            Conta conta = new Conta()
            conta.conta = Conta.findById(ci.id)
            conta.numeroDaConta = cliente.id
            Integer cod = cliente.id.toInteger()
            String str = String.format("%04d", cod)
            conta.codigo = ci.codigo + "." + str
            conta.designacaoDaConta = "conta_cliente" + '_' + conta.codigo
            conta.finalidade = 'conta_movimento'
            conta.save(flush: true)
        } catch (Exception e) {
            System.println(e.toString())
        }

        return true

    }

    boolean mergeCliente(Cliente cliente) {
        try {
            cliente.merge()
            return true
        } catch (Exception e) {
            System.println(e.toString())
            return false
        }
    }

    boolean mergeAssinante(Assinante assinante) {
        try {
            assinante.merge()
            return true
        } catch (Exception e) {
            System.println(e.toString())
            return false
        }
    }

    boolean deleteliente(Cliente cliente) {
        try {
            def contaDb = Conta.findByCliente(cliente)
            contaDb.cliente = null
            contaDb.merge()
            cliente.delete(flush: true)

            return true
        } catch (Exception e) {
            System.println(e.toString())
            return false
        }
    }

    boolean deleteAssinante(Assinante assinante) {
        try {
            assinante.delete()
            return true
        } catch (Exception e) {
            System.println(e.toString())
            return false
        }
    }


}
