package settings

import grails.transaction.Transactional
import mz.maleyanga.SettingsService
import mz.maleyanga.Taxa.Taxa
import mz.maleyanga.pagamento.Pagamento
import mz.maleyanga.settings.DefinicaoDeCredito
import mz.maleyanga.settings.Settings
import org.omg.CORBA.NO_IMPLEMENT
import org.zkoss.bind.annotation.BindingParam
import org.zkoss.zk.grails.*

import org.zkoss.bind.annotation.Command
import org.zkoss.bind.annotation.NotifyChange
import org.zkoss.bind.annotation.Init
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.Button
import org.zkoss.zul.Label
import org.zkoss.zul.ListModelList
import ucar.nc2.util.NamedObject

import java.sql.SQLDataException
import java.sql.SQLException
@Transactional
class DefCreditoViewModel {
    SettingsService settingsService
    @Wire Button cfm
    @Wire Button bt_salvar
    @Wire Button cfq
    @Wire Button cfs
    @Wire Button cfd
    @Wire Button cfdd
    @Wire Label info
    Settings settings
    Settings setting = Settings.findByNome("settings")
    String selecedFormaDeCalculo
    String selecedPeriodicidade
    private  String calcularAutomatico
    private  String permitirDesembolsoComDivida
    private  String pagamentosEmOrdem
    DefinicaoDeCredito selectedDefinicaoDeCredito
    boolean def_fixa = true
    private ListModelList<DefinicaoDeCredito> definicoes
    private ListModelList<Taxa> taxas
    private  Taxa taxa

    Taxa getTaxa() {
        return taxa
    }

    void setTaxa(Taxa taxa) {
        this.taxa = taxa
    }

    ListModelList<Taxa> getTaxas() {
        if(taxas==null){
            taxas = new ListModelList<Taxa>(Taxa.findAllByActivo(true))
        }
        return taxas
    }
    DefinicaoDeCredito definicaoDeCredito = new DefinicaoDeCredito()

    String getPagamentosEmOrdem() {
        return settings?.pagamentosEmOrdem?.toString()
    }

    void setPagamentosEmOrdem(String pagamentosEmOrdem) {

        this.pagamentosEmOrdem = pagamentosEmOrdem
    }

    String getCalcularAutomatico() {
        return settings?.calcularAutomatico?.toString()
    }

    String getPermitirDesembolsoComDivida() {
        return settings?.permitirDesembolsoComDivida?.toString()
    }

    DefinicaoDeCredito getSelectedDefinicaoDeCredito() {
        return selectedDefinicaoDeCredito
    }

    void setSelectedDefinicaoDeCredito(DefinicaoDeCredito selectedDefinicaoDeCredito) {
        this.selectedDefinicaoDeCredito = selectedDefinicaoDeCredito
    }

    String getSelecedPeriodicidade() {
        return selecedPeriodicidade
    }

    void setSelecedPeriodicidade(String selecedPeriodicidade) {
        this.selecedPeriodicidade = selecedPeriodicidade
    }

    DefinicaoDeCredito getDefinicaoDeCredito() {
        return definicaoDeCredito
    }

    void setDefinicaoDeCredito(DefinicaoDeCredito definicaoDeCredito) {
        this.definicaoDeCredito = definicaoDeCredito
    }


    ListModelList<DefinicaoDeCredito> getDefinicoes() {

        if(definicoes==null){
            definicoes = new ListModelList<>()
        }
        definicoes.clear()
        definicoes= DefinicaoDeCredito.all
        return definicoes
    }

    @Command
    @NotifyChange(["definicoes"])
    def saveDefCredito(){
        if(definicaoDeCredito.descricao==""){
            info.value = "Preencha o campo 'Descrição'!"
            return
        }
        try {
                if(DefinicaoDeCredito.findByDescricao(definicaoDeCredito.descricao)){
                    info.value = "Já existe um valor com esta desrição"
                    return
                }
            definicaoDeCredito.periodicidade = selecedPeriodicidade
            definicaoDeCredito.formaDeCalculo = selecedFormaDeCalculo
            System.println(definicaoDeCredito.ativo)
            System.println(definicaoDeCredito.numeroDePrestacoes)
            System.println(definicaoDeCredito.periodicidade)
            System.println(definicaoDeCredito.formaDeCalculo)
            System.println(definicaoDeCredito.recorenciaDeMoras)
            System.println(definicaoDeCredito.percentualDejuros)
            System.println(definicaoDeCredito.percentualJurosDeDemora)

            definicaoDeCredito.ativo=true

              definicaoDeCredito.save(flush: true)
            if(DefinicaoDeCredito.findById(definicaoDeCredito.id)){
                getDefinicoes()
                info.value= "A definição de crédito foi criado com sucesso"
                info.style= "color:blue"
                definicaoDeCredito = new DefinicaoDeCredito()
            }else {
                info.value= "Erro na gravação dos dados"
                info.style= "color:red"
            }


        }catch(SQLException e){
            System.println(e.toString())
        }
    }

    @Command
    @NotifyChange(["definicoes"])
    def deleteDefCredito(){
        if (selectedDefinicaoDeCredito==null){
            info.value = "Selecione o item que deseja eliminar!"
            info.style= "color:red"
        }
        try {

                selectedDefinicaoDeCredito.delete(flush: true)
            getDefinicoes()

        }catch(SQLDataException e){
            System.println(e.toString())
        }
    }

    @Command
   def showDelMessage(){
        info.value="Faça double click para eliminar o item "+selectedDefinicaoDeCredito.descricao

    }
    @Command
    @NotifyChange(["def_fixa","definicaoDeCredito"])
    def changeAtivo(){
        if(definicaoDeCredito.ativo){
            definicaoDeCredito.ativo=false
        }
        else{
            definicaoDeCredito.ativo=true
        }

    }
    @Command
    @NotifyChange(["definicoes","selectedDefinicaoDeCredito","def_fixa","definicaoDeCredito","def_fixa","selecedPeriodicidade","selecedFormaDeCalculo"])
    def showItem(){
        definicaoDeCredito= DefinicaoDeCredito.findById(selectedDefinicaoDeCredito.id)
        selecedPeriodicidade=definicaoDeCredito.periodicidade
        selecedFormaDeCalculo = definicaoDeCredito.formaDeCalculo
        info.value = "O item "+selectedDefinicaoDeCredito.descricao +" foi selecionado!"
        System.println(selectedDefinicaoDeCredito)

    }
    @Command
    @NotifyChange(["def_fixa","definicaoDeCredito","definicoes"])
    def addDef(){
        definicaoDeCredito = new DefinicaoDeCredito()
        definicaoDeCredito.ativo =true

        if(def_fixa){
            def_fixa=false
        }else {def_fixa=true}
       definicaoDeCredito = new DefinicaoDeCredito()
    }
    String getSelecedFormaDeCalculo() {
        return selecedFormaDeCalculo
    }

    void setSelecedFormaDeCalculo(String selecedFormaDeCalculo) {
        this.selecedFormaDeCalculo = selecedFormaDeCalculo
    }
    boolean getDef_fixa() {
        return def_fixa
    }


    @Init init() {
        settings = Settings.findByNome("settings")
        getPagamentosEmOrdem()
    }

    @Command
    def salvarSettings(){

        definicaoDeCredito.merge()
        info.value = "Definições de crédito actualizados com sucesso!"

    }


    @NotifyChange(["info",'cfm','cfq','cfs','cfd'])
    Settings getSettings() {
        return settingsService.getSettings()
    }

    void setSettings(Settings settings) {
        this.settings = settings
    }

    @Command
    @NotifyChange(["settings"])
    def changeValueCfm(){
        if(settings.creditoFixoMensal){
            settings.creditoFixoMensal = false
            System.println(settings.creditoFixoMensal)
        }else {
            settings.creditoFixoMensal= true
            System.println(settings.creditoFixoMensal)
        }
    }
    @NotifyChange(["settings","info",'cfm','cfq','cfs','cfd','cfdd'])
    @Command
    def changeValueCfq(){
        if(settings.creditoFixoQuinzenal){
            settings.creditoFixoQuinzenal = false
        }else {
            settings.creditoFixoQuinzenal= true
        }
    }
    @NotifyChange(["settings","info",'cfm','cfq','cfs','cfd','cfdd'])
    @Command
    def changeValueCfs(){
        if(settings.creditoFixoSemanal){
            settings.creditoFixoSemanal = false
        }else {
            settings.creditoFixoSemanal= true
        }
    }
    @NotifyChange(["settings","info",'cfm','cfq','cfs','cfd','cfdd'])
    @Command
    def changeValueCfdd(){
        if(settings.creditoFixoDoisDias){
            settings.creditoFixoDoisDias = false
        }else {
            settings.creditoFixoDoisDias= true
        }
    }
    @NotifyChange(["settings","info",'cfm','cfq','cfs','cfd','cfdd'])
    @Command
    def changeValueCfd(){
        if(settings.creditoFixoDiario){
            settings.creditoFixoDiario = false
        }else {
            settings.creditoFixoDiario= true
        }
    }

    @Command
    @NotifyChange(["settings","calcularAutomatico"])
    def calcularMorasAutomatico(){
        settings = Settings.findByNome("settings")
        if(settings.calcularAutomatico){
            settings.calcularAutomatico=false
        }else {
            settings.calcularAutomatico=true
        }

        settings.merge(flush: true)
        info.value ="Dados atualizados com sucesso!"+settings.calcularAutomatico
        info.style = "color:red;font-weight;font-size:11pt;background:back"
    }


    @NotifyChange(["settings","permitirDesembolsoComDivida"])
    @Command
    def desembolsarComDividas(){
        settings = Settings.findByNome("settings")
        if(settings.permitirDesembolsoComDivida){
            settings.permitirDesembolsoComDivida=false
        }else {
            settings.permitirDesembolsoComDivida=true
        }

        settings.merge(flush: true)
        info.value ="Dados atualizados com sucesso! "+settings.permitirDesembolsoComDivida
        info.style = "color:red;font-weight;font-size:11pt;background:back"
    }

    Settings getSetting() {
        return Settings.findByNome("settings")
    }

    void setSetting(Settings setting) {
        this.setting = setting
    }

    @NotifyChange(["setting","definicaoDeCredito"])
    @Command
    def excluirSabados(){

        if(definicaoDeCredito.excluirSabados){
            definicaoDeCredito.excluirSabados=false
        }else {
            definicaoDeCredito.excluirSabados=true
        }
        info.value ="Dados alterados com sucesso! "+definicaoDeCredito.excluirSabados
        info.style = "color:red;font-weight;font-size:11pt;background:back"
    }

    @NotifyChange(["setting","definicaoDeCredito"])
    @Command
    def excluirDomingos(){

        if(definicaoDeCredito.excluirDomingos){
            definicaoDeCredito.excluirDomingos=false
        }else {
            definicaoDeCredito.excluirDomingos=true
        }

        info.value ="Dados alterados com sucesso! "+definicaoDeCredito.excluirDomingos
        info.style = "color:red;font-weight;font-size:11pt;background:back"
    }

         @NotifyChange(["definicaoDeCredito"])
         @Command
        def updateDados (){
             settings = Settings.findByNome("settings")
             settings.conta1 = settings.conta1
             settings.conta2 = settings.conta2
             settings.pagamentosEmOrdem = settings.pagamentosEmOrdem
            settings.merge(flush: true)
        info.value ="Dados actualizados com sucesso! "
        info.style = "color:red;font-weight;font-size:11pt;background:back"
        }

    @Command
    @NotifyChange(["settings","pagamentosEmOrdem"])
    def ordenarPagamentos(){
        settings = Settings.findByNome("settings")
        settings.pagamentosEmOrdem =!settings.pagamentosEmOrdem
        settings.merge(flush: true)

    }
}
