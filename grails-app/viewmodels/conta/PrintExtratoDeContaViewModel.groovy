package conta

import com.sun.org.apache.bcel.internal.generic.LMUL
import mz.maleyanga.ExtratoDeConta
import mz.maleyanga.ExtratoDeCredito
import mz.maleyanga.conta.Conta
import mz.maleyanga.conta.ContaService
import mz.maleyanga.transacao.Transacao

import org.zkoss.bind.annotation.Init

import org.zkoss.zul.ListModelList
import sun.font.BidiUtils

class PrintExtratoDeContaViewModel {
    Date dataInicial
    Date dataFinal
    Conta conta
    ContaService contaService
    ListModelList<Transacao> transacoes = new ListModelList<Transacao>()
    ListModelList<ExtratoDeConta>  extratoDeContas = new ListModelList<ExtratoDeConta>()
    BigDecimal totalCreditos = 0.0
    BigDecimal totalDebitos = 0.0
    BigDecimal saldoAnterior = 0.0

    BigDecimal getSaldoAnterior() {
        BigDecimal saldo = 0.0
        getExtratoDeContas()
        if(!extratoDeContas.empty){
            saldo  = extratoDeContas.first().saldo
            saldo-=extratoDeContas.first().debito
            saldo+=extratoDeContas.first().credito
        }

        return saldo
    }

    BigDecimal getTotalCreditos() {
        totalCreditos = 0.0
        for(ExtratoDeConta  e in extratoDeContas){
            totalCreditos+=e.credito
        }
        return totalCreditos
    }

    BigDecimal getTotalDebitos() {
        totalDebitos = 0.0
        for(ExtratoDeConta e in extratoDeContas){
            totalDebitos+=e.debito
        }
        return totalDebitos
    }

    ListModelList<ExtratoDeConta> getExtratoDeContas() {
        getTransacoes()
        BigDecimal saldo=0.0
        List extratos = new ListModelList<ExtratoDeConta>()

        for(Transacao transacao in transacoes){

            if(transacao.credito){
                ExtratoDeConta extratoDeConta = new ExtratoDeConta()
                saldo-=transacao.valor
                extratoDeConta.data = transacao.dateCreated
                extratoDeConta.credito=transacao.valor
                extratoDeConta.debito = 0.0
                extratoDeConta.descricao = transacao.descricao
                extratoDeConta.saldo=saldo
                extratoDeContas.add(extratoDeConta)
            }else {
                ExtratoDeConta extratoDeConta = new ExtratoDeConta()
                saldo+=transacao.valor
                extratoDeConta.data = transacao.dateCreated
                extratoDeConta.credito=0.0
                extratoDeConta.debito =transacao.valor
                extratoDeConta.descricao = transacao.descricao
                extratoDeConta.saldo =saldo
                extratoDeContas.add(extratoDeConta)
            }

        }
        for(ExtratoDeConta e in extratoDeContas){
            if (e.data.after(dataInicial)&&e.data.before(dataFinal)){
                extratos.add(e)
            }
        }
        return extratos
    }

    ListModelList<Transacao> getTransacoes() {
        if(conta.transacoes!=null){
            for(Transacao transacao in conta.transacoes){
                transacoes.add(transacao)
            }
        }

        transacoes.sort{it.id}
        return transacoes
    }

    @Init init() {
       conta = Conta.findById(contaService.conta.id)
        conta.calcularSaldo()
        dataFinal= contaService?.dataFinal
        dataInicial= contaService?.dataInicial
        System.println("dataInicial"+dataInicial)
        System.println("dataFinal"+dataFinal)
    }

    Conta getConta() {
        return conta
    }
}
