package relatorios

import mz.maleyanga.RelatoriosService
import mz.maleyanga.security.Utilizador
import org.zkoss.zk.grails.*

import org.zkoss.bind.annotation.Command
import org.zkoss.bind.annotation.NotifyChange
import org.zkoss.bind.annotation.Init
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.ListModelList

class PrestacoesViewModel {
    RelatoriosService relatoriosService
    private ListModelList gestores
    private  Utilizador selectedGestor
    Date dataInicial
    Date dataFinal

    Date getDataInicial() {
        return dataInicial
    }

    @NotifyChange(["dataFinal","dataInicial"])
    void setDataInicial(Date dataInicial) {
        Calendar c = Calendar.getInstance()
        c.setTime(dataInicial)
        c.add(Calendar.MONTH, 1)
        dataFinal = c.getTime()
        this.dataInicial = dataInicial
        relatoriosService.dataInicial = dataInicial
        relatoriosService.dataFinal = dataFinal
        System.println(relatoriosService.dataInicial)
        System.println(relatoriosService.dataFinal)
    }

    Date getDataFinal() {
        return dataFinal
    }

    void setDataFinal(Date dataFinal) {
        if(dataFinal<dataInicial){
            Calendar c = Calendar.getInstance()
            c.setTime(dataInicial)
            c.add(Calendar.DAY_OF_MONTH, 1)
            dataFinal = c.getTime()
            this.dataFinal = dataFinal

        }else {
            this.dataFinal = dataFinal
        }

        relatoriosService.dataFinal= dataFinal
    }

    Utilizador getSelectedGestor() {
        return selectedGestor
    }

    void setSelectedGestor(Utilizador selectedGestor) {
        this.selectedGestor = selectedGestor
        relatoriosService.selectedGestor = selectedGestor
    }

    ListModelList getGestores() {
        if (gestores==null){
            gestores = new ListModelList<Utilizador>()
        }
        gestores.clear()
        def utilizadores = Utilizador.all
        for(Utilizador u in utilizadores){
            if (u.authorities.any { it.authority == "CLIENTE_GESTOR" }) {
                gestores.add(u)
            }
        }
        return gestores
    }


}
