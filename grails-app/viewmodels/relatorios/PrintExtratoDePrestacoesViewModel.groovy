package relatorios

import mz.maleyanga.RelatoriosService
import mz.maleyanga.pagamento.Pagamento
import org.zkoss.zk.grails.*

import org.zkoss.bind.annotation.Command
import org.zkoss.bind.annotation.NotifyChange
import org.zkoss.bind.annotation.Init
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.ListModelList

class PrintExtratoDePrestacoesViewModel {

    private  dataInicial
    private  dataFinal
    BigDecimal totalValorDaPrestacao
    BigDecimal totalPago
    BigDecimal totalDivida
    BigDecimal totalMoras

    BigDecimal getTotalValorDaPrestacao() {
        return totalValorDaPrestacao
    }

    BigDecimal getTotalPago() {
        return totalPago
    }

    BigDecimal getTotalDivida() {
        return totalDivida
    }

    BigDecimal getTotalMoras() {
        return totalMoras
    }

    def getDataInicial() {

        return dataInicial
    }

    def getDataFinal() {

        return dataFinal
    }
  RelatoriosService relatoriosService
    private ListModelList<Pagamento> pagamentos
    @Init init() {

        if(relatoriosService.dataInicial!=null){
            dataInicial = relatoriosService.getDataInicial()
        }
        if(relatoriosService.dataFinal!=null){
            dataFinal = relatoriosService.getDataFinal()
        }

    }

    ListModelList<Pagamento> getPagamentos() {
        if(pagamentos==null){
            pagamentos = new ListModelList<Pagamento>(Pagamento.findAllByDataPrevistoDePagamentoBetween(dataInicial,dataFinal))
        }
        calcular()
        return pagamentos
    }
    def calcular(){
        totalMoras = 0.0
        totalPago = 0.0
        totalDivida = 0.0
        totalValorDaPrestacao = 0.0
        for(Pagamento p in pagamentos){
            totalMoras += p.valorDeJurosDeDemora
            totalPago += p.valorPago
            totalDivida += p.totalEmDivida
            totalValorDaPrestacao +=p.valorDaPrestacao
        }
    }
}
