package home

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.SpringSecurityUtils
import org.zkoss.bind.annotation.Command
import org.zkoss.bind.annotation.Init
import org.zkoss.bind.annotation.NotifyChange
import org.zkoss.zk.ui.Executions
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.Messagebox
import org.zkoss.zul.Window

class IndexViewModel {
    SpringSecurityService springSecurityService
    String message
    @Wire  btnHello
    @Wire  Window win

    @Init init() {
        if (!springSecurityService.isLoggedIn()) {
            Executions.sendRedirect("/login/auth")
        }

    }

    @NotifyChange(['message'])
    @Command clickMe() {
        message = "Clicked"
    }

}
