package credito

import grails.plugin.springsecurity.SpringSecurityService
import mz.maleyanga.ContadorService
import mz.maleyanga.CreditoService
import mz.maleyanga.PagamentoService
import mz.maleyanga.SettingsService
import mz.maleyanga.TaxaService
import mz.maleyanga.TransferenciaService
import mz.maleyanga.cliente.Cliente
import mz.maleyanga.conta.Conta
import mz.maleyanga.conta.ContaService
import mz.maleyanga.credito.Credito
import mz.maleyanga.pagamento.Pagamento
import mz.maleyanga.pagamento.Parcela
import mz.maleyanga.pedidoDeCredito.PedidoDeCredito
import mz.maleyanga.security.Utilizador
import mz.maleyanga.settings.DefinicaoDeCredito
import mz.maleyanga.settings.Settings
import mz.maleyanga.transacao.Transacao
import grails.transaction.Transactional
import org.zkoss.bind.annotation.Command
import org.zkoss.bind.annotation.NotifyChange
import org.zkoss.bind.annotation.Init
import org.zkoss.zk.ui.Executions
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.Button
import org.zkoss.zul.Grid
import org.zkoss.zul.Hbox
import org.zkoss.zul.Label
import org.zkoss.zul.ListModelList
import org.zkoss.zul.Tab

@Transactional
class CreateCreditoViewModel {
    @Wire Label info
    @Wire Grid gd_parcelas
    @Wire Hbox hb_editor
    @Wire Button bt_fechar
    @Wire Grid gd_new_credito
    String red = "color:red"
    String blue = "color:blue"
    Utilizador utilizador
    ListModelList<Credito> creditos
    ListModelList<Credito> emPagamento
    ListModelList<Credito> pendentes
    ListModelList<Credito> fechados
    ListModelList<Credito> invalidos
    ListModelList<Pagamento> pagamentos
    Pagamento sPagamento
    Conta contaCapital
    Conta contaCliente
    private dataInicial
    private dataFinal
    TransferenciaService transferenciaService
    @Wire Tab tb_abertos
   private PedidoDeCredito pedidoDeCredito
    private  Cliente selectedCliente
    ContadorService contadorService
     int numeroDePrestacoes
    private    String selecedPeriodicidade
    private String filter
    private String filterCredito
    TaxaService taxaService
    ContaService contaService
    PagamentoService pagamentoService
    CreditoService creditoService
    SpringSecurityService springSecurityService
    private  Credito credito = new Credito()
    SettingsService settingsService
    private  Settings settings
    private  String juros
    private  String jurosDeMora
    private  String formaDeCalculo
    private  DefinicaoDeCredito selectedDefinicaoDeCredito
    private BigDecimal v_prestacao =0.0
    private BigDecimal v_divida = 0.0
    private  BigDecimal v_mora= 0.0
    private  BigDecimal v_pago=0.0
    private  BigDecimal v_juro=0.0
    private  BigDecimal v_amo=0.0
    private ListModelList<DefinicaoDeCredito> definicoes
    private Double totalPrestacoes = 0
    private Double totalamortizacao=0
    private Double totaljuros = 0
    private boolean taxa

    boolean getTaxa() {

        return taxar()
    }

    def getDataInicial() {

        Calendar c = Calendar.getInstance()
        c.add(Calendar.MONTH,-1)
        dataInicial = c.getTime()
        return dataInicial
    }

    void setDataInicial(dataInicial) {
        this.dataInicial = dataInicial
    }

    def getDataFinal() {
        Calendar c = Calendar.getInstance()
        dataFinal = c.getTime()
        return dataFinal
    }

    void setDataFinal(dataFinal) {
        this.dataFinal = dataFinal
    }

    String getFilterCredito() {
        return filterCredito
    }

    void setFilterCredito(String filterCredito) {
        this.filterCredito = filterCredito
    }

    @NotifyChange(["creditos"])
    @Command
    def showCreditos(){
       tb_abertos.value = "Creditos do "+selectedCliente.nome
    }
    @Command
    def alertDelete(){
        Utilizador user = springSecurityService.currentUser as Utilizador
        if (!user.authorities.any { it.authority == "CREDITO_DELETE" }) {
            info.value="Este utilizador não tem permissão para executar esta acção !"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
        }
        if(credito.invalido){
            info.value="Este credito já foi invalidado!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
            return
        }
            if(credito.estado=="Pendente"||credito.estado=="Fechado"||credito.estado=="EmProgresso"){
                info.value="Este credito não pode ser invalidade!"
                info.style = "color:red;font-weight;font-size:11pt;background:back"

            }

        else {
            info.value="Double Click para eliminar este credito!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
        }
    }
    @Command
    @NotifyChange(["creditos"])
    def delete(){
        Utilizador user = springSecurityService.currentUser as Utilizador
        if (!user.authorities.any { it.authority == "CREDITO_DELETE" }) {
            info.value="Este utilizador não tem permissão para executar esta acção !"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
            return
        }
        if (credito.id == null) {
            info.value="Seleccione um credito!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
            return
        }
        try {
            def pagamentos = Pagamento.findAllByCredito(credito)
            for(Pagamento p in pagamentos){
                for(Parcela parcela in p.parcelas){
                    if(parcela.valorPago>0.0||parcela.valorParcial>0.0){
                        info.value="Este credito não pode ser invalidado pois tem pagamentos efetivados!"
                        info.style = "color:red;font-weight;font-size:11pt;background:back"
                        return
                    }
                }
            }
            for(Pagamento p in pagamentos){
                for(Parcela parcela in p.parcelas){
                    if(parcela.valorPago==0.0&&parcela.valorParcial==0.0){
                        parcela.pagamento==null
                        parcela.merge()
                        parcela.delete(flush: true)
                    }
                }
            }
            Credito creditoDb = Credito.findById(credito.id)
            Transacao tCredito = new Transacao()
            Transacao tDebito = new Transacao()
            tCredito.setValor(creditoDb.valorCreditado)
            tDebito.setValor(creditoDb.valorCreditado)
            tCredito.descricao = "estorno do crédito Nº:"+creditoDb.numeroDoCredito
            tDebito.descricao =  "estorno do crédito Nº:"+creditoDb.numeroDoCredito
            System.println("tCredito"+tCredito.valor)
            System.println("tDebito"+tDebito.valor)
            tCredito.credito = true
            tDebito.credito = false
            tCredito.save()
            tDebito.save()
           contaCliente = Conta.findByNumeroDaConta(creditoDb.cliente.id.toString())
            if (contaCapital.transacoes == null) {
                contaCapital.transacoes = new LinkedHashSet<Transacao>()
            }
            if (contaCliente.transacoes == null) {
                contaCliente.transacoes = new LinkedHashSet<Transacao>()
            }
            contaCapital.transacoes.add(tDebito)
            contaCliente.transacoes.add(tCredito)
            contaCapital.merge(flush: true)
            contaCliente.merge(flush: true)

            for (Pagamento p in pagamentos) {

               p.valorDaPrestacao = 0.0
                p.valorDeJuros =0.0
                p.valorDeJurosDeDemora =0.0
                p.valorDeAmortizacao = 0.0
                p.valorDeJurosDeDemora =0.0
                p.merge(flash: true)
            }
            creditoDb.invalido = true


            creditoDb.valorCreditado=0.0
            creditoDb.merge flush: true
            info.value="Crédito invalidado com sucesso!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"

        }catch(Exception e){
            System.println(e.toString())
            info.value = "Erro na eliminação do crédito!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
        }

    }
    BigDecimal getV_prestacao() {
        return v_prestacao
    }

    BigDecimal getV_divida() {
        return v_divida
    }

    BigDecimal getV_mora() {
        return v_mora
    }

    BigDecimal getV_pago() {
        return v_pago
    }

    BigDecimal getV_juro() {
        return v_juro
    }

    BigDecimal getV_amo() {
        return v_amo
    }

    def soma(){
        v_prestacao=0
        v_divida=0
        v_mora=0
        v_pago=0
        v_juro=0
        v_amo=0


        if(credito!=null)
        for (Pagamento p in credito.pagamentos){

            v_prestacao+=p.valorDaPrestacao
            v_divida+=p.totalEmDivida
            v_mora+=p.valorDeAmortizacao
            v_pago+=p.valorPago
            v_juro+=p.valorDeJuros
            v_amo+=p.valorDeAmortizacao
        }
    }



    String getFilter() {
        return filter
    }
    void setFilter(String filter) {
        this.filter = filter
    }
    private ListModelList<Cliente> clientes
    private ListModelList<Pagamento> prestacoes
    @NotifyChange(["credito","pagamentos","v_amo","v_juro","v_pago","v_divida","v_mora","v_prestacao"])
    @Command
    void doSearch() {
        info.value=""
        credito = Credito?.findById(filter?.toLong())
        if(credito?.invalido){
            info.value = "O Credito No."+credito.id+" é inválido!"
            info.style = red
            credito = null
            return
        }
      //  credito?.pagamentos?.each {pagamentoService.calcularJurosDeDemora(it)}
        soma()
        hb_editor.visible = false
        bt_fechar.label = "Abrir Editor"
        creditoService.credito = credito

    }

    @NotifyChange(["creditos","credito"])
    @Command
    void doSearchCredito(){
        info.value=""
        credito = Credito?.findByNumeroDoCredito(filterCredito)
        if(credito?.invalido){
            info.value = "O Credito No."+credito.id+" é inválido!"
            info.style = red
            credito = null
            return
        }
        //  credito?.pagamentos?.each {pagamentoService.calcularJurosDeDemora(it)}
        soma()
        hb_editor.visible = false
        bt_fechar.label = "Abrir Editor"
        creditoService.credito = credito

/*        info.value = ""
        credito = Credito?.findByNumeroDoCredito(filterCredito)

        if(credito?.invalido){
            info.value = "O Credito No."+credito.id+" é inválido!"
            info.style = red
            credito = null
            return
        }
       // credito?.pagamentos?.each {pagamentoService.calcularJurosDeDemora(it)}
        soma()
        hb_editor.visible = false
        bt_fechar.label = "Abrir Editor"*/
    }



    ListModelList<Pagamento> getPagamentos() {
        if(pagamentos==null){
            pagamentos = new ListModelList<Pagamento>()
        }
        pagamentos.clear()
        pagamentos = credito?.pagamentos
        soma()
        return pagamentos
    }

    Pagamento getsPagamento() {
        return sPagamento
    }

    void setsPagamento(Pagamento sPagamento) {
        this.sPagamento = sPagamento
    }

    @Command
    @NotifyChange(["pagamentos","v_amo","v_juro","v_pago","v_divida","v_mora","v_prestacao",'credito','hb_editor'])
    def showPagamentos(){
        getPagamentos()
        soma()
        hb_editor.visible = false
        bt_fechar.label = "Abrir Editor"

        creditoService.credito = credito
        creditoService.pagamentos = getPagamentos()
      // reCapitalizar()
    }
    @Command
    @NotifyChange(["prestacoes","credito"])
    def fecharEditor(){
        if(hb_editor.visible){
            hb_editor.visible = false
            bt_fechar.label= "Abrir Editor"

        }else {
            hb_editor.visible = true
            bt_fechar.label= "Fechar Editor"
            credito = new Credito()
        }


    }
    @Command
    def showCredito(){
        creditoService.credito = credito
        Executions.sendRedirect("/credito/show/"+credito.id)
    }
    ListModelList<Credito> getEmPagamento() {
        if(emPagamento ==null){
            emPagamento= new ListModelList<Credito>()
        }
        emPagamento.clear()
        emPagamento = Credito.findAllByEstado('EmProgresso')
        return emPagamento
    }

    ListModelList<Credito> getPendentes() {
        if(pendentes ==null){
            pendentes= new ListModelList<Credito>()
        }
        pendentes.clear()
        pendentes = Credito.findAllByEstado('Pendente')
        return pendentes
    }

    ListModelList<Credito> getFechados() {
        if(fechados ==null){
            fechados= new ListModelList<Credito>()
        }
        fechados.clear()
        fechados = Credito.findAllByEstadoAndInvalido('Fechado',false)
        return fechados
    }
    ListModelList<Credito> getInvalidos() {
        if(invalidos ==null){
            invalidos= new ListModelList<Credito>()
        }
        invalidos.clear()
        invalidos = Credito.findAllByInvalido(true)
        return invalidos
    }

    Conta getContaCliente() {
        return contaCliente
    }
    Conta getContaCapital() {
        return contaCapital
    }

    @NotifyChange(['contaCapital'])
    def geTContaCapital(){
        for(Conta c in utilizador.contas){
            if(c.finalidade=="conta_capital"){
                contaCapital= c
            }
        }
    }

    Utilizador getUtilizador() {
        return utilizador
    }

    ListModelList<Credito> getCreditos() {
        if(creditos ==null){
            creditos= new ListModelList<Credito>()
        }
        if(selectedCliente){
            creditos = Credito.findAllByClienteAndInvalido(selectedCliente,false)
            tb_abertos.value = "Creditos do(a) "+selectedCliente.nome
        }else
        creditos = Credito.findAllByEstado('Aberto')
        return creditos
    }

    void setNumeroDePrestacoes(int numeroDePrestacoes) {
        this.numeroDePrestacoes = numeroDePrestacoes
    }

    String getSelecedPeriodicidade() {
        return selecedPeriodicidade
    }

    @NotifyChange(["selectedDefinicaoDeCredito"])
    void setSelecedPeriodicidade(String selecedPeriodicidade) {
        cb_def.children.clear()

        selectedDefinicaoDeCredito = null

        this.selecedPeriodicidade = selecedPeriodicidade
    }

    ListModelList<DefinicaoDeCredito> getDefinicoes() {

        if(definicoes==null){
            definicoes = new ListModelList<>(DefinicaoDeCredito.all)
        }

        return definicoes
    }

    @Command
    @NotifyChange(["contaCliente","contaCapital"])
    def showDefinicoesDeCredito(){
        contaCliente = Conta.findByNumeroDaConta(selectedCliente.id.toString())
        if(definicoes.empty){
            Executions.sendRedirect("/settings/defCredito/")
        }

    }
    @NotifyChange(["definicaoDeCredito"])
    DefinicaoDeCredito getSelectedDefinicaoDeCredito() {
        if(creditoService?.pedidoDeCredito!=null){
            return creditoService?.pedidoDeCredito?.definicaoDeCredito
        }
        return selectedDefinicaoDeCredito
    }

    @NotifyChange(["selecedPeriodicidade","selectedDefinicaoDeCredito"])
    void setSelectedDefinicaoDeCredito(DefinicaoDeCredito selectedDefinicaoDeCredito) {
        this.selectedDefinicaoDeCredito = selectedDefinicaoDeCredito

    }

    Double getTotalPrestacoes() {

        return totalPrestacoes
    }

    Double getTotalamortizacao() {
        return totalamortizacao
    }

    Double getTotaljuros() {
        return totaljuros
    }

    ListModelList<Pagamento> getPrestacoes() {
        prestacoes=null
        if(prestacoes==null){
            prestacoes = new ListModelList<Pagamento>()
        }

        if (credito.periodicidade!=""&&credito.valorCreditado>0&&credito.numeroDePrestacoes){
            def prestacoes = pagamentoService.simuladorDeCredito(credito)
            totalPrestacoes = 0
            totaljuros = 0
            totalamortizacao = 0
            for(Pagamento p in prestacoes){
                totaljuros +=p.valorDeJuros
                totalamortizacao+=p.valorDeAmortizacao
                totalPrestacoes+=p.valorDaPrestacao
            }
            return  prestacoes
        }


    }

    Cliente getSelectedCliente() {
        return selectedCliente
    }

    void setSelectedCliente(Cliente selectedCliente) {
        this.selectedCliente = selectedCliente
    }
    @NotifyChange(['creditos',"credito"])
    @Init init() {
        utilizador = Utilizador.findById(springSecurityService.principal?.id)
       settings = settingsService.getSettings()
        creditoService.pagamentos = null
        creditoService.credito = null
        geTContaCapital()
        if(getDefinicoes().empty){

        }
   /*     if(creditoService?.credito?.id){
            credito = creditoService.credito
            pagamentos = creditoService.pagamentos
            creditoService.credito = null
        }
        if(contaService?.credito?.id){
            credito = contaService.credito
           creditoService.credito = null
        }*/
        if(creditoService?.pedidoDeCredito?.id){
            credito = new Credito()
            credito.valorCreditado = creditoService.pedidoDeCredito.valorDeCredito
            selectedCliente = Cliente.findById(creditoService.pedidoDeCredito.cliente.id)
            credito.dateCreated = new Date()
            pedidoDeCredito = creditoService.pedidoDeCredito
            contaCliente = Conta.findByCliente(pedidoDeCredito.cliente)
            selectedDefinicaoDeCredito = creditoService.pedidoDeCredito.definicaoDeCredito
            creditoService.pedidoDeCredito = null
        }
    }
    ListModelList<Cliente> getClientes() {
        if(clientes==null){
            clientes = new ListModelList<Cliente>(Cliente.findAllWhere(ativo: true))
        }
        return clientes
    }
    Credito getCredito() {

        return credito
    }


    @NotifyChange(["pagamentos","credito"])
    void setCredito(Credito credito) {
        this.credito = credito
        creditoService.credito = credito
        getPagamentos()
        soma()
    }

    Settings getSettings() {
        return settings
    }

    String getFormaDeCalculo() {
        return formaDeCalculo
    }

    @Command
    @NotifyChange(["juros","jurosDeMora","formaDeCalculo","info","prestacoes","totalPrestacoes","totaljuros","totalamortizacao","credito","contaCapital","contaCliente","taxa"])
    def showDetails(){
        geTContaCapital()
        getClientes()
        if(definicoes.empty){
            Executions.sendRedirect("/settings/defCredito/")
        }
        gd_parcelas.visible=true
        credito.numeroDePrestacoes = numeroDePrestacoes
        if(selectedDefinicaoDeCredito!=null){

            credito.periodicidade = selectedDefinicaoDeCredito.periodicidade

            credito.formaDeCalculo = selectedDefinicaoDeCredito.formaDeCalculo
            credito.percentualJurosDeDemora = selectedDefinicaoDeCredito.percentualJurosDeDemora
            credito.percentualDejuros = selectedDefinicaoDeCredito.percentualDejuros
            juros=selectedDefinicaoDeCredito.percentualDejuros
            jurosDeMora=selectedDefinicaoDeCredito.percentualJurosDeDemora
            formaDeCalculo = selectedDefinicaoDeCredito.formaDeCalculo


            if (selectedDefinicaoDeCredito.numeroDePrestacoes<credito.numeroDePrestacoes){
                info.value = "O número de prestações não permitido"
                info.style = red
                credito.numeroDePrestacoes = selectedDefinicaoDeCredito.numeroDePrestacoes
            }
            getPrestacoes()


        }



    }
    boolean taxar(){
        if (selectedDefinicaoDeCredito&&selectedCliente){
            //taxaService.calcularTaxas(selectedCliente,selectedDefinicaoDeCredito)
            def creditos = Credito.findAllByClienteAndPeriodicidadeAndInvalido(selectedCliente,selectedDefinicaoDeCredito.periodicidade,false) as List
            def reco_taxa = DefinicaoDeCredito.findById(selectedDefinicaoDeCredito.id).taxa.recorencia.toCharArray() as List
            def recorencia_taxa = new ArrayList<>()
            System.println(reco_taxa)
            if(creditos.empty){
                return reco_taxa[0] != "0"

            }else {
                for(int x=0; x<creditos.size(); x++){
                    recorencia_taxa.addAll(reco_taxa)
                }
                System.println(creditos)
                System.println(recorencia_taxa)
                System.println(recorencia_taxa[creditos.size()])
                return recorencia_taxa[creditos.size()] != "0"

            }
        }
    }
    String getJuros() {
        return juros
    }

    String getJurosDeMora() {
        return jurosDeMora
    }
    @Command
    @NotifyChange(['credito','creditos','contaCapital','contaCliente'])
    def salvarCredito(){
        if(contaCliente==null){
            info.value = "Este cliente não tem conta!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
            return
        }

        if(credito.dateConcecao>new Date()){
            info.value = "Data de conceção é inválida!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
           credito.dateConcecao = new Date()
        }
        if(contaCapital==null){
            info.value = "Este Utilizador não tem conta capital para gerar créditos!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
            return
        }
        if(contaCapital.saldo<credito.valorCreditado){
            info.value = "O seu saldo não cobre o valor do crédito!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
            return
        }

        if(selectedCliente==null){
            info.value = "Selecione um cliente!"
            info.style = "color:red"
            return
        }
            if(!settings.permitirDesembolsoComDivida){
                for(Credito credito1 in selectedCliente.creditos){
                    if(credito1.emDivida){
                        info.value = "Este cliente tem o crédito ID "+credito1.id+" em dívida!"
                        info.style = "color:red"
                        return
                    }
                }
            }


            if(credito.valorCreditado<=0){
                info.value = "O valor do crédito é inválido"
                info.style = "color:red"
                return

        }
        try {
            credito.cliente=selectedCliente

            credito.utilizador = utilizador
            def agora = new Date()
            Calendar c = Calendar.getInstance()
            c.setTime(agora)
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK)
            if (!settings.domingo&&dayOfWeek==1){
                info.value = "Dia não permitido!"
                info.style = red
                return
            }
            if (!settings.segunda&&dayOfWeek==2){
                info.value = "Dia não permitido!"
                info.style = red
                return
            }
            if (!settings.terca&&dayOfWeek==3){
                info.value = "Dia não permitido!"
                info.style = red
                return
            }
            if (!settings.quarta&&dayOfWeek==4){
                info.value = "Dia não permitido!"
                info.style = red
                return
            }
            if (!settings.quinta&&dayOfWeek==5){
                info.value = "Dia não permitido!"
                info.style = red
                return
            }
            if (!settings.sexta&&dayOfWeek==6){
                info.value = "Dia não permitido!"
                info.style = red
                return
            }
            if (!settings.sabado&&dayOfWeek==7){
                info.value = "Dia não permitido!"
                info.style = red
                return
            }
            if (!selectedCliente.ativo){
                info.value ="Este cliente esta impedido de ter novos crédito!"
                info.style = red
                return
            }

            if(settings.atualizarDadosDoCliente){
                if (credito.cliente.dataDeExpiracao<agora){
                    info.value ="Este cliente esta com documento fora do prazo!"
                    info.style = red
                    return
                }
            }

            credito.emDivida=true
            credito.invalido=false
            if(taxar()){
                credito.taxas = credito.valorCreditado*DefinicaoDeCredito.findById(selectedDefinicaoDeCredito.id).taxa.percentagem/100
            }

            credito.setNumeroDoCredito(contadorService.gerarNumeroDoCredito())
            credito.save flush: true
            pagamentoService.criarPagamentos(credito,selectedDefinicaoDeCredito)

            Transacao tCredito = new Transacao()
            Transacao tDebito = new Transacao()
            tCredito.setValor(credito.valorCreditado)
            tDebito.setValor(credito.valorCreditado)
            tCredito.descricao = "debito a conta:"+contaCliente.codigo
            tDebito.descricao = "credito da conta:"+contaCapital.codigo
            System.println("tCredito"+tCredito.valor)
            System.println("tDebito"+tDebito.valor)
            tCredito.credito = true
            tDebito.credito = false
            tCredito.save(flush: true)
            tDebito.save(flush: true)

                contaCapital = Conta.findById(contaCapital.id)
                contaCliente = Conta.findById(contaCliente.id)
            if (contaCapital.transacoes == null) {
                contaCapital.transacoes = new LinkedHashSet<Transacao>()
            }
            if (contaCliente.transacoes == null) {
                contaCliente.transacoes = new LinkedHashSet<Transacao>()
            }
            contaCapital.transacoes.add(tCredito)
            contaCliente.transacoes.add(tDebito)
            contaCapital.merge(flush: true)
            contaCliente.merge(flush: true)

            info.value = "O Credito No."+credito.id+" da "+selectedCliente.nome+" foi criado com sucesso!"
            info.style = blue
            geTContaCapital()
            getClientes()
            creditos.add(credito)
            if(pedidoDeCredito?.id){
                PedidoDeCredito pdcDB = PedidoDeCredito.findById(pedidoDeCredito?.id)
                pdcDB.creditado = credito
                pdcDB.merge(flush: true)
            }
           // Executions.sendRedirect("/credito/printPlano")
            printPlanoDePagamento()
       fecharEditor()
        }catch (Exception e){
            info.value = e.toString()
            info.style= red

        }

    }
    @Command
    @NotifyChange(["credito"])
    def addCredito(){
        hb_editor.visible = true
        bt_fechar.label= "Fechar Editor"
        credito = new Credito()
    }
    @Command
     def printExtrato(){
        if(credito.invalido){
            info.value = "O Credito No."+credito.id+" é inválido!"
            info.style = red
            return
        }
        reCapitalizar()
        creditoService.credito = credito
        creditoService.pagamentos = getPagamentos()
      //  Executions.sendRedirect("/credito/printExtratoDeCredito")
    }

    @Command
    @NotifyChange(["credito"])
    def reCapitalizar(){
        try {
            if(credito.id!=null){
                List<Pagamento> pagamentos = Pagamento.findAllByCredito(credito)
                System.println(credito.pagamentos)
                for(Pagamento pagamento in pagamentos){
                    if(pagamento.descricao=="CAPITALIZACAO"){
                        credito.pagamentos.remove(pagamento)
                        pagamento.save()
                        credito.merge(flush: true)

                    }
                }
                List<Pagamento> pagaments = Pagamento.findAllByCredito(credito)
                for(Pagamento pag in pagaments){
                    pagamentoService.calcularMoras(pag)
                    pagamentoService.calcularMoraCaPital(pag)
                }
            }

           // info.value = "Recapitalização feita com sucesso!"
            info.style = red
        }catch(Exception e){
            info.value = "Erro na Recapitalização!, por favor, faça refresh e tente novamente!"
            info.style = red
        }


    }
    @Command
    def editPagamento(){
        if(credito.invalido){
            return
        }
        pagamentoService.pagamentoInstance =sPagamento
        Executions.sendRedirect("/pagamento/pagamentos")
    }
    @Command
    def printPlanoDePagamento(){
        if(credito.invalido){
            info.value = "O Credito No."+credito.id+" é inválido!"
            info.style = red
            return
        }
        creditoService.credito = credito
        creditoService.pagamentos = getPagamentos()
     //  Executions.sendRedirect("/credito/printPlano")
    }

    @Command
    @NotifyChange(['items',"novaConta"])
    def printExtratoCliente (){
        def rconta = Conta.findByNumeroDaConta(credito.id.toString())
        if(rconta==null){
            info.value = "Selecione uma conta!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
            return
        }
        if(dataFinal==null){
            info.value = "Selecione uma data!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
            return
        }
        if(dataInicial==null){
            info.value = "Selecione uma data!"
            info.style = "color:red;font-weight;font-size:11pt;background:back"
            return
        }
        contaService.conta = rconta
        contaService.credito = credito
        contaService.dataInicial = dataInicial
        contaService.dataFinal = dataFinal
        Executions.sendRedirect("/conta/printExtratoDeConta/")
    }
}
