package parcela

import mz.maleyanga.credito.Credito
import mz.maleyanga.pagamento.Parcela
import mz.maleyanga.pagamento.ParcelaService

import org.zkoss.bind.annotation.Init


class PrintEntradaViewModel {

    Parcela parcelaInstance
    ParcelaService parcelaService
    List<Parcela> parcelas

    List<Parcela> getParcelas() {
        if(parcelas==null){
            parcelas = new LinkedList<Parcela>(Parcela.findAllByNumeroDoRecibo(parcelaInstance.numeroDoRecibo))
        }

        return parcelas
    }



    Parcela getParcelaInstance() {
        return parcelaInstance
    }



    @Init init() {
        // initialzation code here
        parcelaInstance = Parcela.findById(parcelaService.entrada.id)
    }



}
