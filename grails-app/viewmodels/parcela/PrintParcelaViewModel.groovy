package parcela

import mz.maleyanga.ClienteService
import mz.maleyanga.cliente.Cliente
import mz.maleyanga.credito.Credito
import mz.maleyanga.pagamento.Parcela
import mz.maleyanga.pagamento.ParcelaService
import org.zkoss.zk.grails.*

import org.zkoss.bind.annotation.Command
import org.zkoss.bind.annotation.NotifyChange
import org.zkoss.bind.annotation.Init
import org.zkoss.zk.ui.select.annotation.Wire

class PrintParcelaViewModel {
    Parcela parcelaInstance
    Credito creditoInstance
    ParcelaService parcelaService
    List<Parcela> parcelas
    BigDecimal total =0.0

    List<Parcela> getParcelas() {
        if(parcelas==null){
            parcelas = new LinkedList<Parcela>(Parcela.findAllByNumeroDoRecibo(parcelaService.parcelaInstance.numeroDoRecibo))
        }

        return parcelas
    }

    BigDecimal getTotal() {
       return total
    }

    Parcela getParcelaInstance() {
        return parcelaInstance
    }



    @Init init() {
        // initialzation code here
        parcelaInstance = Parcela.findById(parcelaService.parcelaInstance.id)
        if(parcelaInstance.pagamento!=null){
            creditoInstance = Credito.findById(parcelaService.parcelaInstance.pagamento.credito.id)
        }
              total = parcelaInstance.valorPago

    }

    Credito getCreditoInstance() {
        return parcelaService.creditoInstance
    }
}
