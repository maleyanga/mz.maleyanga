package parcela

import mz.maleyanga.credito.Credito
import mz.maleyanga.pagamento.Parcela
import mz.maleyanga.pagamento.ParcelaService
import org.zkoss.zk.grails.*

import org.zkoss.bind.annotation.Command
import org.zkoss.bind.annotation.NotifyChange
import org.zkoss.bind.annotation.Init
import org.zkoss.zk.ui.select.annotation.Wire

class PrintParcelaDuplicadoViewModel {

    Parcela parcelaInstance
    Credito creditoInstance
    ParcelaService parcelaService
    List<Parcela> parcelas
    BigDecimal total =0.0

    List<Parcela> getParcelas() {
        if(parcelas==null){
            parcelas = new LinkedList<Parcela>(Parcela.findAllByNumeroDoRecibo(parcelaService.parcelaInstance.numeroDoRecibo))
        }
        return parcelas
    }

    BigDecimal getTotal() {
       return total
    }

    Parcela getParcelaInstance() {
        return parcelaInstance
    }



    @Init init() {
       creditoInstance = Credito.findById(parcelaService.creditoInstance.id)
        parcelaInstance = Parcela.findById(parcelaService.parcelaInstance.id)
        total = parcelaInstance.valorPago
    }

    Credito getCreditoInstance() {

        return parcelaService.creditoInstance
    }

}
