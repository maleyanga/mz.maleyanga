package cliente

import grails.transaction.Transactional
import mz.maleyanga.cliente.Cliente
import mz.maleyanga.conta.Conta
import mz.maleyanga.entidade.Entidade
import mz.maleyanga.security.Utilizador
import org.zkoss.zk.grails.*

import org.zkoss.bind.annotation.Command
import org.zkoss.bind.annotation.NotifyChange
import org.zkoss.bind.annotation.Init
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.Hbox
import org.zkoss.zul.Label
import org.zkoss.zul.ListModelList

@Transactional
class NovoClienteViewModel {
    private String error_ = "Erro na gravação dos dados"
    private  String filter
    private ListModelList gestores
    private ListModelList tiposDeInde
    private Utilizador selectedGestor
    private ListModelList<Conta> integradoras
    private Conta sContaIntegradora
    private ListModelList estadosCivil
    private Cliente novoCliente = new Cliente()
    private  @Wire Label info
    private  @Wire Hbox hb_novo_cliente
    private String blue = "color:blues;font-size:12pt"
    private String red = "color:red"
    private ListModelList<Cliente> clientes

    String getFilter() {
        return filter
    }

    void setFilter(String filter) {
        this.filter = filter
    }

    @Command
    void doSearch() {
        fecharEditor()
        clientes.clear()
        List<Cliente> allItems = Cliente.all
        if (filter == null || "".equals(filter)) {
            clientes.addAll(allItems)
        } else {
            for (Cliente item : allItems) {
                if (item.nome.toLowerCase().indexOf(filter.toLowerCase()) >= 0 ||
                        item.nuit.toString().indexOf(filter) >= 0 ||
                        item.residencia.toString().indexOf(filter) >= 0 ||
                        item.dateCreated.format('dd/MM/yyyy').toString().indexOf(filter) >= 0 ||
                        item.numeroDeIndentificao.indexOf(filter) >= 0) {
                    clientes.add(item)
                }
            }
        }
    }
    ListModelList<Cliente> getClientes() {
        if(clientes==null){
            clientes = new ListModelList<Cliente>()
        }
        clientes.clear()
        clientes = Cliente.all
        return clientes
    }

    @NotifyChange(["novoCliente","clientes"])
    @Command
    def addCliente(){
        info.value = ""
        novoCliente = new Cliente()
    }
    @Command
    def fecharEditor(){
        hb_novo_cliente.visible = false
    }
    Utilizador getSelectedGestor() {
        return selectedGestor
    }

    void setSelectedGestor(Utilizador selectedGestor) {
        this.selectedGestor = selectedGestor
    }

    Cliente getNovoCliente() {
        return novoCliente
    }
    ListModelList getEstadosCivil() {
        if(estadosCivil==null){
            estadosCivil = new ListModelList<String>(["Solteiro","Solteira","Casado","Casada","Separado Judicialmente","Separada Judicialmente", "Outro(a)"])
        }

        return estadosCivil
    }
    ListModelList getTiposDeInde() {
        if(tiposDeInde == null){
            tiposDeInde = new ListModelList<String>(["BI","Passaporte","Carta de conducao", "Outro"])
        }
        return tiposDeInde
    }
    ListModelList<Conta> getIntegradoras() {
        if (integradoras==null){
            integradoras = new ListModelList<Conta>(Conta.findAllByFinalidade("conta_integradora"))
        }

        return integradoras
    }
    ListModelList getGestores() {
        if (gestores==null){
            gestores = new ListModelList<Utilizador>()
        }
        gestores.clear()
        def utilizadores = Utilizador.all
        for(Utilizador u in utilizadores){
            if (u.authorities.any { it.authority == "CLIENTE_GESTOR" }) {
                gestores.add(u)
            }
        }
        return gestores
    }
    void setNovoCliente(Cliente novoCliente) {
        this.novoCliente = novoCliente
    }
    @Command
    @NotifyChange(['novoCliente'])
    def salvarCliente(){
        info.value = ""
        Cliente cliente = new Cliente()
        try {
            if(novoCliente.nome.equals(null)){
                info.value = "Preencha o campo 'Nome Completo'!"
                info.style = "color:red"
                return
            }
            if(Cliente.findByNome(novoCliente.nome)){
                info.value = "Já existe um cliente com este nome!"
                info.style = "color:red"
                return
            }
            if(Cliente.findById(novoCliente.id)){
                info.value = "Este Cliente já existe na base de dados!"
                info.style = "color:red"
                return
            }
            if(novoCliente.estadoCivil.equals(null)){
                info.value = "Selecione  o estado civil !"
                info.style = "color:red"
                return
            }
            /*if(novoCliente.nuit.equals(null)){
                info.value = "Preencha o campo 'NUIT'"
                info.style = red
                return
            }*/
            if(Cliente.findByNuit(novoCliente.nuit)){
                info.value = "Já existe um cliente com este NUIT!"
                info.style = "color:red"
                return
            }
            if(novoCliente.tipoDeIndentificacao.equals(null)){
                info.value = "Selecione  o tipo de documento de indentificação!"
                info.style = "color:red"
                return
            }
            if(novoCliente.dataDeExpiracao.equals(null)){
                info.value = "Selecione  a data de expiração !"
                info.style ="color:red"
                return
            }
            if(novoCliente.numeroDeIndentificao.equals(null)){
                info.value = "Digite o número de indentificação !"
                info.style = "color:red"
                return
            }
            if(novoCliente.residencia.equals(null)){
                info.value = "Preencha o campo residência!"
                info.style = "color:red"
                return
            }
            if(novoCliente.utilizador==null){
                info.value = "Selecione um gestor!"
                info.style = "color:red"
                return
            }

            if(sContaIntegradora==null){
                info.value = "Selecione um uma conta integradora!"
                info.style = "color:red"
                return
            }
            info.value = ""
            novoCliente.classificacao = "medio"
            novoCliente.utilizador = selectedGestor
            novoCliente.ativo = true
            Entidade entidade =Entidade.all.first()
            novoCliente.entidade = entidade
            cliente.nome = novoCliente.nome
            System.println(cliente.nome)
            cliente.nuit = novoCliente.nuit
            System.println(cliente.nuit)
            cliente.tipoDeIndentificacao = novoCliente.tipoDeIndentificacao
            System.println(cliente.tipoDeIndentificacao)
            cliente.numeroDeIndentificao = novoCliente.numeroDeIndentificao
            System.println(cliente.numeroDeIndentificao)
            cliente.residencia = novoCliente.residencia
            System.println(cliente.residencia)
            cliente.email = novoCliente.email
            System.println(cliente.email)
            cliente.localDeTrabalho = novoCliente.localDeTrabalho
            System.println(cliente.localDeTrabalho)
            cliente.telefone = novoCliente.telefone
            System.println(cliente.telefone)
            cliente.telefone1 = novoCliente.telefone1
            System.println(cliente.telefone1)
            cliente.telefone2 = novoCliente.telefone2
            System.println(cliente.telefone2)
            cliente.entidade = Entidade.findById(novoCliente.entidade.id)
            System.println(cliente.entidade)
            cliente.dataDeExpiracao = novoCliente.dataDeExpiracao
            System.println(cliente.dataDeExpiracao)
            cliente.ativo = novoCliente.ativo
            System.println(cliente.ativo)
            cliente.utilizador = Utilizador.findById(novoCliente.utilizador.id)
            System.println(cliente.utilizador)
            cliente.classificacao = novoCliente.classificacao
            System.println(cliente.classificacao)
            cliente.estadoCivil = novoCliente.estadoCivil
            System.println(cliente.estadoCivil)
             cliente.dateCreated = new Date()
            cliente.lastUpdated = new   Date()
            System.println("dataCreated"+cliente.dateCreated)
            System.println("lastUpdate"+cliente.lastUpdated)
            System.println(cliente.errors)
            cliente.save(flush: true)
            Cliente clienteDb = Cliente.findById(cliente.id)
            if(clienteDb.id!=null){
                System.println("clienteDb"+clienteDb)
                Conta conta = new Conta()
                conta.conta = Conta.findById(sContaIntegradora.id)
                conta.numeroDaConta = clienteDb.id
                Integer cod = clienteDb.id.toInteger()
                String str = String.format("%04d", cod)
                conta.codigo = sContaIntegradora.codigo + "." + str
                conta.designacaoDaConta = "conta_cliente" + '_' + conta.codigo
                conta.finalidade = 'conta_cliente'
                conta.cliente = clienteDb
                conta.save(flush: true)
                info.style = "color:blue;font-size:14pt"
                info.value="Os Dados do "+ clienteDb.nome+", foram gravados com sucesso!"

            }else {
                info.value = "Erro na gravação dos dados"
                info.style  ="color:red"

            }

        } catch (Exception e) {
            info.value = "Erro na gravação dos dados"
            info.style  ="color:red"
            System.println(e.toString())


        }




    }

    Conta getsContaIntegradora() {
        return sContaIntegradora
    }

    void setsContaIntegradora(Conta sContaIntegradora) {
        this.sContaIntegradora = sContaIntegradora
    }

}
