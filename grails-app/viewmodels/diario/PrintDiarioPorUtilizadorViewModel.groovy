package diario


import mz.maleyanga.ComposerService
import mz.maleyanga.diario.Diario
import mz.maleyanga.pagamento.Parcela
import mz.maleyanga.saidas.Saida
import mz.maleyanga.security.Utilizador
import org.zkoss.bind.annotation.Init
import org.zkoss.zul.ListModelList

class PrintDiarioPorUtilizadorViewModel {

    Diario diario
    ComposerService composerService
    Utilizador utilizador
    BigDecimal totalParcelas = 0.0
    BigDecimal totalSaidas = 0.0
    BigDecimal totalSaidasAtivas = 0.0
    BigDecimal totalSaidasPassivas = 0.0
    BigDecimal saldo = 0.0
    List<Parcela> parcelas
    List<Saida> saidas
    List<Saida> saidasAtivas
    List<Saida> saidasPassivas

    BigDecimal getTotalSaidasAtivas() {
        totalSaidasAtivas =0.0
        for(Saida saida in saidasAtivas){
            totalSaidasAtivas+=saida.valor
        }
        return totalSaidasAtivas
    }

    BigDecimal getTotalSaidasPassivas() {
        totalSaidasPassivas = 0.0
        for(Saida saida in saidasPassivas){
            totalSaidasPassivas+=saida.valor
        }
        return totalSaidasPassivas
    }

    BigDecimal getSaldo() {
        return getTotalParcelas()-getTotalSaidas()
    }

    void setSaldo(BigDecimal saldo) {
        this.saldo = saldo
    }

    Utilizador getUtilizador() {
        return  utilizador
    }

    BigDecimal getTotalParcelas() {
        totalParcelas = 0.0
        for(Parcela parcela in parcelas){
            totalParcelas +=parcela.valorPago
        }
        return totalParcelas
    }

    BigDecimal getTotalSaidas() {
        totalSaidas = 0.0
        for(Saida saida in saidas){
            totalSaidas +=saida.valor
        }
        return totalSaidas
    }

    List<Parcela> getParcelas() {
        if (parcelas==null){
            parcelas = new ListModelList<Parcela>(Parcela.findAllByValorPagoGreaterThanAndDiarioAndUtilizador(0.0,diario,utilizador))
        }
       return parcelas
    }

    List<Saida> getSaidas() {
       if(saidas==null){
           saidas = new ListModelList<Saida>(Saida.findAllByDiarioAndUtilizador(diario,utilizador))
       }
        return saidas
    }

    List<Saida> getSaidasAtivas() {
        if(saidasAtivas==null){
            saidasAtivas = new ListModelList<Saida>()
        }
        saidas = new ListModelList<Saida>(Saida.findAllByDiarioAndUtilizador(diario,utilizador))
        for(Saida saida in saidas){
            if(saida.valor>0){
                if(saida.destino.ativo){
                    saidasAtivas.add(saida)
                }
            }

        }
        return saidasAtivas
    }


    List<Saida> getSaidasPassivas() {
        if(saidasPassivas==null){
            saidasPassivas = new ListModelList<Saida>()
        }
        saidas = new ListModelList<Saida>(Saida.findAllByDiarioAndUtilizador(diario,utilizador))
        for(Saida saida in saidas){
            if(!saida.destino.ativo){
                saidasPassivas.add(saida)
            }

        }
        return saidasPassivas
    }

    Diario getDiario() {

        return diario
    }

    void setDiario(Diario diario) {
        this.diario = diario
    }
    @Init init() {
        diario = Diario.findById(composerService.diario.id)
        utilizador = Utilizador.findByUsername(composerService.utilizador)

    }


}
