package mz.maleyanga.cliente

import mz.maleyanga.conta.Conta
import mz.maleyanga.credito.Credito
import mz.maleyanga.documento.Anexo
import mz.maleyanga.entidade.Entidade
import mz.maleyanga.pagamento.Pagamento
import mz.maleyanga.pedidoDeCredito.PedidoDeCredito
import mz.maleyanga.security.Utilizador


class Cliente implements Serializable {
    private static final long serialVersionUID = 1
    static searchable = true
    String nome
    String nuit
    String tipoDeIndentificacao
    String numeroDeIndentificao
    String residencia
    String email
    String localDeTrabalho
    String telefone
    String telefone1
    String telefone2
    Entidade entidade
    Date dataDeExpiracao
    String estadoCivil
    boolean ativo = true
    Utilizador utilizador
    String classificacao = "medio"
    Anexo anexo
    Date lastUpdated
    Date dateCreated

    String getClassificacao() {
        Date data = new Date()
        for (Credito c in creditos) {
            if (c.emDivida) {

                if (c.pagamentos.findAll { it.diasDeMora > 60 }) {
                    classificacao = "pessimo"
                } else if (c.pagamentos.findAll { it.diasDeMora > 30 }) {
                    classificacao = "mau"
                }


            } else if (!c.emDivida) {

                for (Pagamento p in c.pagamentos) {
                    if (p.pago) {
                        if (p.diasDeMora == 0) {
                            classificacao = "excelente"
                        }
                    }
                    if (p.pago) {
                        if (p.diasDeMora > 30) {
                            classificacao = "bom"
                        }
                    }
                }
            }
        }
        return classificacao
    }
    static hasMany = [pedidosDeCredito: PedidoDeCredito, creditos: Credito, anexos: Anexo, contas: Conta, assinantes: Assinante]


    static mapping = {
        id generator: 'increment'
        assinantes lazy: false
        pedidosDeCredito lazy: false
    }
    static constraints = {
        /* attachment nullable: true
         fileName nullable: true, display: false
         fileType nullable: true, display:false*/

        nome nullable: false, unique: true, blank: false
        nuit nullable: true, unique: true
        dataDeExpiracao nullable: true
        numeroDeIndentificao nullable: false
        residencia nullable: false
        email nullable: true
        telefone nullable: true, maxSize: 9
        telefone1 nullable: true, maxSize: 9
        telefone2 nullable: true, maxSize: 9
        tipoDeIndentificacao inList: ["BI", "Passaporte", "Carta de conducao", "Outro"]
        estadoCivil inList: ["Solteiro", "Solteira", "Casado", "Casada", "Separado Judicialmente", "Separado Judicialmente", "Outro"]
        classificacao inList: ["excelente", "bom", "medio", "mau", "pessimo"], nullable: true
        entidade nullable: false
        anexos nullable: true
        ativo nullable: false
        assinantes nullable: true
        utilizador nullable: true
        localDeTrabalho nullable: true
        anexo nullable: true

    }

    String toString() {
        return "${nome}"
    }

}
