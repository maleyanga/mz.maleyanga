package mz.maleyanga.sms

/**
 * Ozekimessageout
 * A domain class describes the data object and it's mapping to the database
 */
class Ozekimessagein {

    String sender
    String receiver
    String msg
    String senttime
    String receivedtime
    String reference
     String operator
    String errorsmg
    String msgtype


	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	/* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated
	
//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
    }
    
	static	constraints = {
        sender maxSize: 30
        receiver maxSize: 30
        msg maxSize: 160
        senttime maxSize: 100
        receivedtime maxSize: 100
        reference maxSize: 100
        operator maxSize: 60
        errorsmg maxSize: 250
        msgtype maxSize: 150

    }
	
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
