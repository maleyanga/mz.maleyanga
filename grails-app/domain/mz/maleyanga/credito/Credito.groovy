package mz.maleyanga.credito

import mz.maleyanga.cliente.Cliente
import mz.maleyanga.documento.Anexo
import mz.maleyanga.pagamento.Pagamento
import mz.maleyanga.security.Utilizador

/**
 * Credito
 * A domain class describes the data object and it's mapping to the database
 */
class Credito implements Serializable {
    private static final long serialVersionUID = 1
    static searchable = true
    BigDecimal valorCreditado = 0.0
    Date dateCreated
    Date lastUpdated
    Date dateConcecao
    Integer moras = 0
    Date validade
    String numeroDoCredito
    BigDecimal percentualDejuros = 25
    BigDecimal percentualJurosDeDemora = 2
    BigDecimal taxas = 0
    String estado
    String formaDeCalculo       // Determina a formula usada no calculo das prestaçoes
    // pmt é a formula universal para calculo de juros
    // taxafixa somento calcula o juros sobre o valor total e divide pelo numero de prestacoes
    String periodicidade
    boolean emDivida = true
    boolean invalido = false
    int numeroDePrestacoes
    Cliente cliente
    boolean reterCapital
    Utilizador utilizador

    String getEstado() {
        if (!emDivida) {
            estado = "Fechado"
        }
        if (emDivida) {
            if (pagamentos.findAll { it.diasDeMora > 0 }) {
                estado = "Pendente"
            } else estado = "Aberto"
            if (pagamentos.findAll { it.pago }) {
                estado = "EmProgresso"
            }
        }
        return estado
    }

    boolean getEmDivida() {

        for (Pagamento p in pagamentos) {
            if (!p.pago) {
                return true
            }
        }
        return false
    }
    static hasMany = [pagamentos: Pagamento, anexos: Anexo]


    static mapping = {
        cliente lazy: false
        id generator: 'increment'

    }

    static constraints = {

        moras nullable: true
        validade nullable: true
        anexos nullable: true
        utilizador nullable: true
        lastUpdated nullable: true
        taxas nullable: true
        valorCreditado nullable: false
        percentualDejuros nullable: false, min: 0.0, max: 100.0, scale: 2
        estado nullable: false, inList: ['Aberto', 'Pendente', 'Fechado', 'EmProgresso']
        periodicidade nullable: false, inList: ['mensal', 'quinzenal', 'semanal', 'diario', 'doisdias']
        formaDeCalculo nullable: false, inList: ['pmt', 'taxafixa']
        cliente nullable: false
        numeroDePrestacoes(validator: {
            return it > 0
        })

    }

    public String toString() {
        return "${"id::" + id + ".valor::" + valorCreditado}"
    }


}
