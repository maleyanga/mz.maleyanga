package mz.maleyanga.diario

/**
 * Diario
 * A domain class describes the data object and it's mapping to the database
 */
class Diario {
    String estado
    String numeroDoDiario
    Date dateCreated
    Date lastUpdated
    Date dateClosed


//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
    // static hasMany = [transferencias: Transferencia]
    // tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping

    static mapping = {
        parcelas lazy: false
        transferencias lazy: false
        saidas lazy: false
    }

    static constraints = {
        estado inList: ["aberto", "fechado", "pendente"]
        lastUpdated nullable: true
        dateClosed nullable: true
        estado inList: ["aberto", "fechado", "pendente"]
        numeroDoDiario unique: true

    }

    /*
     * Methods of the Domain Class
     */
//	@Override	// Override toString for a nicer / more descriptive UI
//	public String toString() {
//		return "${name}";
//	}
}
