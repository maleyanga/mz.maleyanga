package utils
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.HttpResponseException
import groovyx.net.http.Method

import static groovyx.net.http.ContentType.*
class ExternalApi {
    static def postRequest(String baseUrl, String path, def query, method = Method.POST) {
        try {
            def ret = null
            // Using HTTP builder for the rest call
            def http = new HTTPBuilder(baseUrl)
            def uri
            def requestContentType
            def body
            def response
            // perform a POST request, expecting TEXT response
            http.request(method, TEXT) {
                uri.path = path
                requestContentType = URLENC
                body =  [xmlRequest: query]

                // response handler for a success response code
                response.success = { resp, reader ->
                    ret = reader.getText()
                }
            }
            return ret

        } catch (HttpResponseException ex) {
            ex.printStackTrace()
            return null
        } catch (ConnectException ex) {
            ex.printStackTrace()
            return null
        }
    }

}
