package mz.maleyanga.sms



import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * OzekimessageoutController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
class OzekimessageoutController extends BasicController {

    static allowedMethods = [save: ["POST","PUT"], update: ["PUT","POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN','OZELIMESSAGEOUT_INDEX'])
	def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Ozekimessageout.list(params), model:[ozekimessageoutInstanceCount: Ozekimessageout.count()]
    }

    @Secured(['ROLE_ADMIN','OZELIMESSAGEOUT_LIST'])
	def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Ozekimessageout.list(params), model:[ozekimessageoutInstanceCount: Ozekimessageout.count()]
    }

    @Secured(['ROLE_ADMIN','OZELIMESSAGEOUT_SHOW'])
    def show(Ozekimessageout ozekimessageoutInstance) {
        updateCurrentAction('show')
        respond ozekimessageoutInstance
    }

    @Secured(['ROLE_ADMIN','OZELIMESSAGEOUT_CREATE'])
      def create() {
        updateCurrentAction('create')
        respond new Ozekimessageout(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','OZELIMESSAGEOUT_SAVE'])
    def save(Ozekimessageout ozekimessageoutInstance) {
        if (ozekimessageoutInstance == null) {
            notFound()
            return
        }

        if (ozekimessageoutInstance.hasErrors()) {
            respond ozekimessageoutInstance.errors, view:'create'
            return
        }

        ozekimessageoutInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'ozekimessageoutInstance.label', default: 'Ozekimessageout'), ozekimessageoutInstance.id])
                redirect ozekimessageoutInstance
            }
            '*' { respond ozekimessageoutInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','OZELIMESSAGEOUT_EDIT'])
    def edit(Ozekimessageout ozekimessageoutInstance) {
        updateCurrentAction('edit')
        respond ozekimessageoutInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','OZELIMESSAGEOUT_UPDATE'])
    def update(Ozekimessageout ozekimessageoutInstance) {
        if (ozekimessageoutInstance == null) {
            notFound()
            return
        }

        if (ozekimessageoutInstance.hasErrors()) {
            respond ozekimessageoutInstance.errors, view:'edit'
            return
        }

        ozekimessageoutInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Ozekimessageout.label', default: 'Ozekimessageout'), ozekimessageoutInstance.id])
                redirect ozekimessageoutInstance
            }
            '*'{ respond ozekimessageoutInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','OZELIMESSAGEOUT_DELETE'])
    def delete(Ozekimessageout ozekimessageoutInstance) {

        if (ozekimessageoutInstance == null) {
            notFound()
            return
        }

        ozekimessageoutInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Ozekimessageout.label', default: 'Ozekimessageout'), ozekimessageoutInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'ozekimessageoutInstance.label', default: 'Ozekimessageout'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
