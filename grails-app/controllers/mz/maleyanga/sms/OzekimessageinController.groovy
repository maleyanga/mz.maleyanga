package mz.maleyanga.sms



import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * OzekimessageinController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
class OzekimessageinController extends BasicController {

    static allowedMethods = [save: ["POST","PUT"], update: ["PUT","POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN','OZELIMESSAGEIN_INDEX'])
	def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Ozekimessagein.list(params), model:[ozekimessageinInstanceCount: Ozekimessagein.count()]
    }

    @Secured(['ROLE_ADMIN','OZELIMESSAGEIN_LIST'])
	def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Ozekimessagein.list(params), model:[ozekimessageinInstanceCount: Ozekimessagein.count()]
    }

    @Secured(['ROLE_ADMIN','OZELIMESSAGEIN_SHOW'])
    def show(Ozekimessagein ozekimessageinInstance) {
        updateCurrentAction('show')
        respond ozekimessageinInstance
    }

    @Secured(['ROLE_ADMIN','OZELIMESSAGEIN_CREATE'])
    def create() {
        updateCurrentAction('create')
        respond new Ozekimessagein(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','OZELIMESSAGEIN_SAVE'])
    def save(Ozekimessagein ozekimessageinInstance) {
        if (ozekimessageinInstance == null) {
            notFound()
            return
        }

        if (ozekimessageinInstance.hasErrors()) {
            respond ozekimessageinInstance.errors, view:'create'
            return
        }

        ozekimessageinInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'ozekimessageinInstance.label', default: 'Ozekimessagein'), ozekimessageinInstance.id])
                redirect ozekimessageinInstance
            }
            '*' { respond ozekimessageinInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','OZELIMESSAGEIN_EDIT'])
    def edit(Ozekimessagein ozekimessageinInstance) {
        updateCurrentAction('edit')
        respond ozekimessageinInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','OZELIMESSAGEIN_UPDATE'])
    def update(Ozekimessagein ozekimessageinInstance) {
        if (ozekimessageinInstance == null) {
            notFound()
            return
        }

        if (ozekimessageinInstance.hasErrors()) {
            respond ozekimessageinInstance.errors, view:'edit'
            return
        }

        ozekimessageinInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Ozekimessagein.label', default: 'Ozekimessagein'), ozekimessageinInstance.id])
                redirect ozekimessageinInstance
            }
            '*'{ respond ozekimessageinInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','OZELIMESSAGEIN_DELETE'])
    def delete(Ozekimessagein ozekimessageinInstance) {

        if (ozekimessageinInstance == null) {
            notFound()
            return
        }

        ozekimessageinInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Ozekimessagein.label', default: 'Ozekimessagein'), ozekimessageinInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'ozekimessageinInstance.label', default: 'Ozekimessagein'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
