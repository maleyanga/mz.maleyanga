package mz.maleyanga.sms



import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * SmslogController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
class SmslogController extends BasicController {

    static allowedMethods = [save: ["POST","PUT"], update: ["PUT","POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN','SMS_LOG_INDEX'])
	def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Smslog.list(params), model:[smslogInstanceCount: Smslog.count()]
    }

    @Secured(['ROLE_ADMIN','SMS_LOG_LIST'])
	def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Smslog.list(params), model:[smslogInstanceCount: Smslog.count()]
    }

    @Secured(['ROLE_ADMIN','SMS_LOG_SHOW'])
    def show(Smslog smslogInstance) {
        updateCurrentAction('show')
        respond smslogInstance
    }

    @Secured(['ROLE_ADMIN','SMS_LOG_CREATE'])
     def create() {
        updateCurrentAction('create')
        respond new Smslog(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','SMS_LOG_SAVE'])
    def save(Smslog smslogInstance) {
        if (smslogInstance == null) {
            notFound()
            return
        }

        if (smslogInstance.hasErrors()) {
            respond smslogInstance.errors, view:'create'
            return
        }

        smslogInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'smslogInstance.label', default: 'Smslog'), smslogInstance.id])
                redirect smslogInstance
            }
            '*' { respond smslogInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','SMS_LOG_EDIT'])
    def edit(Smslog smslogInstance) {
        updateCurrentAction('edit')
        respond smslogInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','SMS_LOG_UPDATE'])
    def update(Smslog smslogInstance) {
        if (smslogInstance == null) {
            notFound()
            return
        }

        if (smslogInstance.hasErrors()) {
            respond smslogInstance.errors, view:'edit'
            return
        }

        smslogInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Smslog.label', default: 'Smslog'), smslogInstance.id])
                redirect smslogInstance
            }
            '*'{ respond smslogInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','SMS_LOG_DELETE'])
    def delete(Smslog smslogInstance) {

        if (smslogInstance == null) {
            notFound()
            return
        }

        smslogInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Smslog.label', default: 'Smslog'), smslogInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'smslogInstance.label', default: 'Smslog'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
