package mz.maleyanga.documento

import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.mirzsoft.grails.actionlogging.annotation.ActionLogging
import org.mirzsoft.grails.actionlogging.annotation.SpringUserIdentification
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * AnexoController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
@ActionLogging
@SpringUserIdentification
class AnexoController extends BasicController {

    static allowedMethods = [save: ["POST","PUT"], update: ["PUT","POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN','ANEXO_INDEX'])
	def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Anexo.list(params), model:[anexoInstanceCount: Anexo.count()]
    }

    @Secured(['ROLE_ADMIN','ANEXO_LIST'])
	def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Anexo.list(params), model:[anexoInstanceCount: Anexo.count()]
    }

    @Secured(['ROLE_ADMIN','ANEXO_SHOW'])
    def show(Anexo anexoInstance) {
        updateCurrentAction('show')
        respond anexoInstance
    }

    @Secured(['ROLE_ADMIN','ANEXO_CREATE'])
    def create() {
        updateCurrentAction('create')
        respond new Anexo(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','ANEXO_SAVE'])
    def save(Anexo anexoInstance) {

        if (anexoInstance == null) {
            notFound()
            return
        }

        if (anexoInstance.hasErrors()) {
            respond anexoInstance.errors, view:'create'
            return
        }
        def uploadedFile = request.getFile('attachment')
        anexoInstance.attachment = uploadedFile?.getBytes() //converting the file to bytes
        anexoInstance.fileName=  uploadedFile.originalFilename //getting the file name from the uploaded file
        anexoInstance.fileType = uploadedFile.contentType//getting and storing the file type

        anexoInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'anexoInstance.label', default: 'Anexo'), anexoInstance.id])
                redirect anexoInstance
            }
            '*' { respond anexoInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','ANEXO_EDIT'])
    def edit(Anexo anexoInstance) {
        updateCurrentAction('edit')
        respond anexoInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','ANEXO_UPDATE'])
    def update(Anexo anexoInstance) {
        if (anexoInstance == null) {
            notFound()
            return
        }

        if (anexoInstance.hasErrors()) {
            respond anexoInstance.errors, view:'edit'
            return
        }

        anexoInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Anexo.label', default: 'Anexo'), anexoInstance.id])
                redirect anexoInstance
            }
            '*'{ respond anexoInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','ANEXO_DELETE'])
    def delete(Anexo anexoInstance) {

        if (anexoInstance == null) {
            notFound()
            return
        }

        anexoInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Anexo.label', default: 'Anexo'), anexoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'anexoInstance.label', default: 'Anexo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
