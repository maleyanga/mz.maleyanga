package mz.maleyanga.documento



import grails.transaction.Transactional
import mz.maleyanga.BasicController
import mz.maleyanga.pagamento.Pagamento
import mz.maleyanga.pagamento.PagamentoController
import mz.maleyanga.security.Utilizador
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * NotaController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)

class NotaController extends BasicController {
    def springSecurityService
    def pagamentoService

    static allowedMethods = [save: ["POST","PUT"], update: ["PUT","POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN','NOTA_INDEX'])
	def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Nota.list(params), model:[notaInstanceCount: Nota.count()]
    }

    @Secured(['ROLE_ADMIN','NOTA_LIST'])
	def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Nota.list(params), model:[notaInstanceCount: Nota.count()]
    }

    @Secured(['ROLE_ADMIN','NOTA_SHOW'])
    def show(Nota notaInstance) {
        updateCurrentAction('show')
        respond notaInstance
    }

    @Secured(['ROLE_ADMIN','NOTA_CREATE'])
    def create() {
        updateCurrentAction('create')
       // def user = springSecurityService.currentUser
        respond new Nota(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','NOTA_SAVE'])
    def save(Nota notaInstance) {
       // Pagamento pagamento = session.getAttribute("pagamento") as Pagamento
      //  pagamentoService.pagamentoInstance.addToNotas(notaInstance)
        notaInstance.pagamento=pagamentoService.pagamentoInstance
        Utilizador utilizador =  Utilizador.findById(springSecurityService.principal?.id)
        String autor = utilizador.username.toString()+""
        BigInteger bigInt = new BigInteger(autor.getBytes())
        int digits1 =Integer.parseInt( String.valueOf(bigInt).substring(1,4))
        int digits2 =Integer.parseInt( String.valueOf(bigInt).substring(0,3))
        notaInstance.setColorR(digits1)
        notaInstance.setColorG(digits2)



        notaInstance.setAutor(autor)
        System.println("autor da nota: "+notaInstance.autor)
        System.println("color da nota: "+notaInstance.colorR)
        System.println("color da nota: "+notaInstance.colorG)
        if (notaInstance == null) {
            notFound()
            return
        }
/*

        if (notaInstance.hasErrors()) {
            respond notaInstance.errors, view:'create'
            return
        }
*/

        pagamentoService.pagamentoInstance.save()
        notaInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'notaInstance.label', default: 'Nota'), notaInstance.id])


            }
            '*'
        }
        System.println(pagamentoService.pagamentoInstance.id)
        redirect(controller: "pagamento", action: "show",id: pagamentoService.pagamentoInstance.id)
    }

    @Secured(['ROLE_ADMIN','NOTA_EDIT'])
    def edit(Nota notaInstance) {
        updateCurrentAction('edit')
        respond notaInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','NOTA_UPDATE'])
    def update(Nota notaInstance) {
        if (notaInstance == null) {
            notFound()
            return
        }

        if (notaInstance.hasErrors()) {
            respond notaInstance.errors, view:'edit'
            return
        }

        notaInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Nota.label', default: 'Nota'), notaInstance.id])
                redirect notaInstance
            }
            '*'{ respond notaInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','NOTA_DELETE'])
    def delete(Nota notaInstance) {

        if (notaInstance == null) {
            notFound()
            return
        }

        notaInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Nota.label', default: 'Nota'), notaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'notaInstance.label', default: 'Nota'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
