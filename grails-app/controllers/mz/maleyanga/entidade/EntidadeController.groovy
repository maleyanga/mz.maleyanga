package mz.maleyanga.entidade

import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.mirzsoft.grails.actionlogging.annotation.ActionLogging
import org.mirzsoft.grails.actionlogging.annotation.SpringUserIdentification
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * EntidadeController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
@ActionLogging
@SpringUserIdentification
class EntidadeController extends BasicController {

    static allowedMethods = [save: ["POST","PUT"], update: ["PUT","POST"], delete: "DELETE"]
    @Secured(['ROLE_ADMIN','ENTIDADE_INDEX'])
	def index(Integer max) {

        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Entidade.list(params), model:[entidadeInstanceCount: Entidade.count()]
    }

    @Secured(['ROLE_ADMIN','ENTIDADE_SELECT'])
    def select(Integer max) {
        updateCurrentAction('select')
        params.max = Math.min(max ?: 10, 100)
        respond Entidade.list(params), model:[entidadeInstanceCount: Entidade.count()]
    }

    @Secured(['ROLE_ADMIN','ENTIDADE_GO_TO_HOME'])
    def goToHome (Entidade entidadeInstance){
        session.setAttribute("entidade",entidadeInstance)
        redirect(controller: 'home', action: 'index')
    }

    @Secured(['ROLE_ADMIN','ENTIDADE_LIST'])
	def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Entidade.list(params), model:[entidadeInstanceCount: Entidade.count()]
    }

    @Secured(['ROLE_ADMIN','ENTIDADE_SHOW'])
    def show(Entidade entidadeInstance) {
        session.setAttribute('entidade', entidadeInstance)
        updateCurrentAction('show')
        respond entidadeInstance
    }

    @Secured(['ROLE_ADMIN','ENTIDADE_CREATE'])
    def create() {
        updateCurrentAction('create')
        respond new Entidade(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','ENTIDADE_SAVE'])
    def save(Entidade entidadeInstance) {
        System.println(entidadeInstance.nome)
        if (entidadeInstance == null) {
            notFound()
            return
        }

        if (entidadeInstance.hasErrors()) {
            respond entidadeInstance.errors, view:'create'
            return
        }

        entidadeInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'entidadeInstance.label', default: 'Entidade'), entidadeInstance.id])
                redirect entidadeInstance
            }
            '*' { respond entidadeInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','ENTIDADE_EDIT'])
    def edit(Entidade entidadeInstance) {
        updateCurrentAction('edit')
        respond entidadeInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','ENTIDADE_UPDATE'])
    def update(Entidade entidadeInstance) {
        if (entidadeInstance == null) {
            notFound()
            return
        }

        if (entidadeInstance.hasErrors()) {
            respond entidadeInstance.errors, view:'edit'
            return
        }

        entidadeInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Entidade.label', default: 'Entidade'), entidadeInstance.id])
                redirect entidadeInstance
            }
            '*'{ respond entidadeInstance, [status: OK] }
        }
    }

    @Transactional
        @Secured(['ROLE_ADMIN','ENTIDADE_DELETE'])
    def delete(Entidade entidadeInstance) {

        if (entidadeInstance == null) {
            notFound()
            return
        }

        entidadeInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Entidade.label', default: 'Entidade'), entidadeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'entidadeInstance.label', default: 'Entidade'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
