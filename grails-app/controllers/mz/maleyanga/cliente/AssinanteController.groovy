package mz.maleyanga.cliente

import grails.transaction.Transactional
import mz.maleyanga.BasicController
import mz.maleyanga.security.Utilizador
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * AssinanteController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
class AssinanteController extends BasicController {
    def springSecurityService
    static allowedMethods = [save: ["POST", "PUT"], update: ["PUT", "POST"], delete: ["DELETE"]]

    @Secured(['ROLE_ADMIN','CLIENTE_INDEX'])
    def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Assinante.list(params), model: [assinanteInstanceCount: Assinante.count()]
    }

    @Secured(['ROLE_ADMIN','CLIENTE_LIST'])
    def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Assinante.list(params), model: [assinanteInstanceCount: Assinante.count()]
    }

    @Secured(['ROLE_ADMIN','CLIENTE_SHOW'])
    def show(Assinante assinanteInstance) {
        updateCurrentAction('show')
        respond assinanteInstance
    }

    @Secured(['ROLE_ADMIN','CLIENTE_CREATE'])
    def create() {
        updateCurrentAction('create')
        respond new Assinante(params)
    }



    @Transactional
    @Secured(['ROLE_ADMIN','CLIENTE_SAVE'])
    def save(Assinante assinanteInstance) {
        def utilizador = Utilizador.findById(springSecurityService.principal.id)
        assinanteInstance.setUtilizador(utilizador)
        if (assinanteInstance == null) {
            notFound()
            return
        }

        if (assinanteInstance.hasErrors()) {
            respond assinanteInstance.errors, view: 'create'
            return
        }
        Cliente cliente = Cliente.find(assinanteInstance.cliente)
        cliente.setAssinante(assinanteInstance)
        cliente.save()
        assinanteInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'assinanteInstance.label', default: 'Assinante'), assinanteInstance.id])
                redirect assinanteInstance
            }
            '*' { respond assinanteInstance, [status: CREATED] }
        }


    }
    @Transactional
    @Secured(['ROLE_ADMIN','CLIENTE_REMOTE_SAVE'])
    def remoteSave(Assinante assinanteInstance) {


        if (assinanteInstance == null) {
            notFound()
            return
        }

        if (assinanteInstance.hasErrors()) {
            respond assinanteInstance.errors, view: 'create'
            return
        }

        assinanteInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'assinanteInstance.label', default: 'Assinante'), assinanteInstance.id])
                redirect (controller: "cliente", action: "create")
            }
            '*' { redirect(controller: "cliente",action: "create"); [status: CREATED] }
        }


    }

    @Secured(['ROLE_ADMIN','CLIENTE_EDIT'])
    def edit(Assinante assinanteInstance) {
        updateCurrentAction('edit')
        respond assinanteInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','CLIENTE_UPDATE'])
    def update(Assinante assinanteInstance) {
        def utilizador = Utilizador.findById(springSecurityService.principal.id)
        assinanteInstance.setUtilizador(utilizador)
        if (assinanteInstance == null) {
            notFound()
            return
        }

        if (assinanteInstance.hasErrors()) {
            respond assinanteInstance.errors, view: 'edit'
            return
        }

        assinanteInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Assinante.label', default: 'Assinante'), assinanteInstance.id])
                redirect assinanteInstance
            }
            '*' { respond assinanteInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','CLIENTE_DELETE'])
    def delete(Assinante assinanteInstance) {

        if (assinanteInstance == null) {
            notFound()
            return
        }

        assinanteInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Assinante.label', default: 'Assinante'), assinanteInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'assinanteInstance.label', default: 'Assinante'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
