package mz.maleyanga.cliente

import grails.transaction.Transactional
import mz.maleyanga.BasicController
import mz.maleyanga.SMSService
import mz.maleyanga.conta.Conta
import mz.maleyanga.credito.Credito
import mz.maleyanga.documento.Anexo
import mz.maleyanga.pagamento.Pagamento
import mz.maleyanga.security.Utilizador
import mz.maleyanga.settings.Settings
import org.mirzsoft.grails.actionlogging.annotation.ActionLogging
import org.mirzsoft.grails.actionlogging.annotation.SpringUserIdentification
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * ClienteController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
@ActionLogging
@SpringUserIdentification
class ClienteController extends BasicController {
    SMSService smsService
    def springSecurityService
    static allowedMethods = [save: ["POST", "PUT"], update: ["PUT", "POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN', 'ROLE_ASSIS_ADMIN', 'CLIENTE_GESTOR'])
    def gestorDeClientes() {

    }

    @Secured(['ROLE_ADMIN', 'ROLE_ASSIS_ADMIN', 'CLIENTE_GESTOR'])
    def novoCliente() {

    }

    @Secured(['ROLE_ADMIN', 'ROLE_ASSIS_ADMIN', 'ROLE_GESTOR_CREDITO'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        ArrayList clientes = new ArrayList()
        def clientesDB = Cliente.all
        for (Cliente cliente in clientesDB) {
            clientes.add(cliente)
        }

        respond Cliente.list(params), model: [clienteInstanceCount: Cliente.count(), clientes: clientes]
    }
    @Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
	def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def clientes = Cliente.all
        Cliente cliente = new Cliente()

        respond Cliente.list(params), model:[clienteInstanceCount: Cliente.count(),clientes:clientes]
    }

    def blackList(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def blacklist = Cliente.findAllByAtivo(false)
        Cliente cliente = new Cliente()

        respond Cliente.list(params), model: [clienteInstanceCount: Cliente.count(), clientes: blacklist]
    }
	def listPessimos(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def clientes = Cliente.findAllByClassificacao("pessimo")
        Cliente cliente = new Cliente()
        respond clientes, model:[clienteInstanceCount: Cliente.count(),clientes:clientes]
    }
	def listMaus(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def clientes = Cliente.findAllByClassificacao("mau")
        Cliente cliente = new Cliente()

        respond clientes, model:[clienteInstanceCount: Cliente.count(),clientes:clientes]
    }
	def listMedios(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def clientes = Cliente.findAllByClassificacao("medio")
        Cliente cliente = new Cliente()

        respond clientes, model:[clienteInstanceCount: Cliente.count(),clientes:clientes]
    }
	def listBom(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def clientes = Cliente.findAllByClassificacao("bom")
        Cliente cliente = new Cliente()

        respond clientes, model:[clienteInstanceCount: Cliente.count(),clientes:clientes]
    }
	def listExcelente(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def clientes = Cliente.findAllByClassificacao("excelente")
        respond clientes, model:[clienteInstanceCount: Cliente.count(),clientes:clientes]
    }


    @Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
    def show(Cliente clienteInstance) {
        session.setAttribute("cliente",clienteInstance)
        def total = 0.0
        for (Credito c in clienteInstance.creditos) {
            total += c.valorCreditado
        }
        respond clienteInstance, model: [total: total]
    }
    @Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
    def create() {


        respond new Cliente(params)

    }

    @Transactional
    @Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN'])
    def save(Cliente clienteInstance) {

        def agora = new Date()
        Calendar c = Calendar.getInstance()
        c.setTime(agora)

        if (clienteInstance == null) {
            notFound()
            return
        }
        if (clienteInstance.nuit == null) {
            flash.message = "O valor do NUIT não deve ser nulo"
            return

        }


        if (clienteInstance.hasErrors()) {
            respond clienteInstance.errors, view: 'create'
            return
        }
        /*   Anexo anexo0 = new Anexo()
           Anexo anexo1 = new Anexo()
           Anexo anexo2 = new Anexo()
           Anexo anexo3 = new Anexo()
           Anexo anexo4 = new Anexo()
           Anexo anexo5 = new Anexo()
           Anexo anexo6 = new Anexo()
           Anexo anexo7 = new Anexo()
           Anexo anexo8 = new Anexo()
           Anexo anexo9 = new Anexo()
           Anexo anexo10 = new Anexo()*/
/*
        def uploadedFile0 = request.getFile('attachment0')
        anexo0.attachment = uploadedFile0?.getBytes() //converting the file to bytes
        anexo0.fileName=  uploadedFile0.originalFilename //getting the file name from the uploaded file
        anexo0.fileType = uploadedFile0.contentType//getting and storing the file type
        anexo0.setCliente(clienteInstance)

        def uploadedFile1 = request.getFile('attachment1')
        anexo1.attachment = uploadedFile1?.getBytes() //converting the file to bytes
        anexo1.fileName=  uploadedFile1.originalFilename //getting the file name from the uploaded file
        anexo1.fileType = uploadedFile1.contentType//getting and storing the file type
        anexo1.setCliente(clienteInstance)

        def uploadedFile2 = request.getFile('attachment2')
        anexo2.attachment = uploadedFile2?.getBytes() //converting the file to bytes
        anexo2.fileName=  uploadedFile2.originalFilename //getting the file name from the uploaded file
        anexo2.fileType = uploadedFile2.contentType//getting and storing the file type
        anexo2.setCliente(clienteInstance)
        def uploadedFile3 = request.getFile('attachment3')
        anexo3.attachment = uploadedFile3?.getBytes() //converting the file to bytes
        anexo3.fileName=  uploadedFile3.originalFilename //getting the file name from the uploaded file
        anexo3.fileType = uploadedFile3.contentType//getting and storing the file type
        anexo3.setCliente(clienteInstance)

        def uploadedFile4 = request.getFile('attachment4')
        anexo4.attachment = uploadedFile4?.getBytes() //converting the file to bytes
        anexo4.fileName=  uploadedFile4.originalFilename //getting the file name from the uploaded file
        anexo4.fileType = uploadedFile4.contentType//getting and storing the file type
        anexo4.setCliente(clienteInstance)

        def uploadedFile5 = request.getFile('attachment5')
        anexo5.attachment = uploadedFile5?.getBytes() //converting the file to bytes
        anexo5.fileName=  uploadedFile5.originalFilename //getting the file name from the uploaded file
        anexo5.fileType = uploadedFile5.contentType//getting and storing the file type
        anexo5.setCliente(clienteInstance)

        def uploadedFile6 = request.getFile('attachment6')
        anexo6.attachment = uploadedFile6?.getBytes() //converting the file to bytes
        anexo6.fileName=  uploadedFile6.originalFilename //getting the file name from the uploaded file
        anexo6.fileType = uploadedFile6.contentType//getting and storing the file type
        anexo6.setCliente(clienteInstance)

        def uploadedFile7 = request.getFile('attachment7')
        anexo7.attachment = uploadedFile7?.getBytes() //converting the file to bytes
        anexo7.fileName=  uploadedFile7.originalFilename //getting the file name from the uploaded file
        anexo7.fileType = uploadedFile7.contentType//getting and storing the file type
        anexo7.setCliente(clienteInstance)

        def uploadedFile8 = request.getFile('attachment8')
        anexo8.attachment = uploadedFile8?.getBytes() //converting the file to bytes
        anexo8.fileName=  uploadedFile8.originalFilename //getting the file name from the uploaded file
        anexo8.fileType = uploadedFile8.contentType//getting and storing the file type
        anexo8.setCliente(clienteInstance)

        def uploadedFile9 = request.getFile('attachment9')
        anexo9.attachment = uploadedFile9?.getBytes() //converting the file to bytes
        anexo9.fileName=  uploadedFile9.originalFilename //getting the file name from the uploaded file
        anexo9.fileType = uploadedFile9.contentType//getting and storing the file type
        anexo9.setCliente(clienteInstance)

        def uploadedFile10 = request.getFile('inputFile')
        anexo10.attachment = uploadedFile10?.getBytes() //converting the file to bytes
        anexo10.fileName = uploadedFile10.originalFilename //getting the file name from the uploaded file
        anexo10.fileType = uploadedFile10.contentType//getting and storing the file type
        anexo10.setCliente(clienteInstance)
        clienteInstance?.addToAnexos(anexo0)
        clienteInstance.anexos?.add(anexo1)
        clienteInstance.anexos?.add(anexo2)
        clienteInstance.anexos?.add(anexo3)
        clienteInstance.anexos?.add(anexo4)
        clienteInstance.anexos?.add(anexo5)
        clienteInstance.anexos?.add(anexo6)
        clienteInstance.anexos?.add(anexo7)
        clienteInstance.anexos?.add(anexo8)
        clienteInstance.anexos?.add(anexo9)
        anexo10.save()
        clienteInstance.setAnexo(anexo10)*/

        clienteInstance.save flush: true
        def clienteDb = Cliente.findById(clienteInstance.id)
        if (clienteDb != null) {
            System.println("clienteDb=" + clienteDb)
            Conta conta = new Conta()
            conta.numeroDaConta = clienteDb.id
            Integer cod = clienteDb.id.toInteger()
            String str = String.format("%04d", cod)
            def contaDb = Conta.findByFinalidadeAndDesignacaoDaConta("conta_integradora", "CLIENTES")
            if (contaDb == null) {
                flash.message = "Por favar de criar uma conta integradora com a descrição 'CLIENTES' "
                return
            } else {
                conta.codigo = contaDb.codigo + "." + str
                conta.designacaoDaConta = "conta_cliente" + '_' + conta.codigo
                conta.finalidade = 'conta_cliente'
                conta.conta = contaDb
                conta.ativo = contaDb.ativo
                conta.save(flush: true)
                if (Conta.findById(conta.id) != null) {
                    System.println("conta==" + conta)
                }
            }

        }


        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'clienteInstance.label', default: 'Cliente'), clienteInstance.id])
                redirect clienteInstance
            }
            '*' { respond clienteInstance, [status: CREATED] }
        }

        flash.message = "O Cliente No."+clienteInstance.id+" foi criado com sucesso!"
        //  redirect(controller: "cliente", action: "show", id: clienteInstance.id)
    }

    @Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN'])
    def edit(Cliente clienteInstance) {
        respond clienteInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN'])
    def update(Cliente clienteInstance) {
        if (clienteInstance == null) {
            notFound()
            return
        }

        if (clienteInstance.hasErrors()) {
            respond clienteInstance.errors, view:'edit'
            return
        }
        Anexo anexo0 = new Anexo()
        Anexo anexo1 = new Anexo()
        Anexo anexo2 = new Anexo()
        Anexo anexo3 = new Anexo()
        Anexo anexo4 = new Anexo()
        Anexo anexo5 = new Anexo()
        Anexo anexo6 = new Anexo()
        Anexo anexo7 = new Anexo()
        Anexo anexo8 = new Anexo()
        Anexo anexo9 = new Anexo()

        def uploadedFile0 = request.getFile('attachment0')
        anexo0.attachment = uploadedFile0?.getBytes() //converting the file to bytes
        anexo0.fileName=  uploadedFile0.originalFilename //getting the file name from the uploaded file
        anexo0.fileType = uploadedFile0.contentType//getting and storing the file type
        anexo0.setCliente(clienteInstance)

        def uploadedFile1 = request.getFile('attachment1')
        anexo1.attachment = uploadedFile1?.getBytes() //converting the file to bytes
        anexo1.fileName=  uploadedFile1.originalFilename //getting the file name from the uploaded file
        anexo1.fileType = uploadedFile1.contentType//getting and storing the file type
        anexo1.setCliente(clienteInstance)

        def uploadedFile2 = request.getFile('attachment2')
        anexo2.attachment = uploadedFile2?.getBytes() //converting the file to bytes
        anexo2.fileName=  uploadedFile2.originalFilename //getting the file name from the uploaded file
        anexo2.fileType = uploadedFile2.contentType//getting and storing the file type
        anexo2.setCliente(clienteInstance)
        def uploadedFile3 = request.getFile('attachment3')
        anexo3.attachment = uploadedFile3?.getBytes() //converting the file to bytes
        anexo3.fileName=  uploadedFile3.originalFilename //getting the file name from the uploaded file
        anexo3.fileType = uploadedFile3.contentType//getting and storing the file type
        anexo3.setCliente(clienteInstance)

        def uploadedFile4 = request.getFile('attachment4')
        anexo4.attachment = uploadedFile4?.getBytes() //converting the file to bytes
        anexo4.fileName=  uploadedFile4.originalFilename //getting the file name from the uploaded file
        anexo4.fileType = uploadedFile4.contentType//getting and storing the file type
        anexo4.setCliente(clienteInstance)

        def uploadedFile5 = request.getFile('attachment5')
        anexo5.attachment = uploadedFile5?.getBytes() //converting the file to bytes
        anexo5.fileName=  uploadedFile5.originalFilename //getting the file name from the uploaded file
        anexo5.fileType = uploadedFile5.contentType//getting and storing the file type
        anexo5.setCliente(clienteInstance)

        def uploadedFile6 = request.getFile('attachment6')
        anexo6.attachment = uploadedFile6?.getBytes() //converting the file to bytes
        anexo6.fileName=  uploadedFile6.originalFilename //getting the file name from the uploaded file
        anexo6.fileType = uploadedFile6.contentType//getting and storing the file type
        anexo6.setCliente(clienteInstance)

        def uploadedFile7 = request.getFile('attachment7')
        anexo7.attachment = uploadedFile7?.getBytes() //converting the file to bytes
        anexo7.fileName=  uploadedFile7.originalFilename //getting the file name from the uploaded file
        anexo7.fileType = uploadedFile7.contentType//getting and storing the file type
        anexo7.setCliente(clienteInstance)

        def uploadedFile8 = request.getFile('attachment8')
        anexo8.attachment = uploadedFile8?.getBytes() //converting the file to bytes
        anexo8.fileName=  uploadedFile8.originalFilename //getting the file name from the uploaded file
        anexo8.fileType = uploadedFile8.contentType//getting and storing the file type
        anexo8.setCliente(clienteInstance)

        def uploadedFile9 = request.getFile('attachment9')
        anexo9.attachment = uploadedFile9?.getBytes() //converting the file to bytes
        anexo9.fileName=  uploadedFile9.originalFilename //getting the file name from the uploaded file
        anexo9.fileType = uploadedFile9.contentType//getting and storing the file type
        anexo9.setCliente(clienteInstance)

        clienteInstance?.addToAnexos(anexo0)
        clienteInstance.anexos?.add(anexo1)
        clienteInstance.anexos?.add(anexo2)
        clienteInstance.anexos?.add(anexo3)
        clienteInstance.anexos?.add(anexo4)
        clienteInstance.anexos?.add(anexo5)
        clienteInstance.anexos?.add(anexo6)
        clienteInstance.anexos?.add(anexo7)
        clienteInstance.anexos?.add(anexo8)
        clienteInstance.anexos?.add(anexo9)

        clienteInstance.save flush:true

               request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Cliente.label', default: 'Cliente'), clienteInstance.id])
                redirect clienteInstance
            }
            '*'{ respond clienteInstance, [status: OK] }
        }
        redirect(controller: "cliente", action: "show",id: clienteInstance.id)
    }

    @Transactional
    @Secured(['ROLE_ADMIN'])
    def delete(Cliente clienteInstance) {

        if (clienteInstance == null) {
            notFound()
            return
        }
        for (Credito creditoInstance:clienteInstance.creditos.findAll()){

            for (Pagamento p:creditoInstance.pagamentos.findAll()){
                p.delete()
            }
            creditoInstance.delete()
        }
        for (Anexo anexo in clienteInstance.anexos.findAll()){
            anexo.delete()
        }

        clienteInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Cliente.label', default: 'Cliente'), clienteInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'clienteInstance.label', default: 'Cliente'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
    @Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
    def showAttachment(Anexo anexo){

        //retrieve photo code here
        response.setHeader("Content-disposition", "attachment; filename=${anexo.fileName}")
        response.contentType = anexo.fileType //'image/jpeg' will do too
        response.outputStream << anexo.attachment //'myphoto.jpg' will do too
        response.outputStream.flush()
    }

    def displayImage = {
        /*def sponsor = Sponsor.get(params.id)
        response.contentType = "image/jpeg"
        response.contentLength = sponsor?.logo.length
        response.outputStream.write(sponsor?.logo)*/
    }
    def send(){

        String url="email=fanisso@gmail.com&senha=akin2010&remetente=Taduma SMS&destino=843854654&mensagem="

     redirect(url: url)
    }

    @Secured(['ROLE_ADMIN', 'PARCELA_SHOW_ATTACHMENT'])
    def showFotoAttachment(Anexo anexo) {
        //retrieve photo code here
        response?.setHeader("Content-disposition", "attachment; filename=${anexo?.fileName}")
        response?.contentType = anexo?.fileType //'image/jpeg' will do too
        response?.outputStream << anexo?.attachment //'myphoto.jpg' will do too
        response?.outputStream?.flush()

    }
}
