package mz.maleyanga.home

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.SpringSecurityUtils
import mz.maleyanga.BasicController

/**
 * HomeController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class HomeController extends BasicController {
    SpringSecurityService springSecurityService

    def index = {}


}
