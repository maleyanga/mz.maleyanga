package mz.maleyanga.contact

import grails.plugin.springsecurity.annotation.Secured
import mz.maleyanga.BasicController

/**
 * ContactController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Secured(['ROLE_ADMIN'])
class ContactController  extends BasicController{

	@Secured(['ROLE_ADMIN','CONTACT_INDEX'])
def index (){ }
}
