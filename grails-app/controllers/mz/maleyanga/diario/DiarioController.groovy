package mz.maleyanga.diario


import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*


class DiarioController extends BasicController {

    @Secured(['ROLE_ADMIN', 'DIARIO_EDIT'])
    def diario() {}

    def printDi() {}

    def printUser() {}

    @Secured(['ROLE_ADMIN', 'DIARIO_EDIT'])
    def printDiario() {
        render(view: "printDiario", target: "_blank")
    }

    @Secured(['ROLE_ADMIN', 'DIARIO_EDIT'])
    def printDiarioPorUtilizador() {

    }
}
