package mz.maleyanga.periodo

import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * PeriodoController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
class PeriodoController extends BasicController {

    static allowedMethods = [save: ["POST","PUT"], update: ["PUT","POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN','PERIODO_INDEX'])
	def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Periodo.list(params), model:[periodoInstanceCount: Periodo.count()]
    }

    @Secured(['ROLE_ADMIN','PERIODO_LIST'])
	def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Periodo.list(params), model:[periodoInstanceCount: Periodo.count()]
    }

    @Secured(['ROLE_ADMIN','PERIODO_SHOW'])
    def show(Periodo periodoInstance) {
        updateCurrentAction('show')
        respond periodoInstance
    }

    @Secured(['ROLE_ADMIN','PERIODO_CREATE'])
    def create() {
        updateCurrentAction('create')
        respond new Periodo(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','PERIODO_SAVE'])
    def save(Periodo periodoInstance) {
        if (periodoInstance == null) {
            notFound()
            return
        }

        if (periodoInstance.hasErrors()) {
            respond periodoInstance.errors, view:'create'
            return
        }

        periodoInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'periodoInstance.label', default: 'Periodo'), periodoInstance.id])
                redirect periodoInstance
            }
            '*' { respond periodoInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','PERIODO_EDIT'])
     def edit(Periodo periodoInstance) {
        updateCurrentAction('edit')
        respond periodoInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','PERIODO_UPDATE'])
    def update(Periodo periodoInstance) {
        if (periodoInstance == null) {
            notFound()
            return
        }

        if (periodoInstance.hasErrors()) {
            respond periodoInstance.errors, view:'edit'
            return
        }

        periodoInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Periodo.label', default: 'Periodo'), periodoInstance.id])
                redirect periodoInstance
            }
            '*'{ respond periodoInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','PERIODO_DELETE'])
    def delete(Periodo periodoInstance) {

        if (periodoInstance == null) {
            notFound()
            return
        }

        periodoInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Periodo.label', default: 'Periodo'), periodoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'periodoInstance.label', default: 'Periodo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
