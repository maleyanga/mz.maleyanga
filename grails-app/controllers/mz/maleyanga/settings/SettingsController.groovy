package mz.maleyanga.settings



import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * SettingsController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN'])
class SettingsController extends BasicController {

    def defCredito() {

    }

    static allowedMethods = [save: ["POST", "PUT"], update: ["PUT", "POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN', 'SETTINGS_INDEX'])
    def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Settings.list(params), model: [settingsInstanceCount: Settings.count()]
    }

    @Secured(['ROLE_ADMIN','SETTINGS_LIST'])
	def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Settings.list(params), model:[settingsInstanceCount: Settings.count()]
    }

    @Secured(['ROLE_ADMIN','SETTINGS_SHOW'])
    def show(Settings settingsInstance) {
        updateCurrentAction('show')
        respond settingsInstance
    }

    @Secured(['ROLE_ADMIN','SETTINGS_CREATE'])
    def create() {
        updateCurrentAction('create')
        respond new Settings(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','SETTINGS_SAVE'])
    def save(Settings settingsInstance) {
        if (settingsInstance == null) {
            notFound()
            return
        }

        if (settingsInstance.hasErrors()) {
            respond settingsInstance.errors, view:'create'
            return
        }

        settingsInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'settingsInstance.label', default: 'Settings'), settingsInstance.id])
                redirect settingsInstance
            }
            '*' { respond settingsInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','SETTINGS_EDIT'])
    def edit(Settings settingsInstance) {
        updateCurrentAction('edit')
        respond settingsInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','SETTINGS_UPDATE'])
    def update(Settings settingsInstance) {
        if (settingsInstance == null) {
            notFound()
            return
        }

        if (settingsInstance.hasErrors()) {
            respond settingsInstance.errors, view:'edit'
            return
        }

        settingsInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Settings.label', default: 'Settings'), settingsInstance.id])
                redirect settingsInstance
            }
            '*'{ respond settingsInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','SETTINGS_DELETE'])
    def delete(Settings settingsInstance) {

        if (settingsInstance == null) {
            notFound()
            return
        }

        settingsInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Settings.label', default: 'Settings'), settingsInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'settingsInstance.label', default: 'Settings'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
