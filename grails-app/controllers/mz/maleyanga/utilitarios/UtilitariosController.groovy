package mz.maleyanga.utilitarios

import grails.plugin.asyncmail.AsynchronousMailService
import grails.plugin.nexmo.NexmoException
import grails.plugin.nexmo.NexmoService
import mz.maleyanga.BasicController

import mz.maleyanga.cliente.Cliente
import mz.maleyanga.pagamento.Pagamento
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse

import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.poi.hslf.model.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.springframework.security.access.annotation.Secured


/**
 * UtilitariosController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
class UtilitariosController  {
    NexmoService nexmoService
    def sendMailService
    AsynchronousMailService asyncMailService
    List clientes = new ArrayList<Cliente>()

    @Secured(['ROLE_ADMIN','UTILITARIOS_INDEX'])
def index(){}
    private final static int nome = 0
    private final static int nuit = 1
    private final static int tipoIndentificacao = 2
    private final static int numeroIndentificacao = 3
    private final static int residencia = 4
    private final static int dataExpiracao = 5
    private final static int email = 6
    private final static int telefone = 7
    def upload() {
    }

    @Secured(['ROLE_ADMIN','UTILITARIOS_DO_UPLAOD'])
    def doUpload() {
        def file = request.getFile('file')
        Workbook workbook = Workbook.getWorkbook(file.getInputStream())
        def sheet = workbook.getSheet(0)

//System.println(sheet.getRows())
        // skip first row (row 0) by starting from 1
        for (int row = 1; row < sheet.getRows(); row++) {
            Cliente cliente = new Cliente()
            cliente.nome=sheet.getCell(nome, row).getContents()
            cliente.nuit=sheet.getCell(nuit, row).getContents()
            cliente.tipoDeIndentificacao= sheet.getCell(tipoIndentificacao, row).getContents()
            cliente.numeroDeIndentificao= sheet.getCell(numeroIndentificacao, row).getContents()
            cliente.residencia = sheet.getCell(residencia, row).getContents()
            String theDate = sheet.getCell(dataExpiracao, row).getContents()
            def newdate = new Date().parse("d/M/yy", theDate)
            cliente.dataDeExpiracao =  newdate
            cliente.email = sheet.getCell(email, row).getContents()
            cliente.telefone = sheet.getCell(telefone, row).getContents()
            cliente.fileName=""
            cliente.fileType=""
            cliente.attachment=null

            clientes.add(cliente)
            cliente.save()
        }

        redirect (action:'list')
    }

    @Secured(['ROLE_ADMIN','UTILITARIOS_LIST'])
    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [clienteInstanceList: clientes, clienteInstanceCount: clientes.size()]
    }
    @Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
    def sms(){

    }

  /*  def sendSMS(String sms, String destino){
        def pagamentos = Pagamento.all
       *//* for (Pagamento p in pagamentos){
            if (p.diasDeMora>0){
                destino+=p.credito.cliente.telefone+";"
            }
        }*//*
       // def destinos = destino.subSequence(0, destino.length() - 1)
       // System.println(destino)
        sms = params.mensagem
       destino = params.destino
        String remetente = "FM"
        String senha = "akin2010"
        String mail="fanisso@gmail.com"


        def url = 'http://admin.taduma.co.mz/api/?email=' +
                mail +
                '&senha=' +
               senha +
                '&remetente=' +
                remetente +
                '&destino=' +
                destino +
                '&mensagem=' +sms
        System.println(url)
        System.println(destino)
        System.println(sms)
        redirect(url: url)
       
    }*/

    @Secured(['ROLE_ADMIN','UTILITARIOS_SEND_HTTP'])
    def sendHttp(){
       def  sms = params.mensagem
       def  destino = params.destino
        String remetente = "FM"
        String senha = "akin2010"
        String mail="fanisso@gmail.com"
        def url = 'http://admin.taduma.co.mz/api/?email=' +
                mail +
                '&senha=' +
                senha +
                '&remetente=' +
                remetente +
                '&destino=' +
                destino +
                '&mensagem=' +sms
        HttpClient httpClient = new DefaultHttpClient()
        HttpGet httpGet = new HttpGet(url)
        /*httpGet.addHeader(BasicScheme.authenticate(
                new UsernamePasswordCredentials("fanisso@gmail.com", "akin2010"),
                "UTF-8", false));*/

        HttpResponse httpResponse = httpClient.execute(httpGet)
        HttpEntity responseEntity = httpResponse.getEntity()
    }

    @Secured(['ROLE_ADMIN','UTILITARIOS_SEND_SMS'])
    def sendSMS(String sms, String destino){
        def smsResult
        def callResult

        try {
            // Send the message "What's up?" to 1-500-123-4567
            smsResult  = nexmoService.sendSms("+2583854654", "What's up?")

            // Call the number and tell them a message
          //  callResult = nexmoService.call("+2583854654", "Have a great day! Goodbye.")


        }catch (Exception e){
            System.println(e)
            System.println(smsResult)
        }

    }

    @Secured(['ROLE_ADMIN','UTILITARIOS_SEND_BULK_SMS'])
   def  sendBulkSms(){

      Pagamento p = Pagamento.last()
       sendMailService.sendPagamneto()
       System.println(p.id)
    }



    def userguide() {

    }
    }



