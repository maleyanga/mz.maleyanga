package mz.maleyanga.banco



import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * BancoController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
class BancoController extends BasicController {

    static allowedMethods = [save: ["POST","PUT"], update: ["PUT","POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN'])
	def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Banco.list(params), model:[bancoInstanceCount: Banco.count()]
    }

    @Secured(['ROLE_ADMIN'])
	def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Banco.list(params), model:[bancoInstanceCount: Banco.count()]
    }

    @Secured(['ROLE_ADMIN'])
    def show(Banco bancoInstance) {
        updateCurrentAction('show')
        respond bancoInstance
    }

    @Secured(['ROLE_ADMIN'])
    def create() {
        updateCurrentAction('create')
        respond new Banco(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN'])
    def save(Banco bancoInstance) {
        if (bancoInstance == null) {
            notFound()
            return
        }

        if (bancoInstance.hasErrors()) {
            respond bancoInstance.errors, view:'create'
            return
        }

        bancoInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bancoInstance.label', default: 'Banco'), bancoInstance.id])
                redirect bancoInstance
            }
            '*' { respond bancoInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN'])
    def edit(Banco bancoInstance) {
        updateCurrentAction('edit')
        respond bancoInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN'])
    def update(Banco bancoInstance) {
        if (bancoInstance == null) {
            notFound()
            return
        }

        if (bancoInstance.hasErrors()) {
            respond bancoInstance.errors, view:'edit'
            return
        }

        bancoInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Banco.label', default: 'Banco'), bancoInstance.id])
                redirect bancoInstance
            }
            '*'{ respond bancoInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN'])
    def delete(Banco bancoInstance) {

        if (bancoInstance == null) {
            notFound()
            return
        }

        bancoInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Banco.label', default: 'Banco'), bancoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bancoInstance.label', default: 'Banco'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
