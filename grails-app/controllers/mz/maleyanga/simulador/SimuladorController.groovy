package mz.maleyanga.simulador

import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.mirzsoft.grails.actionlogging.annotation.ActionLogging
import org.mirzsoft.grails.actionlogging.annotation.SpringUserIdentification
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * SimuladorController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
@ActionLogging
@SpringUserIdentification
@Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
class SimuladorController extends BasicController {

    static allowedMethods = [save: ["POST", "PUT"], update: ["PUT", "POST"], delete: "DELETE"]
    @Secured(['ROLE_ADMIN','SIMULADOR_INDEX'])
    def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Simulador.list(params), model: [simuladorInstanceCount: Simulador.count()]
    }

    @Secured(['ROLE_ADMIN','SIMULADOR_LIST'])
    def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Simulador.list(params), model: [simuladorInstanceCount: Simulador.count()]
    }

    @Secured(['ROLE_ADMIN','SIMULADOR_SHOW'])
    def show(Simulador simuladorInstance) {
        updateCurrentAction('show')
        respond simuladorInstance
    }

    @Secured(['ROLE_ADMIN','SIMULADOR_CREATE'])
    def create() {
        updateCurrentAction('create')
        respond new Simulador(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','SIMULADOR_SAVE'])
    def save(Simulador simuladorInstance) {
        if (simuladorInstance == null) {
            notFound()
            return
        }

        if (simuladorInstance.hasErrors()) {
            respond simuladorInstance.errors, view: 'create'
            return
        }

        simuladorInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'simuladorInstance.label', default: 'Simulador'), simuladorInstance.id])
                redirect simuladorInstance
            }
            '*' { respond simuladorInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','SIMULADOR_EDIT'])
    def edit(Simulador simuladorInstance) {
        updateCurrentAction('edit')
        respond simuladorInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','SIMULADOR_UPDATE'])
    def update(Simulador simuladorInstance) {
        if (simuladorInstance == null) {
            notFound()
            return
        }

        if (simuladorInstance.hasErrors()) {
            respond simuladorInstance.errors, view: 'edit'
            return
        }

        simuladorInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Simulador.label', default: 'Simulador'), simuladorInstance.id])
                redirect simuladorInstance
            }
            '*' { respond simuladorInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','SIMULADOR_DELETE'])
    def delete(Simulador simuladorInstance) {

        if (simuladorInstance == null) {
            notFound()
            return
        }

        simuladorInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Simulador.label', default: 'Simulador'), simuladorInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'simuladorInstance.label', default: 'Simulador'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
