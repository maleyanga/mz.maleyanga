package mz.maleyanga.pagamento

import grails.transaction.Transactional
import mz.maleyanga.BasicController
import mz.maleyanga.PagamentoService
import mz.maleyanga.conta.Conta
import mz.maleyanga.documento.Anexo
import mz.maleyanga.security.Utilizador
import org.mirzsoft.grails.actionlogging.annotation.ActionLogging
import org.mirzsoft.grails.actionlogging.annotation.SpringUserIdentification
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * ParcelaController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = false)
@ActionLogging
@SpringUserIdentification
class ParcelaController extends BasicController {
    PagamentoService pagamentoService
    ParcelaService parcelaService
    def springSecurityService
    def contaService

    static allowedMethods = [save: ["POST", "PUT"], update: ["PUT", "POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN', 'PARCELA_INDEX'])
    def printParcela() {
        [parcela: parcelaService.parcelaInstance.numeroDoRecibo]
    }

    @Secured(['ROLE_ADMIN', 'PARCELA_INDEX'])
    def recibos() {

    }

    @Secured(['ROLE_ADMIN', 'PARCELA_INDEX'])
    def printEntrada() {
        [entrada: parcelaService.entrada.numeroDoRecibo]
    }

    @Secured(['ROLE_ADMIN', 'PARCELA_INDEX'])
    def printPrestacoesDoDia() {

    }

    def printRecibo() {

    }

    def printParcel() {

    }

    def printEntrad() {

    }

    @Secured(['ROLE_ADMIN', 'PARCELA_INDEX'])
    def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Parcela.list(params), model: [parcelaInstanceCount: Parcela.count()]
    }

    @Secured(['ROLE_ADMIN', 'PARCELA_LIST'])
    def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Parcela.list(params), model: [parcelaInstanceCount: Parcela.count()]
    }

    @Secured(['ROLE_ADMIN','PARCELA_SHOW'])
    def show(Parcela parcelaInstance) {
        updateCurrentAction('show')
        respond parcelaInstance
    }

    @Secured(['ROLE_ADMIN','PARCELA_CREATE'])
    def create() {
        updateCurrentAction('create')
        respond new Parcela(params)
    }

    @Secured(['ROLE_ADMIN','PARCELA_CREATE'])
    def createAutomatico() {
        updateCurrentAction('create')

        respond new Parcela(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','PARCELA_SAVE'])
    def save(Parcela parcelaInstance) {
        def utilizador = Utilizador.findById(springSecurityService.principal.id)
        parcelaInstance.setUtilizador(utilizador)
        if(parcelaInstance.pagamento.pago){
            flash.message="Parcela já paga!"
            return
        }


        if(parcelaInstance.valorParcial>parcelaInstance.pagamento.totalEmDivida*(-1)){

            def valor = parcelaInstance.valorParcial - parcelaInstance.pagamento.totalEmDivida * (-1)


            def descricao = 'Crédito do valor remanescente do pag. Nº ' + parcelaInstance.pagamento.id
            System.println(valor)
            boolean result = parcelaService.creditarValor(valor, parcelaInstance.pagamento.credito.cliente, descricao)
            if (!result) {
                flash.message = "O valor a ser pago é superar o valor em divida!, e o cliente não tem conta para alocação do valor remanascente!"
                return
            } else {
                flash.message = "O valor a ser pago é superar o valor em divida!. O valor remanescente foi alocado na conta do cliente"

            }
        }


        if (parcelaInstance == null) {
            notFound()
            return
        }

        if (parcelaInstance.hasErrors()) {
            respond parcelaInstance.errors, view: 'create'
            return
        }



        Anexo anexo0 = new Anexo()
        def uploadedFile0 = request.getFile('attachment0')

        anexo0.attachment = uploadedFile0?.getBytes() //converting the file to bytes
        anexo0.fileName=  uploadedFile0.originalFilename //getting the file name from the uploaded file
        anexo0.fileType = uploadedFile0.contentType//getting and storing the file type
        anexo0.setParcela(parcelaInstance)
        if(!anexo0.fileName.empty){
            anexo0.save()
            parcelaInstance?.setAnexo(anexo0)

        }
        parcelaInstance.save flush: true
        pagamentoService.ActualizarEstadoDeCredito(parcelaInstance.pagamento.credito)
        pagamentoService.calcularJurosDeDemora(parcelaInstance)


        request.withFormat {
            form {
                flash.message = message(code: 'pagamento.realizado', args: [message(code: 'parcelaInstance.label', default: 'Pagamento'), parcelaInstance.id])
                redirect parcelaInstance
            }
            '*' { respond parcelaInstance, [status: CREATED] }

        }

        redirect(controller: "parcela", action: "show",id: parcelaInstance.id)
    }

    @Secured(['ROLE_ADMIN','PARCELA_EDIT'])
    def edit(Parcela parcelaInstance) {
        updateCurrentAction('edit')
        respond parcelaInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','PARCELA_UPDATE'])
    def update(Parcela parcelaInstance) {
        def utilizador = Utilizador.findById(springSecurityService.principal.id)
        parcelaInstance.setUtilizador(utilizador)
        if (parcelaInstance == null) {
            notFound()
            return
        }

        if (parcelaInstance.hasErrors()) {
            respond parcelaInstance.errors, view: 'edit'
            return
        }

        parcelaInstance.save flush: true
      //  pagamentoService.addPagamentoParcial(parcelaInstance)


        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Parcela.label', default: 'Parcela'), parcelaInstance.id])
                redirect parcelaInstance
            }
            '*' { respond parcelaInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','PARCELA_DELETE'])
    def delete(Parcela parcelaInstance) {

        if (parcelaInstance == null) {
            notFound()
            return
        }
        def pagamentoInstance = Pagamento.find(parcelaInstance.pagamento)
        pagamentoInstance.pago = false
        pagamentoInstance.save()
        Anexo anexo0 = new Anexo()
        def uploadedFile0 = request.getFile('attachment0')
        anexo0.attachment = uploadedFile0?.getBytes() //converting the file to bytes
        anexo0.fileName=  uploadedFile0.originalFilename //getting the file name from the uploaded file
        anexo0.fileType = uploadedFile0.contentType//getting and storing the file type
        anexo0.setParcela(parcelaInstance)
        parcelaInstance.setAnexo(anexo0)

        parcelaInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Parcela.label', default: 'Parcela'), parcelaInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'parcelaInstance.label', default: 'Parcela'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    @Secured(['ROLE_ADMIN','PARCELA_SHOW_ATTACHMENT'])
    def showAttachment(Anexo anexo){
        //retrieve photo code here
        response.setHeader("Content-disposition", "attachment; filename=${anexo.fileName}")
        response.contentType = anexo.fileType //'image/jpeg' will do too
        response.outputStream << anexo.attachment //'myphoto.jpg' will do too
        response.outputStream.flush()

    }




}
