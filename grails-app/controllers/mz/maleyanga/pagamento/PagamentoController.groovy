package mz.maleyanga.pagamento

import grails.transaction.Transactional
import mz.maleyanga.BasicController
import mz.maleyanga.conta.Conta
import mz.maleyanga.credito.Credito
import mz.maleyanga.security.Utilizador
import org.mirzsoft.grails.actionlogging.annotation.ActionLogging
import org.mirzsoft.grails.actionlogging.annotation.SpringUserIdentification
import org.springframework.security.access.annotation.Secured

import java.awt.List
import java.nio.ByteBuffer
import java.nio.charset.Charset

import static org.springframework.http.HttpStatus.*

/**
 * PagamentoController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = false)
@ActionLogging
@SpringUserIdentification
class PagamentoController extends BasicController {
    def scaffold = Pagamento
    def pagamentoService

    static allowedMethods = [save: ["POST","PUT"], update: ["PUT","POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN','PAGAMENTO_INDEX'])
	def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
       // pagamentoService.calcularTodosOsJurosDeDemora()

        respond Pagamento.list(params), model:[pagamentoInstanceCount: Pagamento.count()]
    }

    @Secured(['ROLE_ADMIN','PAGAMENTO_LIST'])
	def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Pagamento.list(params), model:[pagamentoInstanceCount: Pagamento.count()]
    }

    @Secured(['ROLE_ADMIN','PAGAMENTO_LIST_DEVEDORES'])
    def listDevedores(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        def  devedores = Credito.findAllByEmDivida(true)

        respond devedores, model:[total: devedores.size()]
    }

    @Secured(['ROLE_ADMIN','PAGAMENTO_SHOW'])
    def show(Pagamento pagamentoInstance) {
     //  def result= pagamentoService.pagamentoAutomatico(pagamentoInstance)
        pagamentoService.pagamentoInstance = pagamentoInstance
        def total_a_pagar = pagamentoInstance.valorDaPrestacao+pagamentoInstance.valorDeJurosDeDemora
       // pagamentoService.calcularJurosDeDemora(pagamentoInstance)

        updateCurrentAction('show')
        respond pagamentoInstance, model: [total_a_pagar: total_a_pagar]
    }



    @Secured(['ROLE_ADMIN','PAGAMENTO_CREATE'])
    def create() {
        updateCurrentAction('create')
        respond new Pagamento(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','PAGAMENTO_SAVE'])
    def save(Pagamento pagamentoInstance) {
        if (pagamentoInstance == null) {
            notFound()
            return
        }

        if (pagamentoInstance.hasErrors()) {
            respond pagamentoInstance.errors, view:'create'
            return
        }

        pagamentoInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'pagamentoInstance.label', default: 'Pagamento'), pagamentoInstance.id])
                redirect pagamentoInstance
            }
            '*' { respond pagamentoInstance, [status: CREATED] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','PAGAMENTO_GERAR_NOVA_PRESTACAO'])
    def gerarNovaPrestacao() {

        Pagamento pagamento = Pagamento.findById(params.id)
        Parcela parcelaInstance = Parcela.findById(params.id_parcela)
        def divida = pagamento.totalEmDivida
        if (divida * (-1) > pagamento.valorDeAmortizacao) {
            flash.message = "Operação recisada, por favor de pagar os juros de mora!"
        }
        System.println("gear nova prestcao, divida=" + divida)
        Parcela parcela = new Parcela()
        parcela.pagamento = pagamento
        parcela.dataDePagamento = parcelaInstance.dataDePagamento
        parcela.valorParcial = divida * (-1)
        parcela.descricao = "Pag. automático do valor em dívida, e geração da nova Prestação. Pagamento anterior=" + parcelaInstance.id
        parcela.save(flush: true)
        if (Parcela.find(parcela)) {
            System.println("Pacela salva")
        } else {
            System.println("parcela não salva")
        }

        Pagamento pagamentoInstance = new Pagamento()
        pagamentoInstance.descricao = (pagamento.credito.pagamentos.size() + 1) + "ª- Prestacao" + ", gerada através do Prestação anterior com ID No." + pagamento.id + ". Uma vez paga o valor dos juros."
        pagamentoInstance.dataPrevistoDePagamento = pagamento.dataPrevistoDePagamento.plus(30) //.plus(30
        pagamentoInstance.credito = pagamento.credito
        pagamentoInstance.valorDeJuros = pagamento.valorDeJuros
        pagamentoInstance.valorDeAmortizacao = pagamento.valorDeAmortizacao
        pagamentoInstance.saldoDevedor = pagamento.saldoDevedor
        pagamentoInstance.valorDaPrestacao = pagamento.valorDaPrestacao

        pagamentoInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'pagamentoInstance.label', default: 'Pagamento'), pagamentoInstance.id])
                redirect pagamentoInstance
            }
            '*' { respond pagamentoInstance, [status: CREATED] }
        }
        redirect(controller: 'pagamento', action: 'show', params: [id: pagamentoInstance.id])
    }

    @Secured(['ROLE_ADMIN','PAGAMENTO_EDIT'])
    def edit(Pagamento pagamentoInstance) {
        if (pagamentoInstance.pago){
            flash.message= ("Esta prestacao ja foi paga, portanto nao pode ser editado novamente!")
            redirect(controller: "pagamento", action: "show", id: pagamentoInstance.id)
        }
        updateCurrentAction('edit')
        respond pagamentoInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','PAGAMENTO_UPDATE'])
    def update(Pagamento pagamentoInstance) {

        if (pagamentoInstance == null) {
            notFound()
            return
        }
        if (pagamentoInstance.dataDePagamento==null){
            flash.message="Seleccione no calendario da data de pagamento !"
            return
        }

        if (pagamentoInstance.hasErrors()) {
            respond pagamentoInstance.errors, view:'edit'
            return
        }

       // pagamentoService.lancarPagamneto(pagamentoInstance)

        pagamentoInstance.save flush:true





        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Pagamento.label', default: 'Pagamento'), pagamentoInstance.id])


                redirect pagamentoInstance

            }
            '*'{ respond pagamentoInstance, [status: OK] }
        }

    }

    @Transactional
    @Secured(['ROLE_ADMIN','PAGAMENTO_DELETE'])
    def delete(Pagamento pagamentoInstance) {

        if (pagamentoInstance == null) {
            notFound()
            return
        }

        pagamentoInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Pagamento.label', default: 'Pagamento'), pagamentoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'pagamentoInstance.label', default: 'Pagamento'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }


    @Secured(['ROLE_ADMIN','PAGAMENTO_PAGAMENTOS_EFECTIVADOS'])
    def  pagamentosEfectivados(Integer max){
        updateCurrentAction('pagamentosEfectivados')
        params.max = Math.min(max ?: 10, 100)

        def pagamentos = Pagamento.findAllByPago(true)

        respond pagamentos, model:[pagamentoInstanceCount: Pagamento.findAllByPago(true).size()]

    }

    @Secured(['ROLE_ADMIN','PAGAMENTOS_PERFEITOS'])
    def  pagamentosPerfeitos(Integer max){
        updateCurrentAction('pagamentosPerfeitos')
        params.max = Math.min(max ?: 10, 100)
        def pagamentos = Pagamento.findAllByPago(false)
        respond pagamentos, model:[pagamentoInstanceCount: Pagamento.count()]

    }


    @Secured(['ROLE_ADMIN','PAGAMENTOS_POR_EFECTIVAR'])
    def  pagamentosPorEfectivar(Integer max){
        updateCurrentAction('pagamentosPorEfectivar')
        params.max = Math.min(max ?: 10, 100)


        respond Pagamento.list(params), model: [ pagamentoInstanceCount:Pagamento.findAllByPago(false).size()]

    }

    @Secured(['ROLE_ADMIN','PAGAMENTO_PRINT_RECEBIDO'])
    def printRebecido (){

        String reportName = '/recibo'
        params.ext = "pdf"
        redirect(action: printReport, params: [reportExt:params.ext,reportName:reportName,id:params.id])

    }

    def pagamentos() {

    }

    def pagamentoAutomatico(){
        def result = pagamentoService.pagamentoAutomatic(pagamentoService.pagamentoInstance)
        System.println(result)
        flash.message=result
        if(pagamentoService.parcelaInstance!=null){
            redirect(controller: 'parcela',action: 'show',id: pagamentoService.parcelaInstance.id)
        }else {
            redirect(controller: 'pagamento',action: 'show',id: pagamentoService.pagamentoInstance.id)
        }

    }


}
