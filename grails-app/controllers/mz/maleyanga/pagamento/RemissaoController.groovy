package mz.maleyanga.pagamento

import grails.transaction.Transactional
import mz.maleyanga.BasicController
import mz.maleyanga.security.Utilizador
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * RemissaoController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
class RemissaoController extends BasicController {
    def springSecurityService
    static allowedMethods = [save: ["POST", "PUT"], update: ["PUT", "POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN','REMISSAO_INDEX'])
    def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Remissao.list(params), model: [remissaoInstanceCount: Remissao.count()]
    }

    @Secured(['ROLE_ADMIN','REMISSAO_LIST'])
    def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Remissao.list(params), model: [remissaoInstanceCount: Remissao.count()]
    }

    @Secured(['ROLE_ADMIN','REMISSAO_SHOW'])
    def show(Remissao remissaoInstance) {
        updateCurrentAction('show')
        respond remissaoInstance
    }

    @Secured(['ROLE_ADMIN','REMISSAO_CREATE',])
    def create() {
        updateCurrentAction('create')
        respond new Remissao(params)
    }
    @Secured(['ROLE_ADMIN','REMISSAO_CREATE_OUT'])
    def createOut() {
        updateCurrentAction('create')
        respond new Remissao(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','REMISSAO_SAVE'])
    def save(Remissao remissaoInstance) {
        def utilizador = Utilizador.findById(springSecurityService.principal.id)
        remissaoInstance.setUtilizador(utilizador)
      //  def pagamentoInstance = session.getAttribute("pagamento") as Pagamento
        def valorRemissao=remissaoInstance.valorDaRemissao+remissaoInstance.pagamento.valorDaRemissao
        def valorDeJurosDeDemora = remissaoInstance.pagamento.valorDeJurosDeDemora*(-1)

        System.println(valorRemissao)
        System.println(valorDeJurosDeDemora)
        System.println(remissaoInstance.pagamento.id)
        System.println(remissaoInstance.pagamento.valorDaRemissao)
        if(valorRemissao>valorDeJurosDeDemora){
            flash.message = "A valor de juros de mora não justifica a remissão!"
            return
        }
        if (remissaoInstance == null) {
            notFound()
            return
        }

        if (remissaoInstance.hasErrors()) {
            respond remissaoInstance.errors, view: 'create'
            return
        }

        remissaoInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'remissaoInstance.label', default: 'Remissao'), remissaoInstance.id])
                redirect remissaoInstance
            }
            '*' { respond remissaoInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','REMISSAO_EDIT'])
    def edit(Remissao remissaoInstance) {
        updateCurrentAction('edit')
        respond remissaoInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','REMISSAO_UPDATE'])
    def update(Remissao remissaoInstance) {
        def utilizador = Utilizador.findById(springSecurityService.principal.id)
        remissaoInstance.setUtilizador(utilizador)
        if (remissaoInstance == null) {
            notFound()
            return
        }

        if (remissaoInstance.hasErrors()) {
            respond remissaoInstance.errors, view: 'edit'
            return
        }

        remissaoInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Remissao.label', default: 'Remissao'), remissaoInstance.id])
                redirect remissaoInstance
            }
            '*' { respond remissaoInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['REMISSAO_DELETE'])
    def delete(Remissao remissaoInstance) {

        if (remissaoInstance == null) {
            notFound()
            return
        }

        remissaoInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Remissao.label', default: 'Remissao'), remissaoInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'remissaoInstance.label', default: 'Remissao'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
