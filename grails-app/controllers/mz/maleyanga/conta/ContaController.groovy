package mz.maleyanga.conta



import grails.transaction.Transactional
import mz.maleyanga.BasicController
import mz.maleyanga.cliente.Cliente
import mz.maleyanga.entidade.Entidade
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * ContaController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
class ContaController extends BasicController {
    ContaService contaService
    static allowedMethods = [save: ["POST", "PUT"], update: ["PUT", "POST"], delete: "DELETE"]

    def printExtratoDeConta() {
        [conta: contaService.conta]
    }

    @Secured(['ROLE_ADMIN', 'CONTA_INDEX'])
    def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        // respond Conta.list(params), model:[contaInstanceCount: Conta.count()]
        redirect(action: 'contas')
    }

    def contas() {

    }

    def printExtrato() {

    }


    @Secured(['ROLE_ADMIN', 'CONTA_LIST'])
    def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Conta.list(params), model: [contaInstanceCount: Conta.count()]
    }

    @Secured(['ROLE_ADMIN', 'CONTA_SHOW'])
    def show(Conta contaInstance) {
        updateCurrentAction('show')
        def titular
        if(contaInstance.finalidade=='conta_cliente'){
            def conta = contaInstance.numeroDaConta
            def conta_split = conta.split('_')
             titular = Cliente.findById(conta_split.last().toLong())
        }
        respond contaInstance,model: [titular:titular]
    }

    @Secured(['ROLE_ADMIN','CONTA_CREATE'])
    def create() {
        updateCurrentAction('create')
        respond new Conta(params)
    }

    @Secured(['ROLE_ADMIN','CONTA_CREATE_FROM_ENTIDADE'])
    def createFromEntidade(){
        updateCurrentAction('create')
        respond new Conta(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','CONTA_SAVE'])
    def save(Conta contaInstance) {
        if (contaInstance == null) {
            notFound()
            return
        }

        if (contaInstance.entidade != null && contaInstance.cliente != null) {
            flash.message = "Selecione a Entidade ou o Cliente e numca os dois ao mesmo tempo!"
            return
        }

        if (contaInstance.entidade == null && contaInstance.cliente == null) {
            flash.message = "Selecione a Entidade ou o Cliente e numca os dois ao mesmo tempo!"
            return
        }

        if (contaInstance.hasErrors()) {
            respond contaInstance.errors, view:'create'
            return
        }

        contaInstance.save flush:true
        if (contaInstance?.entidade?.id != null) {
            def entidadeDb = Entidade.findById(contaInstance.entidade.id)
            if(entidadeDb.contas==null){
                entidadeDb.contas = new ArrayList<Conta>()
            }
            entidadeDb.contas.add(contaInstance)
            entidadeDb.save(flush: true)
        }
        if (contaInstance?.cliente?.id != null) {
            def clienteDb = Cliente.findById(contaInstance.cliente.id)
            if(clienteDb.contas==null){
                clienteDb.contas = new ArrayList<Conta>()
            }
            clienteDb.contas.add(contaInstance)
            clienteDb.save(flush: true)
        }

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'contaInstance.label', default: 'Conta'), contaInstance.id])
                redirect contaInstance
            }
            '*' { respond contaInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','CONTA_EDIT'])
    def edit(Conta contaInstance) {
        updateCurrentAction('edit')
        respond contaInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','CONTA_UPDATE'])
    def update(Conta contaInstance) {

        if (contaInstance.designacaoDaConta == 'conta_amortizacao' || 'conta_jutos_de_mora' || 'conta_capital' || 'conta_juros' || 'conta_cliente') {
            flash.message = "As contas do sistema não de podem ser alterados!"
        }
        if (contaInstance == null) {
            notFound()
            return
        }

        if (contaInstance.hasErrors()) {
            respond contaInstance.errors, view:'edit'
            return
        }

        contaInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Conta.label', default: 'Conta'), contaInstance.id])
                redirect contaInstance
            }
            '*'{ respond contaInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','CONTA_DELETE'])
    def delete(Conta contaInstance) {

        if (contaInstance == null) {
            notFound()
            return
        }

        contaInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Conta.label', default: 'Conta'), contaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'contaInstance.label', default: 'Conta'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
