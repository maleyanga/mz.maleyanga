package mz.maleyanga.feriado

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import mz.maleyanga.BasicController

/**
 * FeriadoController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class FeriadoController extends BasicController{

    @Secured(['ROLE_ADMIN','FERIADO_FERIADO_CRUD'])
    def feriadoCrud(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Feriado.list(params), model: [feriadoInstanceCount: Feriado.count()]
    }

    @Secured(['ROLE_ADMIN','FERIADO_CREATE'])
    def create() {
        respond new Feriado(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','FERIADO_SAVE'])
    def save(Feriado feriadoInstance) {
        if (feriadoInstance == null) {
            respond feriadoInstance.errors, view:'create'
            return
        }

        if (feriadoInstance.hasErrors()) {
            respond feriadoInstance.errors, view:'create'
            return
        }

        feriadoInstance.save flush:true

       /* request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'feriadoInstance.label', default: 'Feriado'), feriadoInstance.id])
                redirect feriadoInstance
            }

        }
        redirect(controller: 'feriado',action: 'index')*/
    }

}
