package mz.maleyanga.credito

import grails.plugin.springsecurity.SpringSecurityService
import grails.transaction.Transactional
import mz.maleyanga.BasicController
import mz.maleyanga.SettingsService
import mz.maleyanga.cliente.Cliente
import mz.maleyanga.documento.Anexo
import mz.maleyanga.pagamento.Pagamento
import mz.maleyanga.security.Utilizador
import mz.maleyanga.settings.Settings
import mz.maleyanga.simulador.Simulador
import org.mirzsoft.grails.actionlogging.annotation.ActionLogging
import org.mirzsoft.grails.actionlogging.annotation.SpringUserIdentification
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * CreditoController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = false)
@ActionLogging
@SpringUserIdentification
class CreditoController extends BasicController {
    def springSecurityService
    def contaService
    def pagamentoService
    def simuladorService
    def taxaService
    def creditoService

    static allowedMethods = [save: ["POST", "PUT"], update: ["PUT", "POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN', 'CREDITO_INDEX'])
    def createCredito() {

    }

    def printExtratoDeCredito() {
        [credito: creditoService.credito.cliente.nome]
    }

    def planoDePagamento() {
        [plano: creditoService.credito.cliente.nome]
    }

    def simulador() {

    }

    def printSimulador() {

    }

    def printPlano() {

    }

    def printExtrato() {

    }

    @Secured(['ROLE_ADMIN', 'CREDITO_INDEX'])
    def index(Integer max) {
        redirect(action: 'createCredito')
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Credito.list(params), model: [creditoInstanceCount: Credito.count()]
    }

    @Secured(['ROLE_ADMIN', 'CREDITO_LIST_ALL'])
    def listAll(Integer max) {
        updateCurrentAction('listAll')
        params.max = Math.min(max ?: 10, 100)
        respond Credito.list(params), model: [creditoInstanceCount: Credito.count()]
    }

    @Secured(['ROLE_ADMIN','CREDITO_LIST_ABERTOS'])
    def listAbertos(Integer max) {
        updateCurrentAction('lisAbertos')
        params.max = Math.min(max ?: 10, 100)
        def  creditosAbertos = Credito.findAllByEstado("Aberto")
        respond creditosAbertos, model: [creditoInstanceCount: creditosAbertos.size()]
    }

    @Secured(['ROLE_ADMIN','CREDITO_LIST_FECHADOS'])
    def listFechados(Integer max) {
        updateCurrentAction('listFechados')
        params.max = Math.min(max ?: 10, 100)
        def  creditosFechados = Credito.findAllByEstado("Fechado")
        respond creditosFechados, model: [creditoInstanceCount: creditosFechados.size()]
    }

    @Secured(['ROLE_ADMIN','CREDITO_LIST_PENDENTES'])
    def listPendentes(Integer max) {
        updateCurrentAction('listPendentes')
        params.max = Math.min(max ?: 10, 100)
        def  creditoPendentes = Credito.findAllByEstado("Pendente")
        respond creditoPendentes, model: [creditoInstanceCount: creditoPendentes.size()]
    }

    @Secured(['ROLE_ADMIN','CREDITO_LIST_EM_PROGRESSO'])
    def listEmProgresso(Integer max) {
        updateCurrentAction('listEmProgresso')
        params.max = Math.min(max ?: 10, 100)
        def  creditoPendentes = Credito.findAllByEstado("EmProgresso")
        respond creditoPendentes, model: [creditoInstanceCount: creditoPendentes.size()]
    }


    @Secured(['ROLE_ADMIN','CREDITO_LIST'])
    def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Credito.list(params), model: [creditoInstanceCount: Credito.count()]
    }

    @Secured(['ROLE_ADMIN','CREDITO_SHOW'])
    def show(Credito creditoInstance) {
        creditoInstance = Credito.findById(creditoService.credito.id)

        session.setAttribute("cliente", creditoInstance.cliente)
        updateCurrentAction('show')
        def total = 0.0
        def totalDivida = 0.0
        creditoInstance.pagamentos.each { total += it.valorDaPrestacao }
        creditoInstance.pagamentos.each { totalDivida += it.totalEmDivida }

        respond creditoInstance, model: [total: total, totalDivida: totalDivida]
    }


    @Secured(['ROLE_ADMIN','CREDITO_CREATE'])
    def create() {

        respond new Credito(params)

    }


    @Transactional
    @Secured(['ROLE_ADMIN','CREDITO_SAVE'])
    def save(Credito creditoInstance) {
        SettingsService settingsService
        def utilizador = Utilizador.findById(springSecurityService.principal?.id)
        creditoInstance.setUtilizador(utilizador)
        def agora = new Date()
        Calendar c = Calendar.getInstance()
        c.setTime(agora)
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK)
        def settings = Settings.first()
        if (!settings.domingo&&dayOfWeek==1){
            flash.message = "Dia não permitido!"
            return
        }
        if (!settings.segunda&&dayOfWeek==2){
            flash.message = "Dia não permitido!"
            return
        }
        if (!settings.terca&&dayOfWeek==3){
            flash.message = "Dia não permitido!"
            return
        }
        if (!settings.quarta&&dayOfWeek==4){
            flash.message = "Dia não permitido!"
            return
        }
        if (!settings.quinta&&dayOfWeek==5){
            flash.message = "Dia não permitido!"
            return
        }
        if (!settings.sexta&&dayOfWeek==6){
            flash.message = "Dia não permitido!"
            return
        }
        if (!settings.sabado&&dayOfWeek==7){
            flash.message = "Dia não permitido!"
            return
        }

        if (!creditoInstance.cliente.ativo){
            flash.message = "Este cliente esta impedido de ter novos crédito!"
            return
        }

        if(settings.atualizarDadosDoCliente){
            if (creditoInstance.cliente.lastUpdated.plus(90)<agora){
                flash.message = "Atualizar os dados do cliente!"
                return
            }
        }

        /*if(dayOfWeek==7||dayOfWeek==1){
            flash.message = "Dia não permitido!"
            return
        }*/

      /* if(creditoInstance.cliente.estadoCivil.equals("Casado")&&creditoInstance.cliente.assinante.equals(null)){
           flash.message = "Por favor, registar o assinante deste cliente!"
           return
       }*/


        creditoInstance.setEstado("Aberto")
            if (creditoInstance == null) {
            notFound()
            return
        }



        if (creditoInstance.hasErrors()) {
            respond creditoInstance.errors, view: 'create'
            return
        }


        if (params.ckboxjuros==1){

            creditoInstance.valorCreditado-=creditoInstance.valorCreditado*creditoInstance.percentualDejuros/100
        }

        System.println(creditoInstance.reterCapital)
        creditoInstance.emDivida=true
        creditoInstance.invalido=false
        taxaService.calcularTaxas(creditoInstance)

        Anexo anexo0 = new Anexo()
        Anexo anexo1 = new Anexo()
        Anexo anexo2 = new Anexo()
        Anexo anexo3 = new Anexo()
        Anexo anexo4 = new Anexo()
        Anexo anexo5 = new Anexo()
        Anexo anexo6 = new Anexo()
        Anexo anexo7 = new Anexo()
        Anexo anexo8 = new Anexo()
        Anexo anexo9 = new Anexo()

        def uploadedFile0 = request.getFile('attachment0')
        anexo0.attachment = uploadedFile0?.getBytes() //converting the file to bytes
        anexo0.fileName=  uploadedFile0.originalFilename //getting the file name from the uploaded file
        anexo0.fileType = uploadedFile0.contentType//getting and storing the file type
        anexo0.setCredito(creditoInstance)

        def uploadedFile1 = request.getFile('attachment1')
        anexo1.attachment = uploadedFile1?.getBytes() //converting the file to bytes
        anexo1.fileName=  uploadedFile1.originalFilename //getting the file name from the uploaded file
        anexo1.fileType = uploadedFile1.contentType//getting and storing the file type
        anexo1.setCredito(creditoInstance)

        def uploadedFile2 = request.getFile('attachment2')
        anexo2.attachment = uploadedFile2?.getBytes() //converting the file to bytes
        anexo2.fileName=  uploadedFile2.originalFilename //getting the file name from the uploaded file
        anexo2.fileType = uploadedFile2.contentType//getting and storing the file type
        anexo2.setCredito(creditoInstance)
        def uploadedFile3 = request.getFile('attachment3')
        anexo3.attachment = uploadedFile3?.getBytes() //converting the file to bytes
        anexo3.fileName=  uploadedFile3.originalFilename //getting the file name from the uploaded file
        anexo3.fileType = uploadedFile3.contentType//getting and storing the file type
        anexo3.setCredito(creditoInstance)

        def uploadedFile4 = request.getFile('attachment4')
        anexo4.attachment = uploadedFile4?.getBytes() //converting the file to bytes
        anexo4.fileName=  uploadedFile4.originalFilename //getting the file name from the uploaded file
        anexo4.fileType = uploadedFile4.contentType//getting and storing the file type
        anexo4.setCredito(creditoInstance)

        def uploadedFile5 = request.getFile('attachment5')
        anexo5.attachment = uploadedFile5?.getBytes() //converting the file to bytes
        anexo5.fileName=  uploadedFile5.originalFilename //getting the file name from the uploaded file
        anexo5.fileType = uploadedFile5.contentType//getting and storing the file type
        anexo5.setCredito(creditoInstance)

        def uploadedFile6 = request.getFile('attachment6')
        anexo6.attachment = uploadedFile6?.getBytes() //converting the file to bytes
        anexo6.fileName=  uploadedFile6.originalFilename //getting the file name from the uploaded file
        anexo6.fileType = uploadedFile6.contentType//getting and storing the file type
        anexo6.setCredito(creditoInstance)

        def uploadedFile7 = request.getFile('attachment7')
        anexo7.attachment = uploadedFile7?.getBytes() //converting the file to bytes
        anexo7.fileName=  uploadedFile7.originalFilename //getting the file name from the uploaded file
        anexo7.fileType = uploadedFile7.contentType//getting and storing the file type
        anexo7.setCredito(creditoInstance)

        def uploadedFile8 = request.getFile('attachment8')
        anexo8.attachment = uploadedFile8?.getBytes() //converting the file to bytes
        anexo8.fileName=  uploadedFile8.originalFilename //getting the file name from the uploaded file
        anexo8.fileType = uploadedFile8.contentType//getting and storing the file type
        anexo8.setCredito(creditoInstance)

        def uploadedFile9 = request.getFile('attachment9')
        anexo9.attachment = uploadedFile9?.getBytes() //converting the file to bytes
        anexo9.fileName=  uploadedFile9.originalFilename //getting the file name from the uploaded file
        anexo9.fileType = uploadedFile9.contentType//getting and storing the file type
        anexo9.setCredito(creditoInstance)

        creditoInstance?.addToAnexos(anexo0)
        creditoInstance.anexos?.add(anexo1)
        creditoInstance.anexos?.add(anexo2)
        creditoInstance.anexos?.add(anexo3)
        creditoInstance.anexos?.add(anexo4)
        creditoInstance.anexos?.add(anexo5)
        creditoInstance.anexos?.add(anexo6)
        creditoInstance.anexos?.add(anexo7)
        creditoInstance.anexos?.add(anexo8)
        creditoInstance.anexos?.add(anexo9)


        creditoInstance.save flush: true
        pagamentoService.criarPagamentos(creditoInstance)
        //  boolean result = contaService.debidoAutomatico(creditoInstance)
        if (result) {
            flash.message = 'valor do credito foi debitado na conta capital com sucesso'
        } else {
            flash.message = 'Erro ao debitar o valor da credito na conta capital'
        }
        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'creditoInstance.label', default: 'Credito'), creditoInstance.id])
                redirect creditoInstance
            }
            '*' { respond creditoInstance, [status: CREATED] }
        }
        flash.message = "O Credito No."+creditoInstance.id+" foi criado com sucesso!"
       // redirect(controller: 'utilitarios',action: 'sendSMS',params: [sms:"Seu credito foi aprovado. valor:"+creditoInstance.valorCreditado,destino:creditoInstance.cliente.telefone])
     redirect(controller: "credito", action: "show", id: creditoInstance.id)
    }

    @Secured(['ROLE_ADMIN','CREDITO_EDIT'])
    def edit(Credito creditoInstance) {
        updateCurrentAction('edit')
        respond creditoInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','CREDITO_UPDATE'])
    def update(Credito creditoInstance) {

        def utilizador = Utilizador.findById(springSecurityService.principal.id)
        creditoInstance.setUtilizador(utilizador)
      if (!creditoInstance.estado=="Fechado"){
          flash.message="Este credito nao pode ser actualiza uma vez que não esta em aberto"
          return
      }
        def agora = new Date()
        Calendar c = Calendar.getInstance()
        c.setTime(agora)
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK)
        if(dayOfWeek==7||dayOfWeek==1){
            flash.message = "Dia não permitido!"
            return
        }

        if (creditoInstance == null) {
            notFound()
            return
        }

       /* if (creditoInstance.hasErrors()) {
            respond creditoInstance.errors, view: 'edit'
            return
        }*/

      /*  for(Pagamento p in creditoInstance.pagamentos.findAll()){
            p.discard()
        }*/


       // taxaService.calcularTaxas(creditoInstance)
        //pagamentoService.eliminarPagamentos(creditoInstance)

        Anexo anexo0 = new Anexo()
        Anexo anexo1 = new Anexo()
        Anexo anexo2 = new Anexo()
        Anexo anexo3 = new Anexo()
        Anexo anexo4 = new Anexo()
        Anexo anexo5 = new Anexo()
        Anexo anexo6 = new Anexo()
        Anexo anexo7 = new Anexo()
        Anexo anexo8 = new Anexo()
        Anexo anexo9 = new Anexo()

        def uploadedFile0 = request.getFile('attachment0')
        anexo0.attachment = uploadedFile0?.getBytes() //converting the file to bytes
        anexo0.fileName=  uploadedFile0.originalFilename //getting the file name from the uploaded file
        anexo0.fileType = uploadedFile0.contentType//getting and storing the file type
        anexo0.setCredito(creditoInstance)

        def uploadedFile1 = request.getFile('attachment1')
        anexo1.attachment = uploadedFile1?.getBytes() //converting the file to bytes
        anexo1.fileName=  uploadedFile1.originalFilename //getting the file name from the uploaded file
        anexo1.fileType = uploadedFile1.contentType//getting and storing the file type
        anexo1.setCredito(creditoInstance)

        def uploadedFile2 = request.getFile('attachment2')
        anexo2.attachment = uploadedFile2?.getBytes() //converting the file to bytes
        anexo2.fileName=  uploadedFile2.originalFilename //getting the file name from the uploaded file
        anexo2.fileType = uploadedFile2.contentType//getting and storing the file type
        anexo2.setCredito(creditoInstance)
        def uploadedFile3 = request.getFile('attachment3')
        anexo3.attachment = uploadedFile3?.getBytes() //converting the file to bytes
        anexo3.fileName=  uploadedFile3.originalFilename //getting the file name from the uploaded file
        anexo3.fileType = uploadedFile3.contentType//getting and storing the file type
        anexo3.setCredito(creditoInstance)

        def uploadedFile4 = request.getFile('attachment4')
        anexo4.attachment = uploadedFile4?.getBytes() //converting the file to bytes
        anexo4.fileName=  uploadedFile4.originalFilename //getting the file name from the uploaded file
        anexo4.fileType = uploadedFile4.contentType//getting and storing the file type
        anexo4.setCredito(creditoInstance)

        def uploadedFile5 = request.getFile('attachment5')
        anexo5.attachment = uploadedFile5?.getBytes() //converting the file to bytes
        anexo5.fileName=  uploadedFile5.originalFilename //getting the file name from the uploaded file
        anexo5.fileType = uploadedFile5.contentType//getting and storing the file type
        anexo5.setCredito(creditoInstance)

        def uploadedFile6 = request.getFile('attachment6')
        anexo6.attachment = uploadedFile6?.getBytes() //converting the file to bytes
        anexo6.fileName=  uploadedFile6.originalFilename //getting the file name from the uploaded file
        anexo6.fileType = uploadedFile6.contentType//getting and storing the file type
        anexo6.setCredito(creditoInstance)

        def uploadedFile7 = request.getFile('attachment7')
        anexo7.attachment = uploadedFile7?.getBytes() //converting the file to bytes
        anexo7.fileName=  uploadedFile7.originalFilename //getting the file name from the uploaded file
        anexo7.fileType = uploadedFile7.contentType//getting and storing the file type
        anexo7.setCredito(creditoInstance)

        def uploadedFile8 = request.getFile('attachment8')
        anexo8.attachment = uploadedFile8?.getBytes() //converting the file to bytes
        anexo8.fileName=  uploadedFile8.originalFilename //getting the file name from the uploaded file
        anexo8.fileType = uploadedFile8.contentType//getting and storing the file type
        anexo8.setCredito(creditoInstance)

        def uploadedFile9 = request.getFile('attachment9')
        anexo9.attachment = uploadedFile9?.getBytes() //converting the file to bytes
        anexo9.fileName=  uploadedFile9.originalFilename //getting the file name from the uploaded file
        anexo9.fileType = uploadedFile9.contentType//getting and storing the file type
        anexo9.setCredito(creditoInstance)

        creditoInstance?.addToAnexos(anexo0)
        creditoInstance.anexos?.add(anexo1)
        creditoInstance.anexos?.add(anexo2)
        creditoInstance.anexos?.add(anexo3)
        creditoInstance.anexos?.add(anexo4)
        creditoInstance.anexos?.add(anexo5)
        creditoInstance.anexos?.add(anexo6)
        creditoInstance.anexos?.add(anexo7)
        creditoInstance.anexos?.add(anexo8)
        creditoInstance.anexos?.add(anexo9)


        creditoInstance.save flush: true
       //  pagamentoService.criarPagamentos(creditoInstance)
        //pagamentoService.criarPagamentos(creditoInstance)
        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Credito.label', default: 'Credito'), creditoInstance.id])
                redirect creditoInstance
            }
            '*' { respond creditoInstance, [status: OK] }
        }
        redirect(controller: "credito", action: "show",id: creditoInstance.id)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','CREDITO_DELETE'])
    def delete(Credito creditoInstance) {

        if (creditoInstance == null) {
            notFound()
            return
        }
        def pagamentos = Pagamento.findAllByCredito(creditoInstance)
        for (Pagamento p in pagamentos) {
            creditoInstance.removeFromPagamentos(p)
            p.getNotas().removeAll()
            p.remissoes.removeAll()
            p.notas.removeAll()
            p.delete(flash: true)
        }
        // pagamentos*.delete()
        creditoInstance.pagamentos.removeAll()
        creditoInstance.anexos.removeAll()
        def anexos = Anexo.findAllByCredito(creditoInstance)
        anexos*.delete()
        creditoInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Credito.label', default: 'Credito'), creditoInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'creditoInstance.label', default: 'Credito'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    @Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
    def simuladorDeCredito(){

    }

    @Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
            showSimulador(){


        int nper = params.meses as int
        BigDecimal pv = params.capital as BigDecimal
        BigDecimal rate = params.juros as BigDecimal
        Simulador simulador = new Simulador(nper: nper,pv:pv,rate: rate)
        simuladorService.simulador = simulador

    }
    @Secured(['ROLE_ADMIN','ROLE_ASSIS_ADMIN','ROLE_GESTOR_CREDITO'])
    def showAttachment(Anexo anexo){

        //retrieve photo code here
        response.setHeader("Content-disposition", "attachment; filename=${anexo.fileName}")
        response.contentType = anexo.fileType //'image/jpeg' will do too
        response.outputStream << anexo.attachment //'myphoto.jpg' will do too
        /* response.outputStream.flush()*/


    }


}
