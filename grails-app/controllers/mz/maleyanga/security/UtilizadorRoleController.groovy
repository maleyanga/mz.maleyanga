package mz.maleyanga.security

import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.mirzsoft.grails.actionlogging.annotation.ActionLogging
import org.mirzsoft.grails.actionlogging.annotation.SpringUserIdentification
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * UtilizadorRoleController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
@ActionLogging
@SpringUserIdentification
@Secured(['ROLE_ADMIN'])
class UtilizadorRoleController extends BasicController {

    static allowedMethods = [save: ["POST", "PUT"], update: ["PUT", "POST"], delete: "DELETE"]

    @Secured(['ROLE_ADMIN','UTILIZADOR_ROLE_INDEX'])
    def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond UtilizadorRole.list(params), model: [utilizadorRoleInstanceCount: UtilizadorRole.count()]
    }

    @Secured(['ROLE_ADMIN','UTILIZADOR_ROLE_LIST'])
    def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond UtilizadorRole.list(params), model: [utilizadorRoleInstanceCount: UtilizadorRole.count()]
    }

    @Secured(['ROLE_ADMIN','UTILIZADOR_ROLE_SHOW'])
    def show(UtilizadorRole utilizadorRoleInstance) {
        updateCurrentAction('show')
        respond utilizadorRoleInstance
    }

    @Secured(['ROLE_ADMIN','UTILIZADOR_ROLE_CREATE'])
      def create() {
        updateCurrentAction('create')
        respond new UtilizadorRole(params)
    }

    @Transactional
    @Secured(['ROLE_ADMIN','UTILIZADOR_ROLE_SAVE'])
    def save(UtilizadorRole utilizadorRoleInstance) {
        if (utilizadorRoleInstance == null) {
            notFound()
            return
        }

        if (utilizadorRoleInstance.hasErrors()) {
            respond utilizadorRoleInstance.errors, view: 'create'
            return
        }
        def user = Utilizador.find(utilizadorRoleInstance.utilizador)
        def rol = Role.find(utilizadorRoleInstance.role)
        UtilizadorRole.create user, rol, true

        request.withFormat {
          /*  form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'utilizadorRoleInstance.label', default: 'UtilizadorRole'), utilizadorRoleInstance.id])
                redirect utilizadorRoleInstance
            }*/
            '*' { respond utilizadorRoleInstance, [status: CREATED] }
        }
        redirect(action: "index")
    }

    @Secured(['ROLE_ADMIN','UTILIZADOR_ROLE_EDIT'])
    def edit(UtilizadorRole utilizadorRoleInstance) {
        updateCurrentAction('edit')
        respond utilizadorRoleInstance
    }

    @Transactional
    @Secured(['ROLE_ADMIN','UTILIZADOR_ROLE_UPDATE'])
    def update(UtilizadorRole utilizadorRoleInstance) {
        if (utilizadorRoleInstance == null) {
            notFound()
            return
        }

        if (utilizadorRoleInstance.hasErrors()) {
            respond utilizadorRoleInstance.errors, view: 'edit'
            return
        }

        utilizadorRoleInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UtilizadorRole.label', default: 'UtilizadorRole'), utilizadorRoleInstance.id])
                redirect utilizadorRoleInstance
            }
            '*' { respond utilizadorRoleInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','UTILIZADOR_ROLE_DELETE'])
    def delete(UtilizadorRole utilizadorRoleInstance) {

        if (utilizadorRoleInstance == null) {
            notFound()
            return
        }

        utilizadorRoleInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UtilizadorRole.label', default: 'UtilizadorRole'), utilizadorRoleInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'utilizadorRoleInstance.label', default: 'UtilizadorRole'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
