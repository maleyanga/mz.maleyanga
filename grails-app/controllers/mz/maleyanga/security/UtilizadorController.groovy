package mz.maleyanga.security

import grails.transaction.Transactional
import mz.maleyanga.BasicController
import org.mirzsoft.grails.actionlogging.annotation.ActionLogging
import org.mirzsoft.grails.actionlogging.annotation.SpringUserIdentification
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

/**
 * UtilizadorController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
@ActionLogging
@SpringUserIdentification
@Secured(['ROLE_ADMIN'])
class UtilizadorController extends BasicController {

    static allowedMethods = [save: ["POST", "PUT"], update: ["PUT", "POST"], delete: "DELETE"]

    @grails.plugin.springsecurity.annotation.Secured(['ROLE_ADMIN','UTILIZADOR_INDEX'])
    def index(Integer max) {
        updateCurrentAction('index')
        params.max = Math.min(max ?: 10, 100)
        respond Utilizador.list(params), model: [utilizadorInstanceCount: Utilizador.count()]
    }
    def logs(){}

    @grails.plugin.springsecurity.annotation.Secured(['ROLE_ADMIN','UTILIZADOR_LIST'])
    def list(Integer max) {
        updateCurrentAction('list')
        params.max = Math.min(max ?: 10, 100)
        respond Utilizador.list(params), model: [utilizadorInstanceCount: Utilizador.count()]
    }

    @grails.plugin.springsecurity.annotation.Secured(['ROLE_ADMIN','UTILIZADOR_SHOW'])
    def show(Utilizador utilizadorInstance) {
        updateCurrentAction('show')
        respond utilizadorInstance
    }

    @grails.plugin.springsecurity.annotation.Secured(['ROLE_ADMIN','UTILIZADOR_CREATE'])
    def create() {
        updateCurrentAction('create')
        respond new Utilizador(params)
    }

    @Transactional
    @grails.plugin.springsecurity.annotation.Secured(['ROLE_ADMIN','UTILIZADOR_SAVE'])
    def save(Utilizador utilizadorInstance) {

        if (utilizadorInstance == null) {
            notFound()
            return
        }

        if (utilizadorInstance.hasErrors()) {
            respond utilizadorInstance.errors, view: 'create'
            return
        }

        utilizadorInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'utilizadorInstance.label', default: 'Utilizador'), utilizadorInstance.id])
                redirect utilizadorInstance
            }
            '*' { respond utilizadorInstance, [status: CREATED] }
        }
    }

    @grails.plugin.springsecurity.annotation.Secured(['ROLE_ADMIN','UTILIZADOR_EDIT'])
    def edit(Utilizador utilizadorInstance) {
        updateCurrentAction('edit')
        respond utilizadorInstance
    }

    @Transactional
    @grails.plugin.springsecurity.annotation.Secured(['ROLE_ADMIN','UTILIZADOR_UPDATE'])
    def update(Utilizador utilizadorInstance) {
        if (utilizadorInstance == null) {
            notFound()
            return
        }

        if (utilizadorInstance.hasErrors()) {
            respond utilizadorInstance.errors, view: 'edit'
            return
        }

        utilizadorInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Utilizador.label', default: 'Utilizador'), utilizadorInstance.id])
                redirect utilizadorInstance
            }
            '*' { respond utilizadorInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN','UTILIZADOR_DELETE'])
    def delete(Utilizador utilizadorInstance) {

        if (utilizadorInstance == null) {
            notFound()
            return
        }
        def role = Role.findById(utilizadorInstance.id)
        utilizadorInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Utilizador.label', default: 'Utilizador'), utilizadorInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Secured(['ROLE_ADMIN','UTILIZADOR_UTILIZADOR_CRUD'])
    def utilizadorCrud(){}

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'utilizadorInstance.label', default: 'Utilizador'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
