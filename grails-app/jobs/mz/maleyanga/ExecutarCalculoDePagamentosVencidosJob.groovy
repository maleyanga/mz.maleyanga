package mz.maleyanga

import mz.maleyanga.cliente.Cliente
import mz.maleyanga.pagamento.Pagamento
import mz.maleyanga.settings.Settings
import mz.maleyanga.utilitarios.UtilitariosController


class ExecutarCalculoDePagamentosVencidosJob {

    PagamentoService pagamentoService
    UtilitariosController utilitariosController
    def contaService
    static triggers = {
        //3600000=1h
        simple startDelay: 60000, repeatInterval: 86400000
    }

    def execute() {
        def data = new Date()
        Calendar cal = Calendar.getInstance()

        Settings settings = Settings.findByNome("settings")
        // utilitariosController.sendSMS("envio de sms","843854654")
        def agora = new Date()
        System.println("calculo automatico de dividas " + agora)
        def pagamentos = Pagamento.all
        def clientes = Cliente.all
        if (settings.calcularAutomatico) {
            for (Pagamento p in pagamentos) {
                def result
                if (!p.pago) {
                   // pagamentoService.calcularMoras(p)
                    pagamentoService.calcularMoraCaPital(p)
                }

            }
        }


        for (Cliente c in clientes) {
            c.getClassificacao()
            c.merge()
        }


        // execute job
      //  pagamentoService.calcularPagamentosVencidos()
    }
}
