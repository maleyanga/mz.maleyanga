<%@ page import="mz.maleyanga.cliente.Cliente" %>
<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="kickstart"/>
    <g:set var="entityName" value="${message(code: 'cliente.label', default: 'Cliente')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-cliente" class="first">

    <table class="table">
        <tbody>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.nome.label" default="Nome"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "nome")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.nuit.label" default="Nuit"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "nuit")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.dataDeExpiracao.label"
                                                     default="Data De Expiracao"/></td>

            <td valign="top" class="value"><g:formatDate date="${clienteInstance?.dataDeExpiracao}"/></td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.numeroDeIndentificao.label"
                                                     default="Numero De Indentificao"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "numeroDeIndentificao")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.residencia.label" default="Residencia"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "residencia")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.email.label" default="Email"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "email")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.telefone.label" default="Telefone"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "telefone")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.telefone1.label" default="Telefone1"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "telefone1")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.telefone2.label" default="Telefone2"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "telefone2")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.tipoDeIndentificacao.label"
                                                     default="Tipo De Indentificacao"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "tipoDeIndentificacao")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.estadoCivil.label" default="Estado Civil"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "estadoCivil")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.classificacao.label" default="Classificacao"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "classificacao")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.entidade.label" default="Entidade"/></td>

            <td valign="top" class="value"><g:link controller="entidade" action="show"
                                                   id="${clienteInstance?.entidade?.id}">${clienteInstance?.entidade?.encodeAsHTML()}</g:link></td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.anexos.label" default="Anexos"/></td>

            <td valign="top" style="text-align: left;" class="value">
                <ul>
                    <g:each in="${clienteInstance.anexos}" var="a">
                        <li><g:link controller="anexo" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                    </g:each>
                </ul>
            </td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.ativo.label" default="Ativo"/></td>

            <td valign="top" class="value"><g:formatBoolean boolean="${clienteInstance?.ativo}"/></td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.assinantes.label" default="Assinantes"/></td>

            <td valign="top" style="text-align: left;" class="value">
                <ul>
                    <g:each in="${clienteInstance.assinantes}" var="a">
                        <li><g:link controller="assinante" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                    </g:each>
                </ul>
            </td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.utilizador.label" default="Utilizador"/></td>

            <td valign="top" class="value"><g:link controller="utilizador" action="show"
                                                   id="${clienteInstance?.utilizador?.id}">${clienteInstance?.utilizador?.encodeAsHTML()}</g:link></td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.localDeTrabalho.label"
                                                     default="Local De Trabalho"/></td>

            <td valign="top" class="value">${fieldValue(bean: clienteInstance, field: "localDeTrabalho")}</td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.anexo.label" default="Anexo"/></td>

            <td valign="top" class="value"><g:link controller="anexo" action="show"
                                                   id="${clienteInstance?.anexo?.id}">${clienteInstance?.anexo?.encodeAsHTML()}</g:link></td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.contas.label" default="Contas"/></td>

            <td valign="top" style="text-align: left;" class="value">
                <ul>
                    <g:each in="${clienteInstance.contas}" var="c">
                        <li><g:link controller="conta" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
                    </g:each>
                </ul>
            </td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.creditos.label" default="Creditos"/></td>

            <td valign="top" style="text-align: left;" class="value">
                <ul>
                    <g:each in="${clienteInstance.creditos}" var="c">
                        <li><g:link controller="credito" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
                    </g:each>
                </ul>
            </td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.dateCreated.label" default="Date Created"/></td>

            <td valign="top" class="value"><g:formatDate date="${clienteInstance?.dateCreated}"/></td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.lastUpdated.label" default="Last Updated"/></td>

            <td valign="top" class="value"><g:formatDate date="${clienteInstance?.lastUpdated}"/></td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name"><g:message code="cliente.pedidosDeCredito.label"
                                                     default="Pedidos De Credito"/></td>

            <td valign="top" style="text-align: left;" class="value">
                <ul>
                    <g:each in="${clienteInstance.pedidosDeCredito}" var="p">
                        <li><g:link controller="pedidoDeCredito" action="show"
                                    id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
                    </g:each>
                </ul>
            </td>

        </tr>

        </tbody>
    </table>
</section>

</body>

</html>
