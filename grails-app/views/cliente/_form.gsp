<%@ page import="mz.maleyanga.cliente.Cliente" %>


<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'nome', 'error')} required">
            <label for="nome" class="control-label"><g:message code="cliente.nome.label" default="Nome"/><span
                    class="required-indicator">*</span></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="nome" required="" value="${clienteInstance?.nome}"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'nome', 'error')}</span>
    </div>

</div>

<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'nuit', 'error')} ">
            <label for="nuit" class="control-label"><g:message code="cliente.nuit.label" default="Nuit"/><span
                    class="required-indicator">*</span></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="nuit" value="${clienteInstance?.nuit}"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'nuit', 'error')}</span>
    </div>

</div>

<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'dataDeExpiracao', 'error')} ">
            <label for="dataDeExpiracao" class="control-label"><g:message code="cliente.dataDeExpiracao.label"
                                                                          default="Data De Emissão"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <calendar:datePicker name="dataDeExpiracao" precision="day" value="${clienteInstance?.dataDeExpiracao}"
                             default="none" noSelection="['': '']"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'dataDeExpiracao', 'error')}</span>
    </div>

</div>

<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'estadoCivil', 'error')} ">
            <label for="estadoCivil" class="control-label"><g:message code="cliente.estadoCivil.label"
                                                                      default="Estado Civil"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:select class="form-control" name="estadoCivil" from="${clienteInstance.constraints.estadoCivil.inList}"
                  value="${clienteInstance?.estadoCivil}" valueMessagePrefix="cliente.estadoCivil"
                  noSelection="['': '']"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'estadoCivil', 'error')}</span>
    </div>

</div>

<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'tipoDeIndentificacao', 'error')} ">
            <label for="tipoDeIndentificacao" class="control-label"><g:message code="cliente.tipoDeIndentificacao.label"
                                                                               default="Tipo De Indentificacao"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:select class="form-control" name="tipoDeIndentificacao"
                  from="${clienteInstance.constraints.tipoDeIndentificacao.inList}"
                  value="${clienteInstance?.tipoDeIndentificacao}" valueMessagePrefix="cliente.tipoDeIndentificacao"
                  noSelection="['': '']"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'tipoDeIndentificacao', 'error')}</span>
    </div>

</div>

<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'numeroDeIndentificao', 'error')} ">
            <label for="numeroDeIndentificao" class="control-label"><g:message code="cliente.numeroDeIndentificao.label"
                                                                               default="Numero De Indentifição"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="numeroDeIndentificao" value="${clienteInstance?.numeroDeIndentificao}"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'numeroDeIndentificao', 'error')}</span>
    </div>

</div>

<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'residencia', 'error')} ">
            <label for="residencia" class="control-label"><g:message code="cliente.residencia.label"
                                                                     default="Residencia"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="residencia" value="${clienteInstance?.residencia}"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'residencia', 'error')}</span>
    </div>

</div>

<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'email', 'error')} ">
            <label for="email" class="control-label"><g:message code="cliente.email.label" default="Email"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="email" value="${clienteInstance?.email}"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'email', 'error')}</span>
    </div>

</div>

<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'telefone', 'error')} ">
            <label for="telefone" class="control-label"><g:message code="cliente.telefone.label"
                                                                   default="Telefone"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="telefone" maxlength="9" value="${clienteInstance?.telefone}"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'telefone', 'error')}</span>
    </div>

</div>


<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'telefone1', 'error')} ">
            <label for="telefone1" class="control-label"><g:message code="cliente.telefone1.label"
                                                                    default="Telefone1"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="telefone1" maxlength="9" value="${clienteInstance?.telefone1}"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'telefone1', 'error')}</span>
    </div>

</div>


<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'telefone2', 'error')} ">
            <label for="telefone2" class="control-label"><g:message code="cliente.telefone2.label"
                                                                    default="Telefone2"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="telefone2" maxlength="9" value="${clienteInstance?.telefone2}"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'telefone2', 'error')}</span>
    </div>

</div>


<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'localDeTrabalho', 'error')} ">
            <label for="localDeTrabalho" class="control-label"><g:message code="cliente.localDeTrabalho.label"
                                                                          default="Local De Trabalho / Negócio"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="localDeTrabalho" value="${clienteInstance?.localDeTrabalho}"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'localDeTrabalho', 'error')}</span>
    </div>

</div>


<div class="panel-body hidden">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'classificacao', 'error')} ">
            <label for="classificacao" class="control-label"><g:message code="cliente.classificacao.label"
                                                                        default="Classificacao"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:select class="form-control" name="classificacao" from="${clienteInstance.constraints.classificacao.inList}"
                  value="${clienteInstance?.classificacao}" valueMessagePrefix="cliente.classificacao"
                  noSelection="['': '']"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'classificacao', 'error')}</span>
    </div>

</div>


<div class="panel-body hidden">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'entidade', 'error')} required">
            <label for="entidade" class="control-label"><g:message code="cliente.entidade.label"
                                                                   default="Entidade"/><span
                    class="required-indicator">*</span></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:select class="form-control" id="entidade" name="entidade.id" from="${mz.maleyanga.entidade.Entidade.list()}"
                  optionKey="id" required="" value="${clienteInstance?.entidade?.id}" class="many-to-one"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'entidade', 'error')}</span>
    </div>

</div>


<div class="panel-body hidden">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'anexos', 'error')} ">
            <label for="anexos" class="control-label"><g:message code="cliente.anexos.label" default="Anexos"/></label>
        </div>
    </div>

    <div class="col-lg-6">

        <ul class="one-to-many hidden">
            <g:each in="${clienteInstance?.anexos ?}" var="a">
                <li><g:link controller="anexo" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
            </g:each>
            <li class="add">
                <g:link controller="anexo" action="create"
                        params="['cliente.id': clienteInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'anexo.label', default: 'Anexo')])}</g:link>
            </li>
        </ul>

        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'anexos', 'error')}</span>
    </div>

</div>


<div class="panel-body hidden">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'ativo', 'error')} ">
            <label for="ativo" class="control-label"><g:message code="cliente.ativo.label" default="Ativo"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <bs:checkBox name="ativo" value="${clienteInstance?.ativo}"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'ativo', 'error')}</span>
    </div>

</div>


<div class="panel-body hidden">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'assinantes', 'error')} ">
            <label for="assinantes" class="control-label"><g:message code="cliente.assinantes.label"
                                                                     default="Assinantes"/></label>
        </div>
    </div>

    <div class="col-lg-6">

        <ul class="one-to-many hidden">
            <g:each in="${clienteInstance?.assinantes ?}" var="a">
                <li><g:link controller="assinante" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
            </g:each>
            <li class="add">
                <g:link controller="assinante" action="create"
                        params="['cliente.id': clienteInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'assinante.label', default: 'Assinante')])}</g:link>
            </li>
        </ul>

        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'assinantes', 'error')}</span>
    </div>

</div>


<div class="panel-body">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'utilizador', 'error')} ">
            <label for="utilizador" class="control-label"><g:message code="cliente.utilizador.label"
                                                                     default="Gestor(a)"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:select class="form-control" id="utilizador" name="utilizador.id"
                  from="${mz.maleyanga.security.Utilizador.list()}" optionKey="id"
                  value="${clienteInstance?.utilizador?.id}" class="many-to-one" noSelection="['null': '']"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'utilizador', 'error')}</span>
    </div>

</div>


<div class="panel-body hidden">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'anexo', 'error')} ">
            <label for="anexo" class="control-label"><g:message code="cliente.anexo.label" default="Anexo"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:select class="form-control" id="anexo" name="anexo.id" from="${mz.maleyanga.documento.Anexo.list()}"
                  optionKey="id" value="${clienteInstance?.anexo?.id}" class="many-to-one" noSelection="['null': '']"/>
        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'anexo', 'error')}</span>
    </div>

</div>


<div class="panel-body hidden">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'contas', 'error')} ">
            <label for="contas" class="control-label"><g:message code="cliente.contas.label" default="Contas"/></label>
        </div>
    </div>

    <div class="col-lg-6">

        <ul class="one-to-many hidden">
            <g:each in="${clienteInstance?.contas ?}" var="c">
                <li><g:link controller="conta" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
            </g:each>
            <li class="add">
                <g:link controller="conta" action="create"
                        params="['cliente.id': clienteInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'conta.label', default: 'Conta')])}</g:link>
            </li>
        </ul>

        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'contas', 'error')}</span>
    </div>

</div>


<div class="panel-body hidden">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'creditos', 'error')} ">
            <label for="creditos" class="control-label"><g:message code="cliente.creditos.label"
                                                                   default="Creditos"/></label>
        </div>
    </div>

    <div class="col-lg-6">

        <ul class="one-to-many hidden">
            <g:each in="${clienteInstance?.creditos ?}" var="c">
                <li><g:link controller="credito" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
            </g:each>
            <li class="add">
                <g:link controller="credito" action="create"
                        params="['cliente.id': clienteInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'credito.label', default: 'Credito')])}</g:link>
            </li>
        </ul>

        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'creditos', 'error')}</span>
    </div>

</div>


<div class="panel-body hidden">
    <div class="col-lg-3">
        <div class="${hasErrors(bean: clienteInstance, field: 'pedidosDeCredito', 'error')} ">
            <label for="pedidosDeCredito" class="control-label"><g:message code="cliente.pedidosDeCredito.label"
                                                                           default="Pedidos De Credito"/></label>
        </div>
    </div>

    <div class="col-lg-6">

        <ul class="one-to-many hidden">
            <g:each in="${clienteInstance?.pedidosDeCredito ?}" var="p">
                <li><g:link controller="pedidoDeCredito" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
            </g:each>
            <li class="add">
                <g:link controller="pedidoDeCredito" action="create"
                        params="['cliente.id': clienteInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'pedidoDeCredito.label', default: 'PedidoDeCredito')])}</g:link>
            </li>
        </ul>

        <span class="help-inline">${hasErrors(bean: clienteInstance, field: 'pedidosDeCredito', 'error')}</span>
    </div>

</div>



