<%--
  Created by IntelliJ IDEA.
  User: Claudino
  Date: 13/11/2015
  Time: 12:55
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="kickstart" />
</head>

<body>
<sec:ifNotGranted roles="UTILITARIOS_INDEX">
    <g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('UTILITARIOS_INDEX')">
<section>
    <g:render template="uplaodClienteFile"/>
    <span class="label label-default">Mostrar o formato para a inserção de dados em excel!</span>
    <div>

        <g:submitButton class="btn btn-outline-inverse" onclick=" change_autorefreshdiv()" name="show_format" value="Show" />
    </div>
    <p id="demo"></p>
    <div id="myexcel"  class="hidden">
        <h2>Basic Table</h2>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>nome</th>
                <th>nuit</th>
                <th>tipoDeIndentificacao</th>
                <th> numeroDeIndentificao</th>
                <th> residencia</th>
                <th> data expiracao</th>
                <th>email</th>
                <th>telefone</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>John</td>
                <td>100001</td>
                <td>BI</td>
                <td>100000</td>
                <td>Bairro Jorje Dimitrov</td>
                <td>1/12/16</td>
                <td>john@example.com</td>
                <td>844444444</td>
            </tr>
            <tr>
                <td>Mary Cris</td>
                <td>20000002</td>
                <td>Passaporte</td>
                <td>9000000</td>
                <td>Bairro Jardim, Rua da Beira No. 1</td>
                <td>20/12/20</td>
                <td>mary@example.com</td>
                <td>82222222</td>
            </tr>
            <tr>
                <td>Jully Dooley </td>
                <td>8000008</td>
                <td>BI</td>
                <td>1000001</td>
                <td>Bairro Cumbeza,Q 23, casa No. 12</td>
                <td>23/5/17</td>
                <td>july@example.com</td>
                <td>860000000</td>
            </tr>
            </tbody>
        </table>
    </div>
</section>
</sec:access>
<script>
    function change_autorefreshdiv(){
        var NAME = document.getElementById("myexcel");
        var currentClass = NAME.className;
        if (currentClass == "hidden") { // Check the current class name
            NAME.className = "container";   // Set other class name
        } else {
            NAME.className = "hidden";  // Otherwise, use `second_name`
        }
    }
</script>
</body>
</html>