
<%@ page import="mz.maleyanga.cliente.Cliente" %>
<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="kickstart" />
    <g:set var="entityName" value="${message(code: 'cliente.label', default: 'Cliente')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<sec:ifNotGranted roles="UTILITARIOS_LIST">
    <g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('UTILITARIOS_LIST')">
<section id="list-cliente" class="first">

    <table class="table table-bordered margin-top-medium">
        <thead>
        <tr>


            <g:sortableColumn property="nome" title="${message(code: 'cliente.nome.label', default: 'Nome')}" />

            <g:sortableColumn property="nuit" title="${message(code: 'cliente.nuit.label', default: 'Nuit')}" />

            <g:sortableColumn property="tipoDeIndentificacao" title="${message(code: 'cliente.dataDeExpiracao.label', default: 'tipo de indentificacao')}" />

            <g:sortableColumn property="numeroDeIndentificao" title="${message(code: 'cliente.numeroDeIndentificao.label', default: 'Numero De Indentificao')}" />
         <g:sortableColumn property="residencia" title="${message(code: 'cliente.residencia.label', default: 'Residencia')}" />
            <g:sortableColumn property="dataDeExpiracao" title="${message(code: 'cliente.residencia.label', default: 'data de expiracao')}" />
            <g:sortableColumn property="email" title="${message(code: 'cliente.residencia.label', default: 'Email')}" />

            <g:sortableColumn property="telefone" title="${message(code: 'cliente.residencia.label', default: 'telefone')}" />

        </tr>
        </thead>
        <tbody>
        <g:each in="${clienteInstanceList}" status="i" var="clienteInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                %{--   private final static int nome = 0
                           private final static int nuit = 1
                           private final static int tipoIndentificacao = 2
                           private final static int numeroIndentificacao = 3
                           private final static int residencia = 4
                           private final static int dataExpiracao = 5
                           private final static int email = 6
                           private final static int telefone = 7--}%
                <td><g:link action="show" id="${clienteInstance.id}">${fieldValue(bean: clienteInstance, field: "nome")}</g:link></td>

                 <td>${fieldValue(bean: clienteInstance, field: "nuit")}</td>
                <td>${fieldValue(bean: clienteInstance, field: "tipoDeIndentificacao")}</td>
                <td>${fieldValue(bean: clienteInstance, field: "numeroDeIndentificao")}</td>
                <td>${fieldValue(bean: clienteInstance, field: "residencia")}</td>
                <td><g:formatDate format="dd-MM-yyyy" date="${clienteInstance.dataDeExpiracao}" /></td>
                <td>${fieldValue(bean: clienteInstance, field: "email")}</td>
                <td>${fieldValue(bean: clienteInstance, field: "telefone")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>
    <div>
        <bs:paginate total="${clienteInstanceCount}" />
    </div>
</section>
</sec:access>
</body>

</html>
