
<%@ page import="mz.maleyanga.pagamento.Parcela" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'parcela.label', default: 'Parcela')}" />
    <g:set var="id" value="${parcelaInstance.pagamento.id}"/>
    <g:set var="id_parcela" value="${parcelaInstance.id}"/>
	<title><g:message code="default.show.label" args="[entityName]" /></title>

</head>

<body>
<sec:ifNotGranted roles="PARCELA_SHOW">
	<g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('PARCELA_SHOW')">
<g:if test="${parcelaInstance.pagamento.valorDeJuros == parcelaInstance.valorParcial && !parcelaInstance.pagamento.pago}">
    <h4 class="label-info "
        style="font-style: italic; color: firebrick">O valor Pago corresponde a quantia de juros desta prestação.

    </h4>
    <g:link controller="pagamento" action="gerarNovaPrestacao" params="[id: id, id_parcela: id_parcela]"
            class="form-horizontal" role="form">
        <glyph:icon iconName="new"/>    Gerar Novo Pagamento
    </g:link>

</g:if>
<section id="show-parcela" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="parcela.valorParcial.label" default="Valor Parcial" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: parcelaInstance, field: "valorParcial")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="parcela.pagamento.label" default="Pagamento" /></td>
				
				<td valign="top" class="value"><g:link controller="pagamento" action="show" id="${parcelaInstance?.pagamento?.id}">${parcelaInstance?.pagamento?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="parcela.dataDePagamento.label" default="Data De Pagamento" /></td>
				
				<td valign="top" class="value"><g:formatDate format="dd/MM/yy" date="${parcelaInstance?.dataDePagamento}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="parcela.descricao.label" default="Descricao" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: parcelaInstance, field: "descricao")}</td>
				
			</tr>

		%{--<tr class="prop">
            <td valign="top" class="name"><g:message code="parcela.anexos.label" default="Anexo" /></td>

            <td valign="top" style="text-align: left;" class="value">
                <ul>
                <g:each in="${parcelaInstance.anexo}" var="a">
                    <li><g:link controller="anexo" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                </g:each>
                </ul>
            </td>

        </tr>--}%
		
		</tbody>
	</table>
    <g:if test="${parcelaInstance?.anexo}">

    </g:if>
	<g:render template="anexo_show"/>
</section>
<g:render template="imprimirExtrato"/>
</sec:access>
</body>

</html>
