<g:form action="imprimirReceitas" class="button-bar" role="form"  >
    <div class="panel-body">
        <div class="col-lg-2">
            <div >
                <label for="dataDeExpiracao" class="control-label"><g:message code="cliente.dataDeExpiracao.label" default="De" /><span class="required-indicator">*</span></label>
            </div>
        </div>
        <div class="col-lg-6">
            <calendar:datePicker  name="inicio" precision="day"  />
        </div>
    </div>
    <div class="panel-body">
        <div class="col-lg-2">
            <div >
                <label for="dataDeExpiracao" class="control-label"><g:message code="cliente.dataDeExpiracao.label" default="A" /><span class="required-indicator">*</span></label>
            </div>
        </div>
        <div class="col-lg-6">
            <calendar:datePicker  name="fim" precision="day"  />
        </div>
    </div>
    <g:submitButton name="create" class="btn btn-danger" value="Imprimir" />

</g:form>