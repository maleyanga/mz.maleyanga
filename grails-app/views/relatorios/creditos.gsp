<%--
  Created by IntelliJ IDEA.
  User: Claudino
  Date: 11/10/2015
  Time: 20:47
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="kickstart"/>
    <title>Relatório de Créditos</title>

</head>

<body>
<g:render template="/_menu/menurelatorios"/>
<sec:ifNotGranted roles="RELATORIOS_CREDITOS">
    <g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('RELATORIOS_CREDITOS')">
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <p style="background: #00b3ee"
               class="panel-title">Este Relatório esta Baseado Nas Datas De Conceção dos Créditos</p>
            <div class="panel-heading">
                <h3 class="panel-title">Resumo de Créditos</h3>

                <g:render template="creditosReport"/>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <p style="background: #00b3ee"
               class="panel-title">Este Relatório esta Baseado Nas Datas de Feicho dos Créditos</p>

            <div class="panel-heading">
                <h3 class="panel-title">Balanço Geral de Créditos Fechados</h3>
                <g:render template="balancoGeralDeRecebimentosFEchados"/>
            </div>
        </div>
    </div>
</div>
</sec:access>

</body>
</html>