<%--
  Created by IntelliJ IDEA.
  User: Claudino
  Date: 11/05/2016
  Time: 05:26
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="kickstart"/>
    <title>Relatórios-Prestações</title>
</head>

<body>
<g:render template="/_menu/menurelatorios"/>
<sec:ifNotGranted roles="RELATORIOS_IMPRIMIR_PRESTACAO">
    <g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('RELATORIOS_IMPRIMIR_PRESTACAO')">
    <z:body/>
%{-- <div class="row">
     <div class="col-md-4">
         <div class="panel panel-primary">
             <div class="panel-heading">
                 <h4 class="panel-title"><span class="z-combobutton-button"><glyph:icon
                         iconName="atraso"/>Todas Prestações Atrasadas</span></h4>
             </div>

             <div class="panel-body">
                 <g:form action="imprimirPrestacoesAtrasadas" class="button-bar" role="form">

                     <g:submitButton name="create" class="btn btn-danger" value="Imprimir"/>
                     <g:select name="ext" from="${["pdf", "doc", "docx", "html", "xls", "xlsx", "ppt"]}"/>
                 </g:form>

             </div>
         </div>
     </div>

     <div class="col-md-4">
     <div class="panel panel-primary">
         <div class="panel-heading">
             <h4 class="panel-title"><span class="z-combobutton-button"><glyph:icon
                     iconName="atraso"/> Prestações em atraso</span></h4>
         </div>

         <div class="panel-body">
            <z:body/>
         </div>
     </div>
 </div>
</div>--}%
</sec:access>
</body>
</html>