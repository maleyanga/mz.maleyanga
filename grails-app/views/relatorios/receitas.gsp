<%--
  Created by IntelliJ IDEA.
  User: Claudino
  Date: 11/10/2015
  Time: 20:47
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="kickstart"/>
    <title>Relatórios-Receitas</title>

</head>

<body>

<sec:ifNotGranted roles="RELATORIOS_RECEITAS">
    <g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('RELATORIOS_RECEITAS')">
    <g:render template="/_menu/menurelatorios"/>
<section>
    <h3><span class="label-info">Estes relatórios se baseia nas datas de Pagamento</span>
    </h3>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Recebimentos Globais</h3>
                    <g:render template="receitasForm"/>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Balanço De Recebimentos</h3>
                    <g:render template="balancoRecebimentosForm"/>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Balanço De Recebimentos De Créditos Não Fechados</h3>
                    <g:render template="balancoGeralDeRecebimentos"/>
                </div>
            </div>
        </div>
    </div>
</section>
</sec:access>

</body>
</html>