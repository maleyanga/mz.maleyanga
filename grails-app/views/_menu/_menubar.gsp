<div class="">
	<ul class="nav nav-tabs " data-role="listview" data-split-icon="gear" data-filter="true">
	
		<g:each status="i" var="c" in="${grailsApplication.controllerClasses.sort { it.logicalPropertyName } }">
			<g:if test="${c.logicalPropertyName!="basic"}">
				<g:if test="${c.logicalPropertyName!="devZul"}">
				<g:if test="${c.logicalPropertyName!="dbdoc"}">
				<g:if test="${c.logicalPropertyName!="DemoPage"}">
				<g:if test="${c.logicalPropertyName!="worksite"}">
				<g:if test="${c.logicalPropertyName!="home"}">
				<g:if test="${c.logicalPropertyName!="attachmentable"}">
				<g:if test="${c.logicalPropertyName != "nge2e"}">

                    <g:if test="${c.logicalPropertyName != "estado"}">

                            <g:if test="${c.logicalPropertyName != "role"}">
                                <g:if test="${c.logicalPropertyName != "utilizador"}">
                                    <g:if test="${c.logicalPropertyName != "login"}">
                                        <g:if test="${c.logicalPropertyName != "utilizadorRole"}">
                                            <g:if test="${c.logicalPropertyName != "logout"}">
                                                <g:if test="${c.logicalPropertyName != "banco"}">
                                                    <g:if test="${c.logicalPropertyName != "actionLoggingEvent"}">
                                                        <g:if test="${c.logicalPropertyName != "actionLoggingSample"}">
                                                            <g:if test="${c.logicalPropertyName != "_DemoPage"}">
                                                                <g:if test="${c.logicalPropertyName != "roleGroup"}">
                                                                    <g:if test="${c.logicalPropertyName != "test"}">

                                                                        <g:if test="${c.logicalPropertyName != "searchable"}">
                                                                            <g:if test="${c.logicalPropertyName != "entidade"}">
                                                                                <g:if test="${c.logicalPropertyName != "contact"}">
                                                                                    <g:if test="${c.logicalPropertyName != "anexo"}">
                                                                                        <g:if test="${c.logicalPropertyName != "parcela"}">
                                                                                            <g:if test="${c.logicalPropertyName != "remissao"}">
                                                                                                <g:if test="${c.logicalPropertyName != "taxa"}">
                                                                                                    <g:if test="${c.logicalPropertyName != "assinante"}">
                                                                                                        <g:if test="${c.logicalPropertyName != "periodo"}">
                                                                                                            <g:if test="${c.logicalPropertyName != "logs"}">
                                                                                                                <g:if test="${c.logicalPropertyName != "utilitarios"}">
                                                                                                                    <g:if test="${c.logicalPropertyName != "nota"}">
                                                                                                                        <g:if test="${c.logicalPropertyName != "ozekimessagein"}">
                                                                                                                            <g:if test="${c.logicalPropertyName != "ozekimessageout"}">
                                                                                                                                <g:if test="${c.logicalPropertyName != "smslog"}">
                                                                                                                                    <g:if test="${c.logicalPropertyName != "demoPage"}">
                                                                                                                                        <g:if test="${c.logicalPropertyName != "enridade"}">
                                                                                                                                            <g:if test="${c.logicalPropertyName != "settings"}">
                                                                                                                                                <g:if test="${c.logicalPropertyName != "simulador"}">
                                                                                                                                                    <g:if test="${c.logicalPropertyName != "feriado"}">

                                                                                                                                                        <li class="controller${params.controller == c.logicalPropertyName ? " active" : ""}">
                                                                                                                                                            <g:link controller="${c.logicalPropertyName}"
                                                                                                                                                                    action="index">
                                                                                                                                                                <g:if test="${c.logicalPropertyName == 'cliente'}">
                                                                                                                                                                    <glyph:icon
                                                                                                                                                                            iconName="clientes"/>    <g:message
                                                                                                                                                                        code="${c.logicalPropertyName}.label"
                                                                                                                                                                        default="${c.logicalPropertyName.capitalize()}"/>

                                                                                                                                                                </g:if>
                                                                                                                                                                <g:if test="${c.logicalPropertyName == 'credito'}">
                                                                                                                                                                    <glyph:icon
                                                                                                                                                                            iconName="credito"/>    <g:message
                                                                                                                                                                        code="${c.logicalPropertyName}.label"
                                                                                                                                                                        default="${c.logicalPropertyName.capitalize()}"/>
                                                                                                                                                                </g:if>
                                                                                                                                                                <g:if test="${c.logicalPropertyName == 'pedidoDeCredito'}">
                                                                                                                                                                    <glyph:icon
                                                                                                                                                                            iconName="pedido"/>    <g:message
                                                                                                                                                                        code="${c.logicalPropertyName}.label"
                                                                                                                                                                        default="${c.logicalPropertyName.capitalize()}"/>
                                                                                                                                                                </g:if>
                                                                                                                                                                <g:if test="${c.logicalPropertyName == 'pagamento'}">
                                                                                                                                                                    <glyph:icon
                                                                                                                                                                            iconName="caixa"/>    <g:message
                                                                                                                                                                        code="${c.logicalPropertyName}.label"
                                                                                                                                                                        default="${c.logicalPropertyName.capitalize()}"/>
                                                                                                                                                                </g:if>
                                                                                                                                                                <g:if test="${c.logicalPropertyName == 'conta'}">
                                                                                                                                                                    <glyph:icon
                                                                                                                                                                            iconName="contas"/>    <g:message
                                                                                                                                                                        code="${c.logicalPropertyName}.label"
                                                                                                                                                                        default="${c.logicalPropertyName.capitalize()}"/>
                                                                                                                                                                </g:if>


                                                                                                                                                                <g:if test="${c.logicalPropertyName == 'relatorios'}">
                                                                                                                                                                    <glyph:icon
                                                                                                                                                                            iconName="relatorios_32"/>    <g:message
                                                                                                                                                                        code="${c.logicalPropertyName}.label"
                                                                                                                                                                        default="${c.logicalPropertyName.capitalize()}"/>
                                                                                                                                                                </g:if>
                                                                                                                                                                <g:if test="${c.logicalPropertyName == 'diario'}">
                                                                                                                                                                    <glyph:icon
                                                                                                                                                                            iconName="diarios"/>    <g:message
                                                                                                                                                                        code="${c.logicalPropertyName}.label"
                                                                                                                                                                        default="${c.logicalPropertyName.capitalize()}"/>
                                                                                                                                                                </g:if>

                                                                                                                                                            </g:link>

                                                                                                                                                        </li>

                                                                                                                                                    </g:if>
                                                                                                                                                </g:if>
                                                                                                                                            </g:if>
                                                                                                                                        </g:if>
                                                                                                                                    </g:if>
                                                                                                                                </g:if>
                                                                                                                            </g:if>
                                                                                                                        </g:if>
                                                                                                                    </g:if>
                                                                                                                </g:if>
                                                                                                            </g:if>
                                                                                                        </g:if>
                                                                                                    </g:if>
                                                                                                </g:if>
                                                                                            </g:if>
                                                                                        </g:if>
                                                                                    </g:if>
                                                                                </g:if>
                                                                            </g:if>
                                                                        </g:if>
                                                                    </g:if>
                                                                </g:if>
                                                            </g:if>
                                                        </g:if>
                                                    </g:if>
                                                </g:if>
                                            </g:if>
                                        </g:if>
                                    </g:if>
                                </g:if>
                            </g:if>

                    </g:if>
                </g:if>
                </g:if>
                </g:if>
                </g:if>
                </g:if>
                </g:if>
                </g:if>
			</g:if>

			%{--</g:if>--}%

		</g:each>

	</ul>
</div>
