<g:form controller="searchable" action="index" method="post" class="navbar-form navbar-left" >
	<div class="form-group fieldcontain text-center">
		<input name="q" value="${params.q}"  type="text" class="form-control nav-search" placeholder="${message(code: 'search.navbar.placeholder', default: 'Search ...')}" value="${query}">
	</div>

</g:form>
<script type="text/javascript">
	var focusQueryInput = function() {
		document.getElementById("q").focus();
	}
</script>