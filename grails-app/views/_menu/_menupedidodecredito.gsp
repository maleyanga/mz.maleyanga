<ul class="nav nav-pills" data-role="listview" data-split-icon="gear" data-filter="true">
    %{--<li class="${ params.action == "create" ? 'active' : '' }">  <g:link controller="pedido" action="create"> <g:img dir="images" file="file_add.png"  uri="" />Novo Pedido</g:link></li>--}%
    <li class="${params.action == "create" ? 'active' : ''}"><g:link controller="pedidoDeCredito"
                                                                     action="create"><glyph:icon
                iconName="new"/>Novo Pedido</g:link></li>
    <li class="${params.action == "listAbertos" ? 'active' : ''}"><g:link controller="pedidoDeCredito"
                                                                          action="listAbertos"><glyph:icon
                iconName="pedidosAbertos"/>Pedidos Abertos</g:link></li>
    <li class="${params.action == "listPendentes" ? 'active' : ''}"><g:link controller="pedidoDeCredito"
                                                                            action="listPendentes"><glyph:icon
                iconName="pedidosPendentes"/> Pedidos Pendentes</g:link></li>
    <li class="${params.action == "listFechados" ? 'active' : ''}"><g:link controller="pedidoDeCredito"
                                                                           action="listFechados"><glyph:icon
                iconName="pedidosFechados"/> Pedidos Fechados</g:link></li>
    <li class="${params.action == "listAprovados" ? 'active' : ''}"><g:link controller="pedidoDeCredito"
                                                                            action="listAprovados"><glyph:icon
                iconName="pedidosAprovados"/> Pedidos Aprovados</g:link></li>
    %{--
        <li class="${ params.action == "listAll" ? 'active' : '' }">  <g:link  controller="pedidoDeCredito" action="listAll"> <glyph:icon iconName="todos"/>Todos Pedidos</g:link></li>
    --}%
    %{-- <g:link onclick="${'Pedidos'}" controller="pedido" action="listPendentes"> <g:img dir="images" file="all.png"  /><span class="alert-info">Pendentes</span></g:link>--}%
</ul>
