<ul class="nav nav-pills" data-role="listview" data-split-icon="gear" data-filter="true">
    %{--<li class="${ params.action == "create" ? 'active' : '' }">  <g:link controller="credito" action="create"> Novo Credito</g:link></li>--}%
    <li class="${params.action == "listAbertos" ? 'active' : ''}"><g:link controller="credito"
                                                                          action="listAbertos">${mz.maleyanga.credito.Credito.findAllByEstado("Aberto").size()}<glyph:icon
                iconName="creditosAbertos"/>Novos Creditos</g:link></li>
    <li class="${params.action == "listEmProgresso" ? 'active' : ''}"><g:link controller="credito"
                                                                              action="listEmProgresso">${mz.maleyanga.credito.Credito.findAllByEstado("EmProgresso").size()}<glyph:icon
                iconName="progress"/>Creditos Em Pagamento</g:link></li>
    <li class="${params.action == "listPendentes" ? 'active' : ''}"><g:link controller="credito"
                                                                            action="listPendentes">${mz.maleyanga.credito.Credito.findAllByEstado("Pendente").size()}   <glyph:icon
                iconName="creditosPendentes"/> Creditos Em Atraso</g:link></li>
    <li class="${params.action == "listFechados" ? 'active' : ''}"><g:link controller="credito"
                                                                           action="listFechados">${mz.maleyanga.credito.Credito.findAllByEstado("Fechado").size()}<glyph:icon
                iconName="creditosfechados"/>Creditos Fechados</g:link></li>
    %{--
        <li class="${ params.action == "listAll" ? 'active' : '' }">  <g:link  controller="credito" action="listAll">${mz.maleyanga.credito.Credito.all.size()} <glyph:icon iconName="todos"/>Todos Creditos</g:link></li>
    --}%
    %{-- <g:link onclick="${'Pedidos'}" controller="pedido" action="listPendentes"> <g:img dir="images" file="all.png"  /><span class="alert-info">Pendentes</span></g:link>--}%

</ul>