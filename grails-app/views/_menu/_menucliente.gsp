<ul class="nav nav-pills" data-role="listview" data-split-icon="gear" data-filter="true">
    <li class="${ params.action == "create" ? 'active' : '' }">  <g:link controller="assinante" action="create"><glyph:icon iconName="novo"/>Novo Assinante</g:link></li>
    <li class="${ params.action == "list" ? 'active' : '' }">  <g:link controller="assinante" action="list"><glyph:icon iconName="todos"/>Lista Dos Assinantes</g:link></li>
    <li class="${ params.action == "listExcelente" ? 'active' : '' }">  <g:link controller="cliente" action="listExcelente"><glyph:icon iconName="excelente"/>Clientes Excelentes</g:link></li>
    <li class="${ params.action == "listBom" ? 'active' : '' }">  <g:link controller="cliente" action="listBom"><glyph:icon iconName="bom"/>Clientes Boms</g:link></li>
    <li class="${ params.action == "listMedios" ? 'active' : '' }">  <g:link controller="cliente" action="listMedios"><glyph:icon iconName="medio"/>Clientes Medios</g:link></li>
    <li class="${ params.action == "listMaus" ? 'active' : '' }">  <g:link controller="cliente" action="listMaus"><glyph:icon iconName="mau"/>Clientes Maus</g:link></li>
    <li class="${params.action == "blackList" ? 'active' : ''}"><g:link controller="cliente"
                                                                        action="blackList"><glyph:icon
                iconName="pessimo"/>Pessimos</g:link></li>
    <li class="${params.action == "listPessimos" ? 'active' : ''}"><g:link controller="cliente"
                                                                           action="listPessimos"><glyph:icon
                iconName="black"/>Black List</g:link></li>

</ul>