<!-- 
This menu is used to show function that can be triggered on the content (an object or list of objects).
-->
<ul class="nav nav-pills" data-role="listview" data-split-icon="gear" data-filter="true">
<%-- Only show the "Pills" navigation menu if a controller exists (but not for home) --%>
<g:if test="${	params.controller != null
			&&	params.controller != ''
			&&	params.controller != 'home'
			&&	params.controller != 'searchable'
			&&	params.controller != 'settings'
			&&	params.controller != 'utilizador'
			&&	params.controller != 'roleGroup'
&& params . controller != 'feriado'


}">
	<ul id="Menu" class="nav nav-pills margin-top-small">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		<g:if test="${params.controller !='pedidoDeCredito'}">
		<g:if test="${params.controller !='pagamento'}">
		<g:if test="${params.controller !='credito'}">
		<g:if test="${params.controller !='relatorios'}">
			%{--Listagem --}%
		%{--<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><glyph:icon iconName="todos"/></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>--}%
		</g:if>
		</g:if>
		</g:if>
		</g:if>
		<g:if test="${params.controller !='utilitarios'}">
            <g:if test="${params.controller != 'conta'}">
                <g:if test="${params.controller != 'entidade'}">
                    <g:if test="${params.controller !='relatorios'&&params.action!='show'}">
                        <g:if test="${params.controller !='pagamento'&&params.action!='show'}">
                            <li class="${ params.action == "create" ? 'active' : '' }">
                                <g:link action="create"><glyph:icon iconName="novo"/> <g:message code="default.new.label" args="[entityName]"/></g:link>
                            </li>
                        </g:if>
                    </g:if>
                </g:if>
            </g:if>
		</g:if>
</g:if>
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
            <g:if test="${params.controller != 'pagamento' || params.action != 'show'}">
                <!-- the item is an object (not a list) -->
                <li class="${params.action == "edit" ? 'active' : ''}">
                    <g:link action="edit" id="${params.id}"><glyph:icon iconName="glyphicons_150_edit"/> <g:message
                            code="default.edit.label" args="[entityName]"/></g:link>
                </li>
                <li class="">
                    <g:render template="/_common/modals/deleteTextLink"/>
                </li>
            </g:if>
        </g:if>
    %{--<g:if test="${ params.controller=='pedidoDeCredito' }">
			<g:if test="${params.action!='show'}">
			<g:if test="${params.action!='edit'}">
			<g:if test="${params.action!='create'}">
			<g:render template="/_menu/menupedidodecredito"/>
			</g:if>
			</g:if>
			</g:if>
		</g:if>--}%
    <g:if test="${ params.controller=='credito' }">
    <g:if test="${params.action!='show'}">
			<g:if test="${params.action!='edit'}">
			<g:if test="${params.action!='create'}">

			<g:render template="/_menu/menucredito"/>
			</g:if>
			</g:if>
			</g:if>
		</g:if>
	<g:if test="${ params.controller=='cliente' }">
	<g:if test="${params.action!='show'}">
	<g:if test="${params.action!='edit'}">
	<g:if test="${params.action!='create'}">

%{--	<g:render template="/_menu/menucliente"/>--}%
	</g:if>
	</g:if>
	</g:if>
	</g:if>
	<g:if test="${ params.controller=='utilitarios' }">
	<g:if test="${params.action!='show'}">
	<g:if test="${params.action!='edit'}">
	<g:if test="${params.action!='create'}">

	<g:render template="/_menu/menuutilitarios"/>
	</g:if>
	</g:if>
	</g:if>
	</g:if>
		<g:if test="${ params.controller=='relatorios' }">
			<g:render template="/_menu/menurelatorios"/>
		</g:if>
		<g:if test="${ params.controller=='pagamento' }">
			<g:if test="${params.action!='show'}">
			<g:if test="${params.action!='edit'}">
			<g:if test="${params.action!='create'}">
			<g:render template="/_menu/menuprestacoes"/>
			</g:if>
			</g:if>
            </g:if>
            <g:if test="${params.controller == 'entidade'}">
                <g:render template="/_menu/menuentidade"/>
            </g:if>
		%{--</g:if>--}%
	</ul>
</g:if>
</ul>