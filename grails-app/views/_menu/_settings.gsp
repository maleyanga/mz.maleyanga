<li class="dropdown dropdown-btn">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <glyph:icon iconName="glyphicons_439_wrench"/>
        <g:message code="default.settings.label"/> <b></b>
    </a>

    <ul class="dropdown-menu">
        <%-- Note: Links to pages without controller are redirected in conf/UrlMappings.groovy --%>
        <li class="">
            <g:link controller="diario" action="diario">Diário</g:link>
            <g:link controller="utilizador" action="utilizadorCrud">Gestão de Utilizadores</g:link>
            <g:link controller="settings" action="defCredito">Defenições de crédito</g:link>
            <g:link controller="entidade" action="index">Minha Entidade</g:link>
            <g:link controller="taxa" action="taxas">Taxas</g:link>
            <g:link controller="settings" action="index">Selecionar Dias permitidos para a geração de crédito</g:link>
            <g:link controller="feriado" action="feriadoCrud">Definição de Feriádos</g:link>
            <g:link controller="utilitarios" action="index">Utilitarios</g:link>
            <g:link controller="utilitarios" action="userguide">Manual do Utilizador</g:link>

        </li>

    </ul>
</li>
