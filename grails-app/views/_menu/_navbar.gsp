<nav id="Navbar" class="navbar navbar-fixed-top navbar-inverse" role="navigation">
	<div class="container">
	
	    <div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        		<span class="sr-only">Toggle navigation</span>
        		<span class="icon-bar"></span>
	           	<span class="icon-bar"></span>
	           	<span class="icon-bar"></span>
			</button>
	
			<a class="navbar-brand" href="${createLink(uri: '/home')}">
                <img style="color: #5EB544" class="logo" src="${resource(dir: 'images', file: 'maleyanga.png')}"
                     alt="${meta(name: 'app.name')}" width="37px" height="32px"/>
				${meta(name:'app.name')}
				<small> v${meta(name:'app.version')}</small>
			</a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse" role="navigation">

		<ul class="nav navbar-nav">
%{--<sec:access expression="hasRole('ROLE_ADMIN')">
	<g:render template="/_menu/controller"/>
</sec:access>--}%

		</ul>

    	<ul class="nav navbar-nav navbar-right">
 			<g:render template="/_menu/search"/> 
			%{--<g:render template="/_menu/admin"/>				--}%
			<g:render template="/_menu/entidade"/>
			%{--<g:render template="/_menu/info"/>--}%

			<g:render template="/_menu/settings"/>
			<g:render template="/_menu/user"/><!-- NOTE: the renderDialog for the "Register" modal dialog MUST be placed outside the NavBar (at least for Bootstrap 2.1.1): see bottom of main.gsp -->
			<g:render template="/_menu/language"/>
			<li><span class="label label-IEfix">${sec.loggedInUserInfo(field: 'username')}</span></li>
	    </ul>			

		</div>
	</div>
</nav>
