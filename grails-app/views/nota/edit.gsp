<%@ page import="mz.maleyanga.documento.Nota" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'nota.label', default: 'Nota')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>

<body>
<sec:ifNotGranted roles="NOTA_EDIT">
	<g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('NOTA_EDIT')">
	<section id="edit-nota" class="first">

		<g:hasErrors bean="${notaInstance}">
		<div class="alert alert-danger">
			<g:renderErrors bean="${notaInstance}" as="list" />
		</div>
		</g:hasErrors>

		<g:form method="post" class="form-horizontal" role="form" >
			<g:hiddenField name="id" value="${notaInstance?.id}" />
			<g:hiddenField name="version" value="${notaInstance?.version}" />
			<g:hiddenField name="_method" value="PUT" />
			
			<g:render template="form"/>
			
			<div class="form-actions margin-top-medium">
				<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
	            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
			</div>
		</g:form>

	</section>
</sec:access>
</body>

</html>
