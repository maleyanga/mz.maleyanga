<%@ page import="mz.maleyanga.documento.Nota" %>
<!DOCTYPE html>
<html>

<head>
	%{--<meta name="layout" content="kickstart" />--}%
	<g:set var="entityName" value="${message(code: 'nota.label', default: 'Nota')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<sec:ifNotGranted roles="NOTA_CREATE">
	<g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('NOTA_CREATE')">
<body>

	<section id="create-nota" class="first">

		<g:hasErrors bean="${notaInstance}">
		<div class="alert alert-danger">
			<g:renderErrors bean="${notaInstance}" as="list" />
		</div>
		</g:hasErrors>

		<g:form action="save" class="form-horizontal" role="form" >
			<g:render template="form"/>

			<div class="form-actions margin-top-medium">
				<g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
	            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
			</div>
		</g:form>

	</section>

</body>
</sec:access>
</html>
