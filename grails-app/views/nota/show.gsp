
<%@ page import="mz.maleyanga.documento.Nota" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'nota.label', default: 'Nota')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<sec:ifNotGranted roles="NOTA_SHOW">
	<g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('NOTA_SHOW')">
<section id="show-nota" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="nota.pai.label" default="Pai" /></td>
				
				<td valign="top" class="value"><g:link controller="nota" action="show" id="${notaInstance?.pai?.id}">${notaInstance?.pai?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="nota.filha.label" default="Filha" /></td>
				
				<td valign="top" class="value"><g:link controller="nota" action="show" id="${notaInstance?.filha?.id}">${notaInstance?.filha?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="nota.pagamento.label" default="Pagamento" /></td>
				
				<td valign="top" class="value"><g:link controller="pagamento" action="show" id="${notaInstance?.pagamento?.id}">${notaInstance?.pagamento?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="nota.autor.label" default="Autor" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: notaInstance, field: "autor")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="nota.dateCreated.label" default="Date Created" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${notaInstance?.dateCreated}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="nota.messagem.label" default="Messagem" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: notaInstance, field: "messagem")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="nota.utilizador.label" default="Utilizador" /></td>
				
				<td valign="top" class="value"><g:link controller="utilizador" action="show" id="${notaInstance?.utilizador?.id}">${notaInstance?.utilizador?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
		</tbody>
	</table>
</section>
</sec:access>
</body>

</html>
