
<%@ page import="mz.maleyanga.documento.Nota" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'nota.label', default: 'Nota')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<sec:ifNotGranted roles="NOTA_LIST">
	<g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('NOTA_LIST')">
<section id="list-nota" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<th><g:message code="nota.pai.label" default="Pai" /></th>
			
				<th><g:message code="nota.filha.label" default="Filha" /></th>
			
				<th><g:message code="nota.pagamento.label" default="Pagamento" /></th>
			
				<g:sortableColumn property="autor" title="${message(code: 'nota.autor.label', default: 'Autor')}" />
			
				<g:sortableColumn property="dateCreated" title="${message(code: 'nota.dateCreated.label', default: 'Date Created')}" />
			
				<g:sortableColumn property="messagem" title="${message(code: 'nota.messagem.label', default: 'Messagem')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${notaInstanceList}" status="i" var="notaInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${notaInstance.id}">${fieldValue(bean: notaInstance, field: "pai")}</g:link></td>
			
				<td>${fieldValue(bean: notaInstance, field: "filha")}</td>
			
				<td>${fieldValue(bean: notaInstance, field: "pagamento")}</td>
			
				<td>${fieldValue(bean: notaInstance, field: "autor")}</td>
			
				<td><g:formatDate date="${notaInstance.dateCreated}" /></td>
			
				<td>${fieldValue(bean: notaInstance, field: "messagem")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${notaInstanceCount}" />
	</div>
</section>
</sec:access>
</body>

</html>
