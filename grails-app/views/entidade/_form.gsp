<%@ page import="mz.maleyanga.entidade.Entidade" %>




<div class="panel-body">
    <div class="col-lg-2">
        <div class="${hasErrors(bean: entidadeInstance, field: 'nome', 'error')} ">
            <label for="nome" class="control-label"><g:message code="entidade.nome.label" default="Nome"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="nome" value="${entidadeInstance?.nome}"/>
        <span class="help-inline">${hasErrors(bean: entidadeInstance, field: 'nome', 'error')}</span>
    </div>

</div>


<div class="panel-body">
    <div class="col-lg-2">
        <div class="${hasErrors(bean: entidadeInstance, field: 'nuit', 'error')} ">
            <label for="nuit" class="control-label"><g:message code="entidade.nuit.label" default="Nuit"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField readonly="true" class="form-control" name="nuit" value="${entidadeInstance?.nuit}"/>
        <span class="help-inline">${hasErrors(bean: entidadeInstance, field: 'nuit', 'error')}</span>
    </div>

</div>


<div class="panel-body">
    <div class="col-lg-2">
        <div class="${hasErrors(bean: entidadeInstance, field: 'residencia', 'error')} ">
            <label for="residencia" class="control-label"><g:message code="entidade.residencia.label"
                                                                     default="Residencia"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="residencia" value="${entidadeInstance?.residencia}"/>
        <span class="help-inline">${hasErrors(bean: entidadeInstance, field: 'residencia', 'error')}</span>
    </div>

</div>


<div class="panel-body">
    <div class="col-lg-2">
        <div class="${hasErrors(bean: entidadeInstance, field: 'email', 'error')} ">
            <label for="email" class="control-label"><g:message code="entidade.email.label" default="Email"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="email" value="${entidadeInstance?.email}"/>
        <span class="help-inline">${hasErrors(bean: entidadeInstance, field: 'email', 'error')}</span>
    </div>

</div>


<div class="panel-body">
    <div class="col-lg-2">
        <div class="${hasErrors(bean: entidadeInstance, field: 'telefone', 'error')} ">
            <label for="telefone" class="control-label"><g:message code="entidade.telefone.label"
                                                                   default="Telefone"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="telefone" value="${entidadeInstance?.telefone}"/>
        <span class="help-inline">${hasErrors(bean: entidadeInstance, field: 'telefone', 'error')}</span>
    </div>

</div>


<div class="panel-body">
    <div class="col-lg-2">
        <div class="${hasErrors(bean: entidadeInstance, field: 'proprietario', 'error')} ">
            <label for="proprietario" class="control-label"><g:message code="entidade.proprietario.label"
                                                                       default="Proprietario"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textField class="form-control" name="proprietario" value="${entidadeInstance?.proprietario}"/>
        <span class="help-inline">${hasErrors(bean: entidadeInstance, field: 'proprietario', 'error')}</span>
    </div>

</div>


<div class="panel-body hidden">
    <div class="col-lg-2">
        <div class="${hasErrors(bean: entidadeInstance, field: 'descricaoDaFormulaDeCalculo', 'error')} ">
            <label for="descricaoDaFormulaDeCalculo" class="control-label"><g:message
                    code="entidade.descricaoDaFormulaDeCalculo.label"
                    default="Descricao Da Formula De Calculo"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:textArea class="form-control" name="descricaoDaFormulaDeCalculo" cols="40" rows="5" maxlength="2000"
                    value="${entidadeInstance?.descricaoDaFormulaDeCalculo}"/>
        <span class="help-inline">${hasErrors(bean: entidadeInstance, field: 'descricaoDaFormulaDeCalculo', 'error')}</span>
    </div>

</div>


<div class="panel-body hidden">
    <div class="col-lg-2">
        <div class="${hasErrors(bean: entidadeInstance, field: 'formaDeCalculo', 'error')} ">
            <label for="formaDeCalculo" class="control-label"><g:message code="entidade.formaDeCalculo.label"
                                                                         default="Forma De Calculo"/></label>
        </div>
    </div>

    <div class="col-lg-6">
        <g:select readonly="true" class="form-control" name="formaDeCalculo"
                  from="${entidadeInstance.constraints.formaDeCalculo.inList}"
                  value="${entidadeInstance?.formaDeCalculo}" valueMessagePrefix="entidade.formaDeCalculo"
                  noSelection="['': '']"/>
        <span class="help-inline">${hasErrors(bean: entidadeInstance, field: 'formaDeCalculo', 'error')}</span>
    </div>

</div>


<div class="panel-body">
    <div class="col-lg-2">
        <div class="${hasErrors(bean: entidadeInstance, field: 'periodos', 'error')} ">
            <label for="periodos" class="control-label"><g:message code="entidade.periodos.label"
                                                                   default="Periodos"/></label>
        </div>
    </div>

    <div class="col-lg-6">

        <ul class="one-to-many">
            <g:each in="${entidadeInstance?.periodos ?}" var="p">
                <li><g:link controller="periodo" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
            </g:each>
            <li class="add">
                <g:link controller="periodo" action="create"
                        params="['entidade.id': entidadeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'periodo.label', default: 'Periodo')])}</g:link>
            </li>
        </ul>

        <span class="help-inline">${hasErrors(bean: entidadeInstance, field: 'periodos', 'error')}</span>
    </div>

</div>

<div class="panel-body">
    <div class="col-lg-2">
        <div class="${hasErrors(bean: entidadeInstance, field: 'clientes', 'error')} ">
            <label for="clientes" class="control-label"><g:message code="entidade.clientes.label"
                                                                   default="Clientes"/></label>
        </div>
    </div>

    <div class="col-lg-6">

        <ul class="one-to-many">
            <g:each in="${entidadeInstance?.clientes ?}" var="c">
                <li><g:link controller="cliente" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
            </g:each>
            <li class="add">
                <g:link controller="cliente" action="create"
                        params="['entidade.id': entidadeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'cliente.label', default: 'Cliente')])}</g:link>
            </li>
        </ul>

        <span class="help-inline">${hasErrors(bean: entidadeInstance, field: 'clientes', 'error')}</span>
    </div>

</div>

