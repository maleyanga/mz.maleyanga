<%@ page import="mz.maleyanga.entidade.Entidade" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'entidade.label', default: 'Entidade')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>
<sec:ifNotGranted roles="ENTIDADE_CREATE">
	<g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('ENTIDADE_CREATE')">
	<section id="create-entidade" class="first">

		<g:hasErrors bean="${entidadeInstance}">
		<div class="alert alert-danger">
			<g:renderErrors bean="${entidadeInstance}" as="list" />
		</div>
		</g:hasErrors>

		<g:form action="save" class="form-horizontal" role="form" >
			<g:render template="form"/>

			<div class="form-actions margin-top-medium">
				<g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
	            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
			</div>
		</g:form>

	</section>
</sec:access>
</body>

</html>
