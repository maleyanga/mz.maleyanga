
<%@ page import="mz.maleyanga.entidade.Entidade" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'entidade.label', default: 'Entidade')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>
<sec:ifNotGranted roles="ENTIDADE_INDEX">
	<g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('ENTIDADE_INDEX')">
<section id="index-entidade" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="nome" title="${message(code: 'entidade.nome.label', default: 'Nome')}" />
			
				<g:sortableColumn property="nuit" title="${message(code: 'entidade.nuit.label', default: 'Nuit')}" />
			
				<g:sortableColumn property="residencia" title="${message(code: 'entidade.residencia.label', default: 'Residencia')}" />
			
				<g:sortableColumn property="email" title="${message(code: 'entidade.email.label', default: 'Email')}" />
			
				<g:sortableColumn property="telefone" title="${message(code: 'entidade.telefone.label', default: 'Telefone')}" />
			
				<g:sortableColumn property="proprietario" title="${message(code: 'entidade.proprietario.label', default: 'Proprietario')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${entidadeInstanceList}" status="i" var="entidadeInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${entidadeInstance.id}">${fieldValue(bean: entidadeInstance, field: "nome")}</g:link></td>
			
				<td>${fieldValue(bean: entidadeInstance, field: "nuit")}</td>
			
				<td>${fieldValue(bean: entidadeInstance, field: "residencia")}</td>
			
				<td>${fieldValue(bean: entidadeInstance, field: "email")}</td>
			
				<td>${fieldValue(bean: entidadeInstance, field: "telefone")}</td>
			
				<td>${fieldValue(bean: entidadeInstance, field: "proprietario")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${entidadeInstanceCount}" />
	</div>
</section>
</sec:access>
</body>

</html>
