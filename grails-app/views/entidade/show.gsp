
<%@ page import="mz.maleyanga.entidade.Entidade" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'entidade.label', default: 'Entidade')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<sec:ifNotGranted roles="ENTIDADE_SHOW">
	<g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('ENTIDADE_SHOW')"><ul class="nav nav-pills margin-top-small">
	<li class="${params.action == "create" ? 'active' : ''}"><g:remoteLink controller="conta"
																		   action="createFromEntidade" update="success"
																		   params="${entidadeInstance.id}"><glyph:icon
				iconName="novo"/>Nova Conta</g:remoteLink></li>
</ul>

<section id="show-entidade" class="first">
	<div id="success">

	</div>
	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="entidade.nome.label" default="Nome" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: entidadeInstance, field: "nome")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="entidade.nuit.label" default="Nuit" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: entidadeInstance, field: "nuit")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="entidade.residencia.label" default="Residencia" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: entidadeInstance, field: "residencia")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="entidade.email.label" default="Email" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: entidadeInstance, field: "email")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="entidade.telefone.label" default="Telefone" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: entidadeInstance, field: "telefone")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="entidade.proprietario.label" default="Proprietario" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: entidadeInstance, field: "proprietario")}</td>
				
			</tr>
		<tr id="contas" class="prop">
			<td valign="top" class="name">Contas:</td>

			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${entidadeInstance.contas}" var="p">
						<li><g:link controller="conta" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
		</tr>
			<tr class="prop">
				<td valign="top" class="name"><g:message code="entidade.clientes.label" default="Clientes" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${entidadeInstance.clientes}" var="c">
						<li><g:link controller="cliente" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="entidade.descricaoDaFormulaDeCalculo.label" default="Descricao Da Formula De Calculo" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: entidadeInstance, field: "descricaoDaFormulaDeCalculo")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="entidade.formaDeCalculo.label" default="Forma De Calculo" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: entidadeInstance, field: "formaDeCalculo")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="entidade.periodos.label" default="Periodos" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${entidadeInstance.periodos}" var="p">
						<li><g:link controller="periodo" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
		</tbody>
	</table>
</section>
</sec:access>
</body>

</html>
