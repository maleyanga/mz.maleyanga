<%@ page import="mz.maleyanga.entidade.Entidade" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'entidade.label', default: 'Entidade')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>

<body>
<sec:ifNotGranted roles="ENTIDADE_EDIT">
	<g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('ENTIDADE_EDIT')">
	<section id="edit-entidade" class="first">

		<g:hasErrors bean="${entidadeInstance}">
		<div class="alert alert-danger">
			<g:renderErrors bean="${entidadeInstance}" as="list" />
		</div>
		</g:hasErrors>

		<g:form method="post" class="form-horizontal" role="form" >
			<g:hiddenField name="id" value="${entidadeInstance?.id}" />
			<g:hiddenField name="version" value="${entidadeInstance?.version}" />
			<g:hiddenField name="_method" value="PUT" />
			
			<g:render template="form"/>
			
			<div class="form-actions margin-top-medium">
				<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
	            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
			</div>
		</g:form>

	</section>
</sec:access>
</body>

</html>
