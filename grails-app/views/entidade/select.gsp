
<%@ page import="mz.maleyanga.entidade.Entidade" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'entidade.label', default: 'Entidade')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
	<z:head/>
</head>

<body>
%{--<z:body/>--}%
<sec:ifNotGranted roles="ENTIDADE_SELECT">
	<g:render template="/layouts/acessoNegado"/>
</sec:ifNotGranted>
<sec:access expression="hasRole('ENTIDADE_SELECT')">
<section id="index-entidade" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
					<h3>Selecione a Empresa</h3>

				<g:sortableColumn property="nome" title="${message(code: 'entidade.nome.label', default: 'Nome')}" />

			</tr>
		</thead>
		<tbody>


		<g:each onchange="submit();" in="${entidadeInstanceList}" status="i" var="entidadeInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

				<td><g:remoteLink action="goToHome" id="${entidadeInstance.id}">${fieldValue(bean: entidadeInstance, field: "nome")}</g:remoteLink></td>

			</tr>
		</g:each>

		</tbody>
	</table>

</section>
</sec:access>
</body>

</html>
