package mz.maleyanga.pedidoDeCredito

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(PedidoDeCreditoController)
@Mock(PedidoDeCredito)
class PedidoDeCreditoControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.pedidoDeCreditoInstanceList
            model.pedidoDeCreditoInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.pedidoDeCreditoInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def pedidoDeCredito = new PedidoDeCredito()
            pedidoDeCredito.validate()
            controller.save(pedidoDeCredito)

        then:"The create view is rendered again with the correct model"
            model.pedidoDeCreditoInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            pedidoDeCredito = new PedidoDeCredito(params)

            controller.save(pedidoDeCredito)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/pedidoDeCredito/show/1'
            controller.flash.message != null
            PedidoDeCredito.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def pedidoDeCredito = new PedidoDeCredito(params)
            controller.show(pedidoDeCredito)

        then:"A model is populated containing the domain instance"
            model.pedidoDeCreditoInstance == pedidoDeCredito
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def pedidoDeCredito = new PedidoDeCredito(params)
            controller.edit(pedidoDeCredito)

        then:"A model is populated containing the domain instance"
            model.pedidoDeCreditoInstance == pedidoDeCredito
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/pedidoDeCredito/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def pedidoDeCredito = new PedidoDeCredito()
            pedidoDeCredito.validate()
            controller.update(pedidoDeCredito)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.pedidoDeCreditoInstance == pedidoDeCredito

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            pedidoDeCredito = new PedidoDeCredito(params).save(flush: true)
            controller.update(pedidoDeCredito)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/pedidoDeCredito/show/$pedidoDeCredito.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/pedidoDeCredito/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def pedidoDeCredito = new PedidoDeCredito(params).save(flush: true)

        then:"It exists"
            PedidoDeCredito.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(pedidoDeCredito)

        then:"The instance is deleted"
            PedidoDeCredito.count() == 0
            response.redirectedUrl == '/pedidoDeCredito/index'
            flash.message != null
    }
}
