package mz.maleyanga.cliente

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(AssinanteController)
@Mock(Assinante)
class AssinanteControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when: "The index action is executed"
        controller.index()

        then: "The model is correct"
        !model.conjugeInstanceList
        model.conjugeInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when: "The create action is executed"
        controller.create()

        then: "The model is correctly created"
        model.conjugeInstance != null
    }

    void "Test the save action correctly persists an instance"() {

        when: "The save action is executed with an invalid instance"
        def conjuge = new Assinante()
        conjuge.validate()
        controller.save(conjuge)

        then: "The create view is rendered again with the correct model"
        model.conjugeInstance != null
        view == 'create'

        when: "The save action is executed with a valid instance"
        response.reset()
        populateValidParams(params)
        conjuge = new Assinante(params)

        controller.save(conjuge)

        then: "A redirect is issued to the show action"
        response.redirectedUrl == '/conjuge/show/1'
        controller.flash.message != null
        Assinante.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when: "The show action is executed with a null domain"
        controller.show(null)

        then: "A 404 error is returned"
        response.status == 404

        when: "A domain instance is passed to the show action"
        populateValidParams(params)
        def conjuge = new Assinante(params)
        controller.show(conjuge)

        then: "A model is populated containing the domain instance"
        model.conjugeInstance == conjuge
    }

    void "Test that the edit action returns the correct model"() {
        when: "The edit action is executed with a null domain"
        controller.edit(null)

        then: "A 404 error is returned"
        response.status == 404

        when: "A domain instance is passed to the edit action"
        populateValidParams(params)
        def conjuge = new Assinante(params)
        controller.edit(conjuge)

        then: "A model is populated containing the domain instance"
        model.conjugeInstance == conjuge
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when: "Update is called for a domain instance that doesn't exist"
        controller.update(null)

        then: "A 404 error is returned"
        response.redirectedUrl == '/conjuge/index'
        flash.message != null


        when: "An invalid domain instance is passed to the update action"
        response.reset()
        def conjuge = new Assinante()
        conjuge.validate()
        controller.update(conjuge)

        then: "The edit view is rendered again with the invalid instance"
        view == 'edit'
        model.conjugeInstance == conjuge

        when: "A valid domain instance is passed to the update action"
        response.reset()
        populateValidParams(params)
        conjuge = new Assinante(params).save(flush: true)
        controller.update(conjuge)

        then: "A redirect is issues to the show action"
        response.redirectedUrl == "/conjuge/show/$conjuge.id"
        flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when: "The delete action is called for a null instance"
        controller.delete(null)

        then: "A 404 is returned"
        response.redirectedUrl == '/conjuge/index'
        flash.message != null

        when: "A domain instance is created"
        response.reset()
        populateValidParams(params)
        def conjuge = new Assinante(params).save(flush: true)

        then: "It exists"
        Assinante.count() == 1

        when: "The domain instance is passed to the delete action"
        controller.delete(conjuge)

        then: "The instance is deleted"
        Assinante.count() == 0
        response.redirectedUrl == '/conjuge/index'
        flash.message != null
    }
}
