package mz.maleyanga.diario


import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(DiarioController)
@Mock(Diario)
class DiarioControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties in order to make the test pass
    }

    def populateInvalidParams(params) {
        assert params != null
        // TODO: Populate properties that fail validation in order to make the test pass
    }

    void "Test the index action returns the correct model"() {

        when: "The index action is executed"
        controller.index().get()

        then: "The model is correct"
        !model.diarioInstanceList
        model.diarioInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when: "The create action is executed"
        controller.create()

        then: "The model is correctly created"
        model.diarioInstance != null
    }

    void "Test the save action correctly persists an instance"() {

        when: "The save action is executed with an invalid instance"
        def diario = new Diario()
        diario.validate()
        controller.save(diario).get()

        then: "The create view is rendered again with the correct model"
        model.diarioInstance != null
        view == 'create'

        when: "The save action is executed with a valid instance"
        response.reset()
        populateValidParams(params)
        diario = new Diario(params)

        controller.save(diario).get()

        then: "A redirect is issued to the show action"
        response.redirectedUrl == "/diario/show/$diario.id"
        controller.flash.message != null
        Diario.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when: "The show action is execu ted with a null domain"
        controller.show(null).get()

        then: "A 404 error is returned"
        response.status == 404

        when: "A domain instance is passed to the show action"
        populateValidParams(params)
        def diario = new Diario(params).save(flush: true)

        controller.show(diario.id).get()

        then: "A model is populated containing the domain instance"
        model.diarioInstance.id == diario.id
    }

    void "Test that the edit action returns the correct model"() {
        when: "The edit action is executed with a null domain"
        controller.edit(null).get()

        then: "A 404 error is returned"
        response.status == 404

        when: "A domain instance is passed to the edit action"
        populateValidParams(params)
        def diario = new Diario(params).save(flush: true)
        controller.edit(diario?.id).get()

        then: "A model is populated containing the domain instance"
        model.diarioInstance.id == diario.id
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when: "Update is called for a domain instance that doesn't exist"
        controller.update(null).get()

        then: "A 404 error is returned"
        status == 404

        when: "An invalid domain instance is passed to the update action"
        response.reset()
        populateValidParams(params)
        def diario = new Diario(params).save(flush: true)
        params.clear()
        populateInvalidParams(params)
        controller.update(diario.id).get()

        then: "The edit view is rendered again with the invalid instance"
        view == 'edit'
        model.diarioInstance.id == diario.id

        when: "A valid domain instance is passed to the update action"
        response.reset()
        params.clear()
        populateValidParams(params)
        controller.update(diario.id).get()

        then: "A redirect is issues to the show action"
        response.redirectedUrl == "/diario/show/$diario.id"
        flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when: "The delete action is called for a null instance"
        controller.delete(null).get()

        then: "A 404 is returned"
        status == 404

        when: "A domain instance is created"
        response.reset()
        populateValidParams(params)
        def diario = new Diario(params).save(flush: true)

        then: "It exists"
        Diario.count() == 1

        when: "The domain instance is passed to the delete action"
        controller.delete(diario.id).get()

        then: "The instance is deleted"
        Diario.count() == 0
        response.redirectedUrl == '/diario/index'
        flash.message != null
    }
}
