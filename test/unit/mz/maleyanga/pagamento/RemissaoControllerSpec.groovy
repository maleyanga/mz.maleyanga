package mz.maleyanga.pagamento

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification
import mz.maleyanga.pagamento.Remissao

@TestFor(RemissaoController)
@Mock(Remissao)
class RemissaoControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when: "The index action is executed"
        controller.index()

        then: "The model is correct"
        !model.remissaoInstanceList
        model.remissaoInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when: "The create action is executed"
        controller.create()

        then: "The model is correctly created"
        model.remissaoInstance != null
    }

    void "Test the save action correctly persists an instance"() {

        when: "The save action is executed with an invalid instance"
        def remissao = new Remissao()
        remissao.validate()
        controller.save(remissao)

        then: "The create view is rendered again with the correct model"
        model.remissaoInstance != null
        view == 'create'

        when: "The save action is executed with a valid instance"
        response.reset()
        populateValidParams(params)
        remissao = new Remissao(params)

        controller.save(remissao)

        then: "A redirect is issued to the show action"
        response.redirectedUrl == '/remissao/show/1'
        controller.flash.message != null
        Remissao.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when: "The show action is executed with a null domain"
        controller.show(null)

        then: "A 404 error is returned"
        response.status == 404

        when: "A domain instance is passed to the show action"
        populateValidParams(params)
        def remissao = new Remissao(params)
        controller.show(remissao)

        then: "A model is populated containing the domain instance"
        model.remissaoInstance == remissao
    }

    void "Test that the edit action returns the correct model"() {
        when: "The edit action is executed with a null domain"
        controller.edit(null)

        then: "A 404 error is returned"
        response.status == 404

        when: "A domain instance is passed to the edit action"
        populateValidParams(params)
        def remissao = new Remissao(params)
        controller.edit(remissao)

        then: "A model is populated containing the domain instance"
        model.remissaoInstance == remissao
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when: "Update is called for a domain instance that doesn't exist"
        controller.update(null)

        then: "A 404 error is returned"
        response.redirectedUrl == '/remissao/index'
        flash.message != null


        when: "An invalid domain instance is passed to the update action"
        response.reset()
        def remissao = new Remissao()
        remissao.validate()
        controller.update(remissao)

        then: "The edit view is rendered again with the invalid instance"
        view == 'edit'
        model.remissaoInstance == remissao

        when: "A valid domain instance is passed to the update action"
        response.reset()
        populateValidParams(params)
        remissao = new Remissao(params).save(flush: true)
        controller.update(remissao)

        then: "A redirect is issues to the show action"
        response.redirectedUrl == "/remissao/show/$remissao.id"
        flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when: "The delete action is called for a null instance"
        controller.delete(null)

        then: "A 404 is returned"
        response.redirectedUrl == '/remissao/index'
        flash.message != null

        when: "A domain instance is created"
        response.reset()
        populateValidParams(params)
        def remissao = new Remissao(params).save(flush: true)

        then: "It exists"
        Remissao.count() == 1

        when: "The domain instance is passed to the delete action"
        controller.delete(remissao)

        then: "The instance is deleted"
        Remissao.count() == 0
        response.redirectedUrl == '/remissao/index'
        flash.message != null
    }
}
