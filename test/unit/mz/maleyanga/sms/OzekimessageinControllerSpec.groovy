package mz.maleyanga.sms




import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(OzekimessageinController)
@Mock(Ozekimessagein)
class OzekimessageinControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.ozekimessageinInstanceList
            model.ozekimessageinInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.ozekimessageinInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def ozekimessagein = new Ozekimessagein()
            ozekimessagein.validate()
            controller.save(ozekimessagein)

        then:"The create view is rendered again with the correct model"
            model.ozekimessageinInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            ozekimessagein = new Ozekimessagein(params)

            controller.save(ozekimessagein)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/ozekimessagein/show/1'
            controller.flash.message != null
            Ozekimessagein.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def ozekimessagein = new Ozekimessagein(params)
            controller.show(ozekimessagein)

        then:"A model is populated containing the domain instance"
            model.ozekimessageinInstance == ozekimessagein
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def ozekimessagein = new Ozekimessagein(params)
            controller.edit(ozekimessagein)

        then:"A model is populated containing the domain instance"
            model.ozekimessageinInstance == ozekimessagein
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/ozekimessagein/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def ozekimessagein = new Ozekimessagein()
            ozekimessagein.validate()
            controller.update(ozekimessagein)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.ozekimessageinInstance == ozekimessagein

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            ozekimessagein = new Ozekimessagein(params).save(flush: true)
            controller.update(ozekimessagein)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/ozekimessagein/show/$ozekimessagein.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/ozekimessagein/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def ozekimessagein = new Ozekimessagein(params).save(flush: true)

        then:"It exists"
            Ozekimessagein.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(ozekimessagein)

        then:"The instance is deleted"
            Ozekimessagein.count() == 0
            response.redirectedUrl == '/ozekimessagein/index'
            flash.message != null
    }
}
